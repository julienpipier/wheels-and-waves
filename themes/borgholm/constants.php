<?php

define( 'BORGHOLM_ROOT', get_template_directory_uri() );
define( 'BORGHOLM_ROOT_DIR', get_template_directory() );
define( 'BORGHOLM_ASSETS_ROOT', BORGHOLM_ROOT . '/assets' );
define( 'BORGHOLM_ASSETS_ROOT_DIR', BORGHOLM_ROOT_DIR . '/assets' );
define( 'BORGHOLM_ASSETS_CSS_ROOT', BORGHOLM_ASSETS_ROOT . '/css' );
define( 'BORGHOLM_ASSETS_CSS_ROOT_DIR', BORGHOLM_ASSETS_ROOT_DIR . '/css' );
define( 'BORGHOLM_ASSETS_JS_ROOT', BORGHOLM_ASSETS_ROOT . '/js' );
define( 'BORGHOLM_ASSETS_JS_ROOT_DIR', BORGHOLM_ASSETS_ROOT_DIR . '/js' );
define( 'BORGHOLM_INC_ROOT', BORGHOLM_ROOT . '/inc' );
define( 'BORGHOLM_INC_ROOT_DIR', BORGHOLM_ROOT_DIR . '/inc' );
