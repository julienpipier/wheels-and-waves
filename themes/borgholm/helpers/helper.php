<?php

if ( ! function_exists( 'borgholm_is_installed' ) ) {
	/**
	 * Function that checks if forward plugin installed
	 *
	 * @param string $plugin - plugin name
	 *
	 * @return bool
	 */
	function borgholm_is_installed( $plugin ) {
		
		switch ( $plugin ) {
			case 'framework';
				return class_exists( 'QodeFramework' );
				break;
			case 'core';
				return class_exists( 'BorgholmCore' );
				break;
			case 'woocommerce';
				return class_exists( 'WooCommerce' );
				break;
			case 'gutenberg-page';
				$current_screen = function_exists( 'get_current_screen' ) ? get_current_screen() : array();
				
				return method_exists( $current_screen, 'is_block_editor' ) && $current_screen->is_block_editor();
				break;
			case 'gutenberg-editor':
				return class_exists( 'WP_Block_Type' );
				break;
			default:
				return false;
		}
	}
}

if ( ! function_exists( 'borgholm_include_theme_is_installed' ) ) {
	/**
	 * Function that set case is installed element for framework functionality
	 *
	 * @param bool $installed
	 * @param string $plugin - plugin name
	 *
	 * @return bool
	 */
	function borgholm_include_theme_is_installed( $installed, $plugin ) {
		
		if ( $plugin === 'theme' ) {
			return class_exists( 'BorgholmHandler' );
		}
		
		return $installed;
	}
	
	add_filter( 'qode_framework_filter_is_plugin_installed', 'borgholm_include_theme_is_installed', 10, 2 );
}

if ( ! function_exists( 'borgholm_template_part' ) ) {
	/**
	 * Function that echo module template part.
	 *
	 * @param string $module name of the module from inc folder
	 * @param string $template full path of the template to load
	 * @param string $slug
	 * @param array  $params array of parameters to pass to template
	 */
	function borgholm_template_part( $module, $template, $slug = '', $params = array() ) {
		echo borgholm_get_template_part( $module, $template, $slug, $params );
	}
}

if ( ! function_exists( 'borgholm_get_template_part' ) ) {
	/**
	 * Function that load module template part.
	 *
	 * @param string $module name of the module from inc folder
	 * @param string $template full path of the template to load
	 * @param string $slug
	 * @param array  $params array of parameters to pass to template
	 *
	 * @return string - string containing html of template
	 */
	function borgholm_get_template_part( $module, $template, $slug = '', $params = array() ) {
		//HTML Content from template
		$html          = '';
		$template_path = BORGHOLM_INC_ROOT_DIR . '/' . $module;
		
		$temp = $template_path . '/' . $template;
		if ( is_array( $params ) && count( $params ) ) {
			extract( $params );
		}
		
		$template = '';
		
		if ( ! empty( $temp ) ) {
			if ( ! empty( $slug ) ) {
				$template = "{$temp}-{$slug}.php";
				
				if ( ! file_exists( $template ) ) {
					$template = $temp . '.php';
				}
			} else {
				$template = $temp . '.php';
			}
		}
		
		if ( $template ) {
			ob_start();
			include( $template );
			$html = ob_get_clean();
		}
		
		return $html;
	}
}

if ( ! function_exists( 'borgholm_get_page_id' ) ) {
	/**
	 * Function that returns current page id
	 * Additional conditional is to check if current page is any wp archive page (archive, category, tag, date etc.) and returns -1
	 *
	 * @return int
	 */
	function borgholm_get_page_id() {
		$page_id = get_queried_object_id();
		
		if ( borgholm_is_wp_template() ) {
			$page_id = -1;
		}
		
		return apply_filters( 'borgholm_filter_page_id', $page_id );
	}
}

if ( ! function_exists( 'borgholm_is_wp_template' ) ) {
	/**
	 * Function that checks if current page default wp page
	 *
	 * @return bool
	 */
	function borgholm_is_wp_template() {
		return is_archive() || is_search() || is_404() || ( is_front_page() && is_home() );
	}
}

if ( ! function_exists( 'borgholm_get_ajax_status' ) ) {
	/**
	 * Function that return status from ajax functions
	 *
	 * @param string $status - success or error
	 * @param string $message - ajax message value
	 * @param string|array $data - returned value
	 * @param string $redirect - url address
	 */
	function borgholm_get_ajax_status( $status, $message, $data = null, $redirect = '' ) {
		$response = array(
			'status'   => esc_attr( $status ),
			'message'  => esc_html( $message ),
			'data'     => $data,
			'redirect' => ! empty( $redirect ) ? esc_url( $redirect ) : '',
		);
		
		$output = json_encode( $response );
		
		exit( $output );
	}
}

if ( ! function_exists( 'borgholm_get_icon' ) ) {
	/**
	 * Function that return icon html
	 *
	 * @param string $icon - icon class name
	 * @param string $icon_pack - icon pack name
	 * @param string $backup_text - backup text label if framework is not installed
	 * @param array $params - icon parameters
	 *
	 * @return string|mixed
	 */
	function borgholm_get_icon( $icon, $icon_pack, $backup_text, $params = array() ) {
		$value = borgholm_is_installed( 'framework' ) && borgholm_is_installed( 'core' ) ? qode_framework_icons()->render_icon( $icon, $icon_pack, $params ) : $backup_text;
		
		return $value;
	}
}

if ( ! function_exists( 'borgholm_render_icon' ) ) {
	/**
	 * Function that render icon html
	 *
	 * @param string $icon - icon class name
	 * @param string $icon_pack - icon pack name
	 * @param string $backup_text - backup text label if framework is not installed
	 * @param array $params - icon parameters
	 */
	function borgholm_render_icon( $icon, $icon_pack, $backup_text, $params = array() ) {
		echo borgholm_get_icon( $icon, $icon_pack, $backup_text, $params );
	}
}

if ( ! function_exists( 'borgholm_get_button_element' ) ) {
	/**
	 * Function that returns button with provided params
	 *
	 * @param array $params - array of parameters
	 *
	 * @return string - string representing button html
	 */
	function borgholm_get_button_element( $params ) {
		if ( class_exists( 'BorgholmCoreButtonShortcode' ) ) {
			return BorgholmCoreButtonShortcode::call_shortcode( $params );
		} else {
			$link   = isset( $params['link'] ) ? $params['link'] : '#';
			$target = isset( $params['target'] ) ? $params['target'] : '_self';
			$text   = isset( $params['text'] ) ? $params['text'] : '';
			
			return '<a itemprop="url" class="qodef-theme-button" href="' . esc_url( $link ) . '" target="' . esc_attr( $target ) . '">' . esc_html( $text ) . '</a>';
		}
	}
}

if ( ! function_exists( 'borgholm_render_button_element' ) ) {
	/**
	 * Function that render button with provided params
	 *
	 * @param array $params - array of parameters
	 */
	function borgholm_render_button_element( $params ) {
		echo borgholm_get_button_element( $params );
	}
}

if ( ! function_exists( 'borgholm_class_attribute' ) ) {
	/**
	 * Function that render class attribute
	 *
	 * @param string|array $class
	 */
	function borgholm_class_attribute( $class ) {
		echo borgholm_get_class_attribute( $class );
	}
}

if ( ! function_exists( 'borgholm_get_class_attribute' ) ) {
	/**
	 * Function that return class attribute
	 *
	 * @param string|array $class
	 *
	 * @return string|mixed
	 */
	function borgholm_get_class_attribute( $class ) {
		$value = borgholm_is_installed( 'framework' ) ? qode_framework_get_class_attribute( $class ) : '';
		
		return $value;
	}
}

if ( ! function_exists( 'borgholm_get_post_value_through_levels' ) ) {
	/**
	 * Function that returns meta value if exists
	 *
	 * @param string $name name of option
	 * @param int    $post_id id of
	 *
	 * @return string value of option
	 */
	function borgholm_get_post_value_through_levels( $name, $post_id = null ) {
		return borgholm_is_installed( 'framework' ) && borgholm_is_installed( 'core' ) ? borgholm_core_get_post_value_through_levels( $name, $post_id ) : '';
	}
}

if ( ! function_exists( 'borgholm_get_space_value' ) ) {
	/**
	 * Function that returns spacing value based on selected option
	 *
	 * @param string $text_value - textual value of spacing
	 *
	 * @return int
	 */
	function borgholm_get_space_value( $text_value ) {
		return borgholm_is_installed( 'core' ) ? borgholm_core_get_space_value( $text_value ) : 0;
	}
}

if ( ! function_exists( 'borgholm_wp_kses_html' ) ) {
	/**
	 * Function that does escaping of specific html.
	 * It uses wp_kses function with predefined attributes array.
	 *
	 * @see wp_kses()
	 *
	 * @param string $type - type of html element
	 * @param string $content - string to escape
	 *
	 * @return string escaped output
	 */
	function borgholm_wp_kses_html( $type, $content ) {
		return borgholm_is_installed( 'framework' ) ? qode_framework_wp_kses_html( $type, $content ) : $content;
	}
}

if ( ! function_exists( 'borgholm_modify_wp_kses_svg_atts' ) ) {
	/**
	 * Function that modifies array of allowed attributes for svg content
	 *
	 * @param array $atts
	 *
	 * @return bool
	 */
	function borgholm_modify_wp_kses_svg_atts( $atts ) {

		$atts['polygon'] = array(
			'points' => true
		);

		return $atts;
	}

	add_filter( 'qode_framework_filter_wp_kses_svg_atts', 'borgholm_modify_wp_kses_svg_atts' );
}

if ( ! function_exists( 'borgholm_modify_text_special_style_words' ) ) {
	/**
	 * Function that modifies text with special styled words
	 *
	 * @param string $text
	 * @param array $words_positions
	 *
	 * @return html
	 */
	function borgholm_modify_text_special_style_words( $text, $words_positions ) {

		if ( ! empty( $text ) ) {
			$split_text = explode( ' ', $text );

			if ( ! empty( $words_positions ) ) {
				foreach ( $words_positions as $position ) {
					$position = (int) $position;
					if ( isset( $split_text[ $position - 1 ] ) && ! empty( $split_text[ $position - 1 ] ) ) {
						$split_text[ $position - 1 ] = '<span class="qodef-special-style">' . $split_text[ $position - 1 ] . '</span>';
					}
				}
			}

			$text = implode( ' ', $split_text );
		}

		return $text;
	}
}

if ( ! function_exists( 'borgholm_custom_decoration_svg' ) ) {
	function borgholm_custom_decoration_svg() {

		$html = '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="52px" height="52px" x="0px" y="0px" viewBox="0 0 52 52" enable-background="new 0 0 52 52" xml:space="preserve">
					<g>
						<g>
							<path fill="#484848" d="M26.3,34.6c-4.6,0-8.3-3.8-8.3-8.4c0-4.7,3.7-8.4,8.3-8.4c4.6,0,8.3,3.8,8.3,8.4 C34.6,30.8,30.9,34.6,26.3,34.6z M26.3,19.3c-3.7,0-6.8,3.1-6.8,6.8c0,3.8,3,6.8,6.8,6.8c3.7,0,6.8-3.1,6.8-6.8 C33,22.3,30,19.3,26.3,19.3z"/>
							<path fill="#484848" d="M26.3,37.4c-4.1,0-8.6-1.9-12.9-5.4c-3.2-2.6-5.2-5.3-5.3-5.4c-0.2-0.3-0.2-0.7,0-1 c0.1-0.1,2.1-2.7,5.3-5.4c4.3-3.5,8.8-5.4,12.9-5.4c4.1,0,8.6,1.9,12.9,5.4c3.2,2.6,5.2,5.3,5.3,5.4c0.2,0.3,0.2,0.7,0,1 c-0.1,0.1-2.1,2.7-5.3,5.4C34.9,35.5,30.4,37.4,26.3,37.4z M9.7,26.1c1.7,2,8.5,9.7,16.5,9.7c8,0,14.9-7.7,16.5-9.7 c-1.7-2-8.5-9.7-16.5-9.7C18.2,16.4,11.4,24.1,9.7,26.1z"/>
							<path fill="#484848" d="M29.2,25.5c0.4,2.2-1.4,4-3.5,3.6c-1.2-0.2-2.1-1.2-2.4-2.4c-0.4-2.2,1.4-4,3.5-3.6 C28,23.4,29,24.3,29.2,25.5z"/>
							<path fill="#484848" d="M29.8,26.1c-1.9,0-3.4-1.5-3.4-3.4c0-1.9,1.5-3.4,3.4-3.4c1.9,0,3.4,1.5,3.4,3.4 C33.2,24.6,31.7,26.1,29.8,26.1z M29.8,20.9c-1,0-1.8,0.8-1.8,1.8c0,1,0.8,1.8,1.8,1.8c1,0,1.8-0.8,1.8-1.8 C31.6,21.7,30.8,20.9,29.8,20.9z"/>
						</g>
						<path fill="#484848" d="M26.3,2.4c-0.4,0-0.8-0.4-0.8-0.8V0.8c0-0.4,0.4-0.8,0.8-0.8S27,0.4,27,0.8v0.8C27,2,26.7,2.4,26.3,2.4z"/>
						<path fill="#484848" d="M26.3,12c-0.4,0-0.8-0.4-0.8-0.8V5.1c0-0.4,0.4-0.8,0.8-0.8S27,4.7,27,5.1v6.1C27,11.6,26.7,12,26.3,12z"/>
						<path fill="#484848" d="M26.3,48.2c-0.4,0-0.8-0.4-0.8-0.8v-6.2c0-0.4,0.4-0.8,0.8-0.8s0.8,0.4,0.8,0.8v6.2 C27,47.9,26.7,48.2,26.3,48.2z"/>
						<path fill="#484848" d="M26.3,52c-0.4,0-0.8-0.4-0.8-0.8v-0.6c0-0.4,0.4-0.8,0.8-0.8s0.8,0.4,0.8,0.8v0.6C27,51.6,26.7,52,26.3,52z "/>
						<path fill="#484848" d="M5.7,26.8H0.8C0.4,26.8,0,26.5,0,26c0-0.4,0.4-0.8,0.8-0.8h4.9c0.4,0,0.8,0.4,0.8,0.8 C6.5,26.5,6.2,26.8,5.7,26.8z"/>
						<path fill="#484848" d="M51.2,26.8h-4.9c-0.4,0-0.8-0.4-0.8-0.8c0-0.4,0.4-0.8,0.8-0.8h4.9c0.4,0,0.8,0.4,0.8,0.8 C52,26.5,51.6,26.8,51.2,26.8z"/>
						<path fill="#484848" d="M9.3,44c-0.2,0-0.4-0.1-0.6-0.2c-0.3-0.3-0.3-0.8,0-1.1l5.8-5.8c0.3-0.3,0.8-0.3,1.1,0 c0.3,0.3,0.3,0.8,0,1.1l-5.8,5.8C9.7,43.9,9.5,44,9.3,44z"/>
						<path fill="#484848" d="M37.4,15.5c-0.2,0-0.4-0.1-0.6-0.2c-0.3-0.3-0.3-0.8,0-1.1l5.8-5.9C43,8,43.5,8,43.8,8.3 c0.3,0.3,0.3,0.8,0,1.1L38,15.3C37.8,15.4,37.6,15.5,37.4,15.5z"/>
						<path fill="#484848" d="M9.1,9.4c-0.2,0-0.4-0.1-0.6-0.2L8.1,8.7c-0.3-0.3-0.3-0.8,0-1.1c0.3-0.3,0.8-0.3,1.1,0l0.4,0.5 c0.3,0.3,0.3,0.8,0,1.1C9.5,9.4,9.3,9.4,9.1,9.4z"/>
						<path fill="#484848" d="M15,15.5c-0.2,0-0.4-0.1-0.6-0.2l-3.8-3.9c-0.3-0.3-0.3-0.8,0-1.1c0.3-0.3,0.8-0.3,1.1,0l3.8,3.9 c0.3,0.3,0.3,0.8,0,1.1C15.4,15.4,15.2,15.5,15,15.5z"/>
						<path fill="#484848" d="M43.5,44.3c-0.2,0-0.4-0.1-0.6-0.2l-5.9-5.9c-0.3-0.3-0.3-0.8,0-1.1c0.3-0.3,0.8-0.3,1.1,0l5.9,5.9 c0.3,0.3,0.3,0.8,0,1.1C43.9,44.2,43.7,44.3,43.5,44.3z"/>
						<path fill="#484848" d="M45.7,46.5c-0.2,0-0.4-0.1-0.6-0.2l-0.3-0.3c-0.3-0.3-0.3-0.8,0-1.1c0.3-0.3,0.8-0.3,1.1,0l0.3,0.3 c0.3,0.3,0.3,0.8,0,1.1C46.1,46.4,45.9,46.5,45.7,46.5z"/>
						<path fill="#484848" d="M19.2,9.6c-0.3,0-0.6-0.2-0.7-0.5l-2.2-5.2c-0.2-0.4,0-0.9,0.4-1c0.4-0.2,0.9,0,1,0.4l2.2,5.2 c0.2,0.4,0,0.9-0.4,1C19.4,9.6,19.3,9.6,19.2,9.6z"/>
						<path fill="#484848" d="M35.2,48.6c-0.3,0-0.6-0.2-0.7-0.5l-1.7-4.2c-0.2-0.4,0-0.9,0.4-1c0.4-0.2,0.9,0,1,0.4l1.7,4.2 c0.2,0.4,0,0.9-0.4,1C35.4,48.6,35.3,48.6,35.2,48.6z"/>
						<path fill="#484848" d="M16.9,48.5c-0.1,0-0.2,0-0.3-0.1c-0.4-0.2-0.6-0.6-0.4-1l2.2-5.2c0.2-0.4,0.6-0.6,1-0.4 c0.4,0.2,0.6,0.6,0.4,1L17.6,48C17.5,48.3,17.2,48.5,16.9,48.5z"/>
						<path fill="#484848" d="M33.6,8.7c-0.1,0-0.2,0-0.3-0.1c-0.4-0.2-0.6-0.6-0.4-1l1.8-4.2c0.2-0.4,0.6-0.6,1-0.4 c0.4,0.2,0.6,0.6,0.4,1l-1.8,4.2C34.2,8.5,33.9,8.7,33.6,8.7z"/>
						<path fill="#484848" d="M4.7,36.5c-0.3,0-0.6-0.2-0.7-0.5c-0.2-0.4,0-0.9,0.4-1.1l5.1-2.4c0.4-0.2,0.9,0,1,0.4 c0.2,0.4,0,0.9-0.4,1.1L5,36.4C4.9,36.5,4.8,36.5,4.7,36.5z"/>
						<path fill="#484848" d="M43.5,18.4c-0.3,0-0.6-0.2-0.7-0.5c-0.2-0.4,0-0.9,0.4-1.1l4.1-1.9c0.4-0.2,0.9,0,1,0.4 c0.2,0.4,0,0.9-0.4,1.1l-4.1,1.9C43.7,18.4,43.6,18.4,43.5,18.4z"/>
						<path fill="#484848" d="M47.9,35.9c-0.1,0-0.2,0-0.3-0.1l-5.2-2.2c-0.4-0.2-0.6-0.6-0.4-1c0.2-0.4,0.6-0.6,1-0.4l5.2,2.2 c0.4,0.2,0.6,0.6,0.4,1C48.5,35.7,48.2,35.9,47.9,35.9z"/>
						<path fill="#484848" d="M8.5,18.9c-0.1,0-0.2,0-0.3-0.1L4.1,17c-0.4-0.2-0.6-0.6-0.4-1c0.2-0.4,0.6-0.6,1-0.4l4.2,1.8 c0.4,0.2,0.6,0.6,0.4,1C9.1,18.7,8.8,18.9,8.5,18.9z"/>
					</g>
				</svg>';

		return $html;
	}
}

if ( ! function_exists( 'borgholm_arrow_slim_left_svg' ) ) {
	function borgholm_arrow_slim_left_svg() {

		$html = '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="40px" height="10px" viewBox="0 0 39.7 9.1" style="transform:rotate(180deg)" xml:space="preserve">
                    <polygon points="35.1,5.1 35.1,7.7 38.9,4.6 35.1,1.6 35.1,4.2 "/>
                    <rect x="1.1" y="4.2" width="34.1" height="1"/>
                </svg>';

		return $html;
	}
}

if ( ! function_exists( 'borgholm_arrow_slim_right_svg' ) ) {
	function borgholm_arrow_slim_right_svg() {

		$html = '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="40px" height="10px" viewBox="0 0 39.7 9.1" style="transform:rotate(0)" xml:space="preserve">
                    <polygon points="35.1,5.1 35.1,7.7 38.9,4.6 35.1,1.6 35.1,4.2 "/>
                    <rect x="1.1" y="4.2" width="34.1" height="1"/>
                </svg>';

		return $html;
	}
}

if ( ! function_exists( 'borgholm_quote_svg' ) ) {
	function borgholm_quote_svg() {

		$html = '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="243px" height="204px" x="0px" y="0px"
	 viewBox="0 0 250 214" enable-background="new 0 0 250 214" xml:space="preserve">
<g>
	<path fill="none" d="M207.2,122.8c-14.9,0-24.8,4-29.6,12c-4.5-5.9-7.1-14.6-7.8-26.2c-0.1,0.4-0.4,0.7-0.8,0.7
		c-0.5,0-0.9-0.4-0.9-0.9s0.4-0.9,0.9-0.9c0.4,0,0.7,0.2,0.8,0.5c-0.1-1.9-0.2-3.9-0.2-6c0-2.1,0-4.1,0.1-6.1
		c-0.1,0.3-0.4,0.5-0.8,0.5c-0.5,0-0.9-0.4-0.9-0.9c0-0.5,0.4-0.9,0.9-0.9c0.4,0,0.7,0.2,0.8,0.5c0.9-18.2,5.1-35.1,12.8-50.8
		c-0.2,0.1-0.4,0.2-0.6,0.2c-0.5,0-0.9-0.4-0.9-0.9c0-0.5,0.4-0.9,0.9-0.9c0.5,0,0.9,0.4,0.9,0.9c0,0,0,0.1,0,0.1
		c6.4-12.9,15-24.9,26-36.1c1.1-1.1,1.1-2.4,0-4c-1.1-1.6-2.4-2.4-4-2.4c-1.1,0-1.9,0.3-2.4,0.8c-21.3,20.8-37.3,41.5-48,62
		c-5.1,9.8-9,20-11.6,30.6c0.1,0,0.1,0,0.2,0c0.5,0,0.9,0.4,0.9,0.9c0,0.5-0.4,0.9-0.9,0.9c-0.2,0-0.4-0.1-0.6-0.2
		c-2.7,11.1-4,22.7-4,34.7c0,26.7,5.2,47.2,15.6,61.6c7.1,9.8,16.1,16.3,27.1,19.4c0,0,0-0.1,0-0.1c0-0.5,0.4-0.9,0.9-0.9
		c0.5,0,0.9,0.4,0.9,0.9c0,0.2-0.1,0.4-0.2,0.5c4.7,1.2,9.6,1.8,15,1.8c16,0,28.7-4.1,38-12.4c4.9-4.3,8.4-9.4,10.8-15.4
		c-0.2-0.2-0.3-0.4-0.3-0.7c0-0.5,0.4-0.9,0.9-0.9c0,0,0,0,0,0c1.8-5.1,2.7-10.8,2.7-17.1c0-13.3-3.9-24.1-11.6-32.4
		C230.3,126.9,220,122.8,207.2,122.8z"/>
	<circle fill="none" cx="207.9" cy="4" r="0.9"/>
	<circle fill="none" cx="194.9" cy="17.4" r="0.9"/>
	<circle fill="none" cx="181.9" cy="30.3" r="0.9"/>
	<circle fill="none" cx="169" cy="43.6" r="0.9"/>
	<circle fill="none" cx="169" cy="56.5" r="0.9"/>
	<circle fill="none" cx="169" cy="69.3" r="0.9"/>
	<circle fill="none" cx="169" cy="82.2" r="0.9"/>
	<circle fill="none" cx="143" cy="159.9" r="0.9"/>
	<circle fill="none" cx="143" cy="147" r="0.9"/>
	<circle fill="none" cx="143" cy="133.7" r="0.9"/>
	<circle fill="none" cx="143" cy="120.8" r="0.9"/>
	<circle fill="none" cx="143" cy="108.4" r="0.9"/>
	<circle fill="none" cx="156" cy="185.5" r="0.9"/>
	<circle fill="none" cx="156" cy="172.6" r="0.9"/>
	<circle fill="none" cx="156" cy="159.9" r="0.9"/>
	<circle fill="none" cx="156" cy="147" r="0.9"/>
	<circle fill="none" cx="156" cy="133.7" r="0.9"/>
	<circle fill="none" cx="156" cy="120.8" r="0.9"/>
	<circle fill="none" cx="156" cy="108.4" r="0.9"/>
	<circle fill="none" cx="156" cy="95.5" r="0.9"/>
	<circle fill="none" cx="156" cy="82.2" r="0.9"/>
	<circle fill="none" cx="156" cy="69.3" r="0.9"/>
	<circle fill="none" cx="169" cy="198.8" r="0.9"/>
	<circle fill="none" cx="169" cy="185.5" r="0.9"/>
	<circle fill="none" cx="169" cy="172.6" r="0.9"/>
	<circle fill="none" cx="169" cy="159.9" r="0.9"/>
	<circle fill="none" cx="169" cy="147" r="0.9"/>
	<circle fill="none" cx="169" cy="133.7" r="0.9"/>
	<circle fill="none" cx="169" cy="120.8" r="0.9"/>
	<circle fill="none" cx="181.9" cy="198.8" r="0.9"/>
	<circle fill="none" cx="181.9" cy="185.5" r="0.9"/>
	<circle fill="none" cx="181.9" cy="172.6" r="0.9"/>
	<circle fill="none" cx="181.9" cy="159.9" r="0.9"/>
	<circle fill="none" cx="181.9" cy="147" r="0.9"/>
	<circle fill="none" cx="181.9" cy="133.7" r="0.9"/>
	<circle fill="none" cx="194.9" cy="211.7" r="0.9"/>
	<circle fill="none" cx="194.9" cy="198.8" r="0.9"/>
	<circle fill="none" cx="194.9" cy="185.5" r="0.9"/>
	<circle fill="none" cx="194.9" cy="172.6" r="0.9"/>
	<circle fill="none" cx="194.9" cy="159.9" r="0.9"/>
	<circle fill="none" cx="194.9" cy="147" r="0.9"/>
	<circle fill="none" cx="194.9" cy="133.7" r="0.9"/>
	<circle fill="none" cx="207.9" cy="211.7" r="0.9"/>
	<circle fill="none" cx="207.9" cy="198.8" r="0.9"/>
	<circle fill="none" cx="207.9" cy="185.5" r="0.9"/>
	<circle fill="none" cx="207.9" cy="172.6" r="0.9"/>
	<circle fill="none" cx="207.9" cy="159.9" r="0.9"/>
	<circle fill="none" cx="207.9" cy="147" r="0.9"/>
	<circle fill="none" cx="207.9" cy="133.7" r="0.9"/>
	<circle fill="none" cx="220.9" cy="198.8" r="0.9"/>
	<circle fill="none" cx="220.9" cy="185.5" r="0.9"/>
	<circle fill="none" cx="220.9" cy="172.6" r="0.9"/>
	<circle fill="none" cx="220.9" cy="159.9" r="0.9"/>
	<circle fill="none" cx="220.9" cy="147" r="0.9"/>
	<circle fill="none" cx="220.9" cy="133.7" r="0.9"/>
	<circle fill="none" cx="233.9" cy="198.8" r="0.9"/>
	<circle fill="none" cx="233.9" cy="185.5" r="0.9"/>
	<circle fill="none" cx="233.9" cy="172.6" r="0.9"/>
	<circle fill="none" cx="233.9" cy="159.9" r="0.9"/>
	<circle fill="none" cx="233.9" cy="147" r="0.9"/>
	<circle fill="none" cx="233.9" cy="133.7" r="0.9"/>
	<circle fill="none" cx="246.9" cy="159.9" r="0.9"/>
	<circle fill="none" cx="246.9" cy="172.6" r="0.9"/>
	<path fill="none" d="M68.8,122.8c-14.3,0-23.9,3.7-28.9,11c0,0.3-0.2,0.6-0.5,0.7c-0.1,0.1-0.2,0.2-0.2,0.3
		c-0.1-0.1-0.1-0.2-0.2-0.2c0,0,0,0,0,0c-0.5,0-0.9-0.4-0.9-0.9c0-0.1,0-0.2,0.1-0.3c-4.7-7-7-17.4-7-31.4c0-15.8,2.5-30.7,7.5-44.7
		c-0.3-0.1-0.5-0.4-0.5-0.8c0-0.5,0.4-0.9,0.9-0.9c0.1,0,0.2,0,0.2,0c3.2-8.5,7.3-16.8,12.3-24.6c-0.2-0.2-0.4-0.4-0.4-0.7
		c0-0.5,0.4-0.9,0.9-0.9c0.2,0,0.4,0.1,0.5,0.2c5.1-7.7,11-15,17.9-22c1.1-1.1,1.1-2.4,0-4c-1.1-1.6-2.4-2.4-4-2.4
		c-1.1,0-1.9,0.3-2.4,0.8c-9.4,9.2-17.8,18.4-25.2,27.5c0.1,0,0.1,0,0.2,0c0.5,0,0.9,0.4,0.9,0.9c0,0.5-0.4,0.9-0.9,0.9
		c-0.5,0-0.8-0.4-0.9-0.8C29.2,41.6,21.8,52.8,16,64c-0.8,1.6-1.6,3.1-2.3,4.7c0.1,0.2,0.2,0.4,0.2,0.6c0,0.5-0.4,0.9-0.9,0.9
		c0,0,0,0,0,0C5.8,85.8,1.6,102.4,0.4,120c0.3,0.1,0.5,0.4,0.5,0.8c0,0.4-0.3,0.7-0.7,0.8c-0.2,3-0.3,6.1-0.3,9.2c0,0.7,0,1.3,0,2
		c0,0,0,0,0,0c0.5,0,0.9,0.4,0.9,0.9c0,0.5-0.4,0.9-0.9,0.9c0.1,4.1,0.3,8,0.6,11.8c0.2,0.2,0.3,0.4,0.3,0.6c0,0.2-0.1,0.3-0.2,0.5
		c1.8,18.7,6.7,33.7,14.8,45C26,206.8,40.5,214,59.2,214c6.6,0,12.5-0.7,18-2.1c0-0.1,0-0.1,0-0.2c0-0.5,0.4-0.9,0.9-0.9
		c0.4,0,0.7,0.3,0.8,0.6c7.1-2,13.2-5.3,18.4-9.9c9.3-8.3,14-19.6,14-34c0-13.3-3.9-24.1-11.6-32.4
		C91.9,126.9,81.6,122.8,68.8,122.8z M65,3.1c0.5,0,0.9,0.4,0.9,0.9c0,0.5-0.4,0.9-0.9,0.9c-0.5,0-0.9-0.4-0.9-0.9
		C64.1,3.5,64.5,3.1,65,3.1z M52,16.6c0.5,0,0.9,0.4,0.9,0.9c0,0.5-0.4,0.9-0.9,0.9c-0.5,0-0.9-0.4-0.9-0.9
		C51.1,17,51.5,16.6,52,16.6z M39,42.7c0.5,0,0.9,0.4,0.9,0.9c0,0.5-0.4,0.9-0.9,0.9c-0.5,0-0.9-0.4-0.9-0.9
		C38.1,43.1,38.5,42.7,39,42.7z M13,186.4c-0.5,0-0.9-0.4-0.9-0.9s0.4-0.9,0.9-0.9c0.5,0,0.9,0.4,0.9,0.9S13.5,186.4,13,186.4z
		 M13,173.5c-0.5,0-0.9-0.4-0.9-0.9s0.4-0.9,0.9-0.9c0.5,0,0.9,0.4,0.9,0.9S13.5,173.5,13,173.5z M13,160.7c-0.5,0-0.9-0.4-0.9-0.9
		c0-0.5,0.4-0.9,0.9-0.9c0.5,0,0.9,0.4,0.9,0.9C13.9,160.3,13.5,160.7,13,160.7z M13,147.9c-0.5,0-0.9-0.4-0.9-0.9
		c0-0.5,0.4-0.9,0.9-0.9c0.5,0,0.9,0.4,0.9,0.9C13.9,147.5,13.5,147.9,13,147.9z M13,134.6c-0.5,0-0.9-0.4-0.9-0.9
		c0-0.5,0.4-0.9,0.9-0.9c0.5,0,0.9,0.4,0.9,0.9C13.9,134.2,13.5,134.6,13,134.6z M13,121.7c-0.5,0-0.9-0.4-0.9-0.9
		c0-0.5,0.4-0.9,0.9-0.9c0.5,0,0.9,0.4,0.9,0.9C13.9,121.3,13.5,121.7,13,121.7z M13,109.2c-0.5,0-0.9-0.4-0.9-0.9s0.4-0.9,0.9-0.9
		c0.5,0,0.9,0.4,0.9,0.9S13.5,109.2,13,109.2z M13,96.3c-0.5,0-0.9-0.4-0.9-0.9c0-0.5,0.4-0.9,0.9-0.9c0.5,0,0.9,0.4,0.9,0.9
		C13.9,96,13.5,96.3,13,96.3z M13,83.1c-0.5,0-0.9-0.4-0.9-0.9s0.4-0.9,0.9-0.9c0.5,0,0.9,0.4,0.9,0.9S13.5,83.1,13,83.1z M26,199.7
		c-0.5,0-0.9-0.4-0.9-0.9s0.4-0.9,0.9-0.9c0.5,0,0.9,0.4,0.9,0.9S26.5,199.7,26,199.7z M26,186.4c-0.5,0-0.9-0.4-0.9-0.9
		s0.4-0.9,0.9-0.9c0.5,0,0.9,0.4,0.9,0.9S26.5,186.4,26,186.4z M26,173.5c-0.5,0-0.9-0.4-0.9-0.9s0.4-0.9,0.9-0.9
		c0.5,0,0.9,0.4,0.9,0.9S26.5,173.5,26,173.5z M26,160.7c-0.5,0-0.9-0.4-0.9-0.9c0-0.5,0.4-0.9,0.9-0.9c0.5,0,0.9,0.4,0.9,0.9
		C26.9,160.3,26.5,160.7,26,160.7z M26,147.9c-0.5,0-0.9-0.4-0.9-0.9c0-0.5,0.4-0.9,0.9-0.9c0.5,0,0.9,0.4,0.9,0.9
		C26.9,147.5,26.5,147.9,26,147.9z M26,134.6c-0.5,0-0.9-0.4-0.9-0.9c0-0.5,0.4-0.9,0.9-0.9c0.5,0,0.9,0.4,0.9,0.9
		C26.9,134.2,26.5,134.6,26,134.6z M26,121.7c-0.5,0-0.9-0.4-0.9-0.9c0-0.5,0.4-0.9,0.9-0.9c0.5,0,0.9,0.4,0.9,0.9
		C26.9,121.3,26.5,121.7,26,121.7z M26,109.2c-0.5,0-0.9-0.4-0.9-0.9s0.4-0.9,0.9-0.9c0.5,0,0.9,0.4,0.9,0.9S26.5,109.2,26,109.2z
		 M26,96.3c-0.5,0-0.9-0.4-0.9-0.9c0-0.5,0.4-0.9,0.9-0.9c0.5,0,0.9,0.4,0.9,0.9C26.9,96,26.5,96.3,26,96.3z M26,83.1
		c-0.5,0-0.9-0.4-0.9-0.9s0.4-0.9,0.9-0.9c0.5,0,0.9,0.4,0.9,0.9S26.5,83.1,26,83.1z M26,70.2c-0.5,0-0.9-0.4-0.9-0.9
		s0.4-0.9,0.9-0.9c0.5,0,0.9,0.4,0.9,0.9S26.5,70.2,26,70.2z M26,57.4c-0.5,0-0.9-0.4-0.9-0.9c0-0.5,0.4-0.9,0.9-0.9
		c0.5,0,0.9,0.4,0.9,0.9C26.9,57,26.5,57.4,26,57.4z M39,199.7c-0.5,0-0.9-0.4-0.9-0.9s0.4-0.9,0.9-0.9c0.5,0,0.9,0.4,0.9,0.9
		S39.5,199.7,39,199.7z M39,186.4c-0.5,0-0.9-0.4-0.9-0.9s0.4-0.9,0.9-0.9c0.5,0,0.9,0.4,0.9,0.9S39.5,186.4,39,186.4z M39,173.5
		c-0.5,0-0.9-0.4-0.9-0.9s0.4-0.9,0.9-0.9c0.5,0,0.9,0.4,0.9,0.9S39.5,173.5,39,173.5z M39,160.7c-0.5,0-0.9-0.4-0.9-0.9
		c0-0.5,0.4-0.9,0.9-0.9c0.5,0,0.9,0.4,0.9,0.9C39.9,160.3,39.5,160.7,39,160.7z M39,147.9c-0.5,0-0.9-0.4-0.9-0.9
		c0-0.5,0.4-0.9,0.9-0.9c0.5,0,0.9,0.4,0.9,0.9C39.9,147.5,39.5,147.9,39,147.9z M52,212.6c-0.5,0-0.9-0.4-0.9-0.9s0.4-0.9,0.9-0.9
		c0.5,0,0.9,0.4,0.9,0.9S52.5,212.6,52,212.6z M52,199.7c-0.5,0-0.9-0.4-0.9-0.9s0.4-0.9,0.9-0.9c0.5,0,0.9,0.4,0.9,0.9
		S52.5,199.7,52,199.7z M52,186.4c-0.5,0-0.9-0.4-0.9-0.9s0.4-0.9,0.9-0.9c0.5,0,0.9,0.4,0.9,0.9S52.5,186.4,52,186.4z M52,173.5
		c-0.5,0-0.9-0.4-0.9-0.9s0.4-0.9,0.9-0.9c0.5,0,0.9,0.4,0.9,0.9S52.5,173.5,52,173.5z M52,160.7c-0.5,0-0.9-0.4-0.9-0.9
		c0-0.5,0.4-0.9,0.9-0.9c0.5,0,0.9,0.4,0.9,0.9C52.9,160.3,52.5,160.7,52,160.7z M52,147.9c-0.5,0-0.9-0.4-0.9-0.9
		c0-0.5,0.4-0.9,0.9-0.9c0.5,0,0.9,0.4,0.9,0.9C52.9,147.5,52.5,147.9,52,147.9z M52,134.6c-0.5,0-0.9-0.4-0.9-0.9
		c0-0.5,0.4-0.9,0.9-0.9c0.5,0,0.9,0.4,0.9,0.9C52.9,134.2,52.5,134.6,52,134.6z M65,212.6c-0.5,0-0.9-0.4-0.9-0.9s0.4-0.9,0.9-0.9
		c0.5,0,0.9,0.4,0.9,0.9S65.5,212.6,65,212.6z M65,199.7c-0.5,0-0.9-0.4-0.9-0.9s0.4-0.9,0.9-0.9c0.5,0,0.9,0.4,0.9,0.9
		S65.5,199.7,65,199.7z M65,186.4c-0.5,0-0.9-0.4-0.9-0.9s0.4-0.9,0.9-0.9c0.5,0,0.9,0.4,0.9,0.9S65.5,186.4,65,186.4z M65,173.5
		c-0.5,0-0.9-0.4-0.9-0.9s0.4-0.9,0.9-0.9c0.5,0,0.9,0.4,0.9,0.9S65.5,173.5,65,173.5z M65,160.7c-0.5,0-0.9-0.4-0.9-0.9
		c0-0.5,0.4-0.9,0.9-0.9c0.5,0,0.9,0.4,0.9,0.9C65.9,160.3,65.5,160.7,65,160.7z M65,147.9c-0.5,0-0.9-0.4-0.9-0.9
		c0-0.5,0.4-0.9,0.9-0.9c0.5,0,0.9,0.4,0.9,0.9C65.9,147.5,65.5,147.9,65,147.9z M65,134.6c-0.5,0-0.9-0.4-0.9-0.9
		c0-0.5,0.4-0.9,0.9-0.9c0.5,0,0.9,0.4,0.9,0.9C65.9,134.2,65.5,134.6,65,134.6z M78,199.7c-0.5,0-0.9-0.4-0.9-0.9s0.4-0.9,0.9-0.9
		c0.5,0,0.9,0.4,0.9,0.9S78.5,199.7,78,199.7z M78,186.4c-0.5,0-0.9-0.4-0.9-0.9s0.4-0.9,0.9-0.9c0.5,0,0.9,0.4,0.9,0.9
		S78.5,186.4,78,186.4z M78,173.5c-0.5,0-0.9-0.4-0.9-0.9s0.4-0.9,0.9-0.9c0.5,0,0.9,0.4,0.9,0.9S78.5,173.5,78,173.5z M78,160.7
		c-0.5,0-0.9-0.4-0.9-0.9c0-0.5,0.4-0.9,0.9-0.9c0.5,0,0.9,0.4,0.9,0.9C78.9,160.3,78.5,160.7,78,160.7z M78,147.9
		c-0.5,0-0.9-0.4-0.9-0.9c0-0.5,0.4-0.9,0.9-0.9c0.5,0,0.9,0.4,0.9,0.9C78.9,147.5,78.5,147.9,78,147.9z M78,134.6
		c-0.5,0-0.9-0.4-0.9-0.9c0-0.5,0.4-0.9,0.9-0.9c0.5,0,0.9,0.4,0.9,0.9C78.9,134.2,78.5,134.6,78,134.6z M91,199.7
		c-0.5,0-0.9-0.4-0.9-0.9s0.4-0.9,0.9-0.9c0.5,0,0.9,0.4,0.9,0.9S91.5,199.7,91,199.7z M91,186.4c-0.5,0-0.9-0.4-0.9-0.9
		s0.4-0.9,0.9-0.9c0.5,0,0.9,0.4,0.9,0.9S91.5,186.4,91,186.4z M91,173.5c-0.5,0-0.9-0.4-0.9-0.9s0.4-0.9,0.9-0.9
		c0.5,0,0.9,0.4,0.9,0.9S91.5,173.5,91,173.5z M91,160.7c-0.5,0-0.9-0.4-0.9-0.9c0-0.5,0.4-0.9,0.9-0.9c0.5,0,0.9,0.4,0.9,0.9
		C91.9,160.3,91.5,160.7,91,160.7z M91,147.9c-0.5,0-0.9-0.4-0.9-0.9c0-0.5,0.4-0.9,0.9-0.9c0.5,0,0.9,0.4,0.9,0.9
		C91.9,147.5,91.5,147.9,91,147.9z M91,134.6c-0.5,0-0.9-0.4-0.9-0.9c0-0.5,0.4-0.9,0.9-0.9c0.5,0,0.9,0.4,0.9,0.9
		C91.9,134.2,91.5,134.6,91,134.6z M104,146.1c0.5,0,0.9,0.4,0.9,0.9c0,0.5-0.4,0.9-0.9,0.9c-0.5,0-0.9-0.4-0.9-0.9
		C103.1,146.5,103.5,146.1,104,146.1z M104,159c0.5,0,0.9,0.4,0.9,0.9c0,0.5-0.4,0.9-0.9,0.9c-0.5,0-0.9-0.4-0.9-0.9
		C103.1,159.4,103.5,159,104,159z M104,171.8c0.5,0,0.9,0.4,0.9,0.9s-0.4,0.9-0.9,0.9c-0.5,0-0.9-0.4-0.9-0.9S103.5,171.8,104,171.8
		z M104,184.6c0.5,0,0.9,0.4,0.9,0.9s-0.4,0.9-0.9,0.9c-0.5,0-0.9-0.4-0.9-0.9S103.5,184.6,104,184.6z"/>
	<circle fill="#040404" cx="52" cy="211.7" r="0.9"/>
	<circle fill="#040404" cx="65" cy="211.7" r="0.9"/>
	<path fill="#040404" d="M78,210.8c-0.5,0-0.9,0.4-0.9,0.9c0,0.1,0,0.1,0,0.2c0.6-0.1,1.1-0.3,1.7-0.4
		C78.7,211.1,78.4,210.8,78,210.8z"/>
	<path fill="#040404" d="M181.9,210.8c-0.5,0-0.9,0.4-0.9,0.9c0,0,0,0.1,0,0.1c0.5,0.1,1,0.3,1.5,0.4c0.1-0.1,0.2-0.3,0.2-0.5
		C182.8,211.2,182.4,210.8,181.9,210.8z"/>
	<circle fill="#040404" cx="194.9" cy="211.7" r="0.9"/>
	<circle fill="#040404" cx="207.9" cy="211.7" r="0.9"/>
	<circle fill="#040404" cx="26" cy="198.8" r="0.9"/>
	<circle fill="#040404" cx="39" cy="198.8" r="0.9"/>
	<circle fill="#040404" cx="52" cy="198.8" r="0.9"/>
	<circle fill="#040404" cx="65" cy="198.8" r="0.9"/>
	<circle fill="#040404" cx="78" cy="198.8" r="0.9"/>
	<circle fill="#040404" cx="91" cy="198.8" r="0.9"/>
	<circle fill="#040404" cx="169" cy="198.8" r="0.9"/>
	<circle fill="#040404" cx="181.9" cy="198.8" r="0.9"/>
	<circle fill="#040404" cx="194.9" cy="198.8" r="0.9"/>
	<circle fill="#040404" cx="207.9" cy="198.8" r="0.9"/>
	<circle fill="#040404" cx="220.9" cy="198.8" r="0.9"/>
	<circle fill="#040404" cx="233.9" cy="198.8" r="0.9"/>
	<circle fill="#040404" cx="13" cy="185.5" r="0.9"/>
	<circle fill="#040404" cx="26" cy="185.5" r="0.9"/>
	<circle fill="#040404" cx="39" cy="185.5" r="0.9"/>
	<circle fill="#040404" cx="52" cy="185.5" r="0.9"/>
	<circle fill="#040404" cx="65" cy="185.5" r="0.9"/>
	<circle fill="#040404" cx="78" cy="185.5" r="0.9"/>
	<circle fill="#040404" cx="91" cy="185.5" r="0.9"/>
	<circle fill="#040404" cx="104" cy="185.5" r="0.9"/>
	<circle fill="#040404" cx="156" cy="185.5" r="0.9"/>
	<circle fill="#040404" cx="169" cy="185.5" r="0.9"/>
	<circle fill="#040404" cx="181.9" cy="185.5" r="0.9"/>
	<circle fill="#040404" cx="194.9" cy="185.5" r="0.9"/>
	<circle fill="#040404" cx="207.9" cy="185.5" r="0.9"/>
	<circle fill="#040404" cx="220.9" cy="185.5" r="0.9"/>
	<circle fill="#040404" cx="233.9" cy="185.5" r="0.9"/>
	<path fill="#040404" d="M246,185.5c0,0.3,0.1,0.5,0.3,0.7c0.2-0.5,0.4-1,0.6-1.5c0,0,0,0,0,0C246.4,184.6,246,185,246,185.5z"/>
	<circle fill="#040404" cx="13" cy="172.6" r="0.9"/>
	<circle fill="#040404" cx="26" cy="172.6" r="0.9"/>
	<circle fill="#040404" cx="39" cy="172.6" r="0.9"/>
	<circle fill="#040404" cx="52" cy="172.6" r="0.9"/>
	<circle fill="#040404" cx="65" cy="172.6" r="0.9"/>
	<circle fill="#040404" cx="78" cy="172.6" r="0.9"/>
	<circle fill="#040404" cx="91" cy="172.6" r="0.9"/>
	<circle fill="#040404" cx="104" cy="172.6" r="0.9"/>
	<circle fill="#040404" cx="156" cy="172.6" r="0.9"/>
	<circle fill="#040404" cx="169" cy="172.6" r="0.9"/>
	<circle fill="#040404" cx="181.9" cy="172.6" r="0.9"/>
	<circle fill="#040404" cx="194.9" cy="172.6" r="0.9"/>
	<circle fill="#040404" cx="207.9" cy="172.6" r="0.9"/>
	<circle fill="#040404" cx="220.9" cy="172.6" r="0.9"/>
	<circle fill="#040404" cx="233.9" cy="172.6" r="0.9"/>
	<circle fill="#040404" cx="246.9" cy="172.6" r="0.9"/>
	<circle fill="#040404" cx="13" cy="159.9" r="0.9"/>
	<circle fill="#040404" cx="26" cy="159.9" r="0.9"/>
	<circle fill="#040404" cx="39" cy="159.9" r="0.9"/>
	<circle fill="#040404" cx="52" cy="159.9" r="0.9"/>
	<circle fill="#040404" cx="65" cy="159.9" r="0.9"/>
	<circle fill="#040404" cx="78" cy="159.9" r="0.9"/>
	<circle fill="#040404" cx="91" cy="159.9" r="0.9"/>
	<circle fill="#040404" cx="104" cy="159.9" r="0.9"/>
	<circle fill="#040404" cx="143" cy="159.9" r="0.9"/>
	<circle fill="#040404" cx="156" cy="159.9" r="0.9"/>
	<circle fill="#040404" cx="169" cy="159.9" r="0.9"/>
	<circle fill="#040404" cx="181.9" cy="159.9" r="0.9"/>
	<circle fill="#040404" cx="194.9" cy="159.9" r="0.9"/>
	<circle fill="#040404" cx="207.9" cy="159.9" r="0.9"/>
	<circle fill="#040404" cx="220.9" cy="159.9" r="0.9"/>
	<circle fill="#040404" cx="233.9" cy="159.9" r="0.9"/>
	<circle fill="#040404" cx="246.9" cy="159.9" r="0.9"/>
	<path fill="#040404" d="M0.7,146.4c0,0.4,0.1,0.7,0.1,1.1c0.1-0.1,0.2-0.3,0.2-0.5C0.9,146.7,0.8,146.5,0.7,146.4z"/>
	<circle fill="#040404" cx="13" cy="147" r="0.9"/>
	<circle fill="#040404" cx="26" cy="147" r="0.9"/>
	<circle fill="#040404" cx="39" cy="147" r="0.9"/>
	<circle fill="#040404" cx="52" cy="147" r="0.9"/>
	<circle fill="#040404" cx="65" cy="147" r="0.9"/>
	<circle fill="#040404" cx="78" cy="147" r="0.9"/>
	<circle fill="#040404" cx="91" cy="147" r="0.9"/>
	<circle fill="#040404" cx="104" cy="147" r="0.9"/>
	<circle fill="#040404" cx="143" cy="147" r="0.9"/>
	<circle fill="#040404" cx="156" cy="147" r="0.9"/>
	<circle fill="#040404" cx="169" cy="147" r="0.9"/>
	<circle fill="#040404" cx="181.9" cy="147" r="0.9"/>
	<circle fill="#040404" cx="194.9" cy="147" r="0.9"/>
	<circle fill="#040404" cx="207.9" cy="147" r="0.9"/>
	<circle fill="#040404" cx="220.9" cy="147" r="0.9"/>
	<circle fill="#040404" cx="233.9" cy="147" r="0.9"/>
	<path fill="#040404" d="M0.9,133.7c0-0.5-0.4-0.9-0.9-0.9c0,0,0,0,0,0c0,0.6,0,1.2,0,1.8C0.5,134.6,0.9,134.2,0.9,133.7z"/>
	<circle fill="#040404" cx="13" cy="133.7" r="0.9"/>
	<circle fill="#040404" cx="26" cy="133.7" r="0.9"/>
	<circle fill="#040404" cx="52" cy="133.7" r="0.9"/>
	<circle fill="#040404" cx="39.3" cy="133.5" r="0.9"/>
	<circle fill="#040404" cx="65" cy="133.7" r="0.9"/>
	<circle fill="#040404" cx="78" cy="133.7" r="0.9"/>
	<circle fill="#040404" cx="195.1" cy="121" r="0.9"/>
	<circle fill="#040404" cx="208.1" cy="121" r="0.9"/>
	<circle fill="#040404" cx="221.1" cy="121" r="0.9"/>
	<circle fill="#040404" cx="51.8" cy="121" r="0.9"/>
	<circle fill="#040404" cx="64.8" cy="121" r="0.9"/>
	<circle fill="#040404" cx="77.8" cy="121" r="0.9"/>
	<circle fill="#040404" cx="91" cy="133.7" r="0.9"/>
	<circle fill="#040404" cx="143" cy="133.7" r="0.9"/>
	<circle fill="#040404" cx="156" cy="133.7" r="0.9"/>
	<circle fill="#040404" cx="169" cy="133.7" r="0.9"/>
	<circle fill="#040404" cx="181.9" cy="133.7" r="0.9"/>
	<circle fill="#040404" cx="194.9" cy="133.7" r="0.9"/>
	<circle fill="#040404" cx="207.9" cy="133.7" r="0.9"/>
	<circle fill="#040404" cx="220.9" cy="133.7" r="0.9"/>
	<circle fill="#040404" cx="233.9" cy="133.7" r="0.9"/>
	<path fill="#040404" d="M0.9,120.8c0-0.4-0.2-0.7-0.5-0.8c0,0.5-0.1,1.1-0.1,1.6C0.6,121.5,0.9,121.2,0.9,120.8z"/>
	<circle fill="#040404" cx="13" cy="120.8" r="0.9"/>
	<circle fill="#040404" cx="26" cy="120.8" r="0.9"/>
	<circle fill="#040404" cx="143" cy="120.8" r="0.9"/>
	<circle fill="#040404" cx="156" cy="120.8" r="0.9"/>
	<circle fill="#040404" cx="169" cy="120.8" r="0.9"/>
	<circle fill="#040404" cx="13" cy="108.4" r="0.9"/>
	<circle fill="#040404" cx="26" cy="108.4" r="0.9"/>
	<circle fill="#040404" cx="143" cy="108.4" r="0.9"/>
	<circle fill="#040404" cx="156" cy="108.4" r="0.9"/>
	<path fill="#040404" d="M169,107.5c-0.5,0-0.9,0.4-0.9,0.9s0.4,0.9,0.9,0.9c0.4,0,0.7-0.3,0.8-0.7c0-0.2,0-0.4,0-0.6
		C169.6,107.7,169.3,107.5,169,107.5z"/>
	<circle fill="#040404" cx="13" cy="95.5" r="0.9"/>
	<circle fill="#040404" cx="26" cy="95.5" r="0.9"/>
	<path fill="#040404" d="M143.8,95.5c0-0.5-0.4-0.9-0.9-0.9c-0.1,0-0.1,0-0.2,0c-0.1,0.5-0.3,1-0.4,1.5c0.2,0.1,0.4,0.2,0.6,0.2
		C143.5,96.3,143.8,96,143.8,95.5z"/>
	<circle fill="#040404" cx="156" cy="95.5" r="0.9"/>
	<path fill="#040404" d="M169,94.6c-0.5,0-0.9,0.4-0.9,0.9c0,0.5,0.4,0.9,0.9,0.9c0.3,0,0.6-0.2,0.8-0.5c0-0.2,0-0.5,0-0.7
		C169.6,94.8,169.3,94.6,169,94.6z"/>
	<circle fill="#040404" cx="13" cy="82.2" r="0.9"/>
	<circle fill="#040404" cx="26" cy="82.2" r="0.9"/>
	<circle fill="#040404" cx="156" cy="82.2" r="0.9"/>
	<circle fill="#040404" cx="169" cy="82.2" r="0.9"/>
	<path fill="#040404" d="M13.9,69.3c0-0.2-0.1-0.4-0.2-0.6c-0.2,0.5-0.5,1-0.7,1.5c0,0,0,0,0,0C13.5,70.2,13.9,69.8,13.9,69.3z"/>
	<circle fill="#040404" cx="26" cy="69.3" r="0.9"/>
	<circle fill="#040404" cx="156" cy="69.3" r="0.9"/>
	<circle fill="#040404" cx="169" cy="69.3" r="0.9"/>
	<circle fill="#040404" cx="26" cy="56.5" r="0.9"/>
	<path fill="#040404" d="M38.1,56.5c0,0.4,0.2,0.7,0.5,0.8c0.2-0.5,0.4-1.1,0.6-1.6c-0.1,0-0.1,0-0.2,0C38.5,55.6,38.1,56,38.1,56.5
		z"/>
	<circle fill="#040404" cx="169" cy="56.5" r="0.9"/>
	<circle fill="#040404" cx="39" cy="43.6" r="0.9"/>
	<circle fill="#040404" cx="169" cy="43.6" r="0.9"/>
	<path fill="#040404" d="M181.9,42.7c-0.5,0-0.9,0.4-0.9,0.9c0,0.5,0.4,0.9,0.9,0.9c0.2,0,0.4-0.1,0.6-0.2c0.1-0.2,0.2-0.4,0.3-0.6
		c0,0,0-0.1,0-0.1C182.8,43.1,182.4,42.7,181.9,42.7z"/>
	<path fill="#040404" d="M39,31.2c0.5,0,0.9-0.4,0.9-0.9c0-0.5-0.4-0.9-0.9-0.9c-0.1,0-0.1,0-0.2,0c-0.2,0.3-0.5,0.6-0.7,0.9
		C38.2,30.8,38.6,31.2,39,31.2z"/>
	<path fill="#040404" d="M51.1,30.3c0,0.3,0.2,0.6,0.4,0.7c0.3-0.5,0.6-1,0.9-1.4c-0.1-0.1-0.3-0.2-0.5-0.2
		C51.5,29.5,51.1,29.8,51.1,30.3z"/>
	<circle fill="#040404" cx="181.9" cy="30.3" r="0.9"/>
	<circle fill="#040404" cx="52" cy="17.4" r="0.9"/>
	<circle fill="#040404" cx="194.9" cy="17.4" r="0.9"/>
	<circle fill="#040404" cx="65" cy="4" r="0.9"/>
	<circle fill="#040404" cx="207.9" cy="4" r="0.9"/>
</g>
</svg>';

		return $html;
	}
}

if ( ! function_exists( 'borgholm_link_svg' ) ) {
	function borgholm_link_svg() {

		$html = '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="183px" height="197px" x="0px" y="0px"
	 viewBox="0 0 190 201" enable-background="new 0 0 190 201" xml:space="preserve">
<g>
	<path fill="none" d="M182.5,144.3c-3.4-5-6.3-8.8-8.6-11.5c-1-1.2-2.2-2.5-3.7-4.1c-0.1,0.1-0.2,0.1-0.3,0.1
		c-0.3,0-0.5-0.2-0.5-0.5c0-0.1,0.1-0.3,0.1-0.3c-1.7-1.8-3.7-3.8-6-6.1l-0.5-0.5c0,0,0,0,0,0c0,0.3-0.2,0.5-0.5,0.5
		s-0.5-0.2-0.5-0.5s0.2-0.5,0.5-0.5c0,0,0,0,0,0l-6.9-6.9c0,0,0,0,0,0.1c0,0.3-0.2,0.5-0.5,0.5s-0.5-0.2-0.5-0.5s0.2-0.5,0.5-0.5
		c0,0,0,0,0.1,0l-7-6.9c-0.1,0.2-0.2,0.4-0.5,0.4c-0.3,0-0.5-0.2-0.5-0.5c0-0.2,0.2-0.4,0.4-0.5l-6.8-6.7c0,0.2-0.2,0.4-0.5,0.4
		c-0.3,0-0.5-0.2-0.5-0.5c0-0.2,0.2-0.4,0.4-0.5l-6.8-6.7c0,0.3-0.2,0.5-0.5,0.5c-0.3,0-0.5-0.2-0.5-0.5c0-0.3,0.2-0.5,0.5-0.5
		l-6.8-6.8c0,0.3-0.2,0.5-0.5,0.5c-0.3,0-0.5-0.2-0.5-0.5c0-0.3,0.2-0.5,0.5-0.5l-6.9-6.9c-0.1,0.2-0.3,0.3-0.5,0.3
		c-0.3,0-0.5-0.2-0.5-0.5c0-0.2,0.1-0.4,0.3-0.5l-6.7-6.7c-0.1,0.2-0.2,0.3-0.5,0.3c-0.3,0-0.5-0.2-0.5-0.5c0-0.2,0.1-0.4,0.3-0.5
		l-6.7-6.7c0,0,0,0.1,0,0.1c0,0.3-0.2,0.5-0.5,0.5s-0.5-0.2-0.5-0.5s0.2-0.5,0.5-0.5c0,0,0.1,0,0.1,0l-7-7c0,0,0,0.1,0,0.1
		c0,0.3-0.2,0.5-0.5,0.5s-0.5-0.2-0.5-0.5S96,55,96.2,55c0,0,0.1,0,0.1,0l-7-7c0,0.2-0.2,0.4-0.5,0.4c-0.3,0-0.5-0.2-0.5-0.5
		c0-0.2,0.2-0.4,0.4-0.5l-3.4-3.4c-1.1-1.2-2.3-2.3-3.5-3.2c-0.1,0.2-0.2,0.3-0.4,0.3c-0.3,0-0.5-0.2-0.5-0.5c0-0.2,0.1-0.3,0.2-0.4
		c-4.4-3.5-9.1-5.7-14-6.6c-0.1,0.1-0.2,0.2-0.4,0.2c-0.2,0-0.4-0.1-0.5-0.3c-0.6-0.1-1.2-0.1-1.8-0.2c-1.6-0.1-3.1-0.1-4.6,0.1
		c0,0.2-0.2,0.4-0.5,0.4c-0.2,0-0.4-0.1-0.4-0.3c-5.2,0.8-10,3.3-14.1,7.6c-4.1,4.2-6.5,8.7-7.4,13.8c0.2,0.1,0.4,0.2,0.4,0.5
		c0,0.3-0.2,0.5-0.5,0.5c0,0,0,0,0,0c-0.2,1.6-0.3,3.3-0.1,5c0,0.4,0.1,0.9,0.1,1.3c0,0,0,0,0,0c0.3,0,0.5,0.2,0.5,0.5
		c0,0.2-0.2,0.4-0.4,0.5c1,6.6,4.4,12.8,10.1,18.8l70.1,70.1c3.1,2.8,6,2.7,8.8-0.2c2.8-2.9,2.8-5.8,0-8.6l-0.2-0.2
		c0,0.1,0,0.1,0,0.2c0,0.3-0.2,0.5-0.5,0.5s-0.5-0.2-0.5-0.5c0-0.3,0.2-0.5,0.5-0.5c0.1,0,0.1,0,0.2,0l-7.2-7.2
		c0.1,0.1,0.1,0.2,0.1,0.3c0,0.3-0.2,0.5-0.5,0.5s-0.5-0.2-0.5-0.5c0-0.3,0.2-0.5,0.5-0.5c0.1,0,0.3,0.1,0.3,0.1l-7.2-7.2
		c0,0.2-0.2,0.4-0.5,0.4c-0.3,0-0.5-0.2-0.5-0.5c0-0.3,0.2-0.5,0.4-0.5L56.3,73.2c-3.6-3.6-5.7-7.3-6.5-11.1s0.5-7.9,3.8-12.3
		c4.1-3.3,8.1-4.6,12.1-3.8c4,0.8,7.7,2.9,11.3,6.5l77.8,78.1c7.9,7.9,13.5,14.3,16.9,19.2c3.3,4.9,4.9,9.5,4.8,13.8
		c-0.1,4.3-2.5,8.7-7.1,13c-6.6,6.6-13.8,9.1-21.4,7.5c-7.7-1.7-16.9-8-27.6-19L33.3,78.6c-4.9-5.1-8.7-10.6-11.7-16.5
		c-2.9-5.9-4-12.3-3.3-19.3c0.8-7,4-13.5,9.6-19.3c5.1-5.1,10.6-8.2,16.5-9.4c5.9-1.1,11.4-0.5,16.5,1.9c1.9,0.9,3.7,1.8,5.4,2.7
		c0,0,0-0.1,0-0.1c0-0.3,0.2-0.5,0.5-0.5s0.5,0.2,0.5,0.5c0,0.2-0.1,0.4-0.3,0.5c2.4,1.3,4.6,2.6,6.4,3.8c3.2,2.2,6.1,4.7,8.6,7.5
		l79.7,79.3c2.8,3.1,5.7,3.1,8.6,0.2c2.9-2.9,3-5.8,0.2-8.6L90.8,21.5c-12-12-24.4-18.6-37.2-19.9c-5.6-0.6-11,0-16.1,1.6
		c0.2,0.1,0.3,0.2,0.3,0.4c0,0.3-0.2,0.5-0.5,0.5s-0.5-0.2-0.5-0.5c0-0.1,0-0.1,0-0.2c-4.8,1.7-9.4,4.3-13.8,8
		c-0.1,0.2-0.2,0.3-0.4,0.4c-1.1,0.9-2.1,1.9-3.1,2.9C13.9,20.2,10,26.4,7.8,32.9c0,0,0.1,0,0.1,0c0.3,0,0.5,0.2,0.5,0.5
		s-0.2,0.5-0.5,0.5c-0.1,0-0.3-0.1-0.4-0.2c-1.6,5.1-2.2,10.5-1.8,16.1C6.7,62,12.2,73.6,22.1,84.5c0.1-0.2,0.3-0.3,0.5-0.3
		c0.3,0,0.5,0.2,0.5,0.5c0,0.2-0.2,0.4-0.4,0.5c0.7,0.8,1.4,1.5,2.2,2.2l4.6,4.6c0-0.3,0.2-0.5,0.5-0.5c0.3,0,0.5,0.2,0.5,0.5
		c0,0.3-0.2,0.5-0.5,0.5l6.9,6.9c0,0,0-0.1,0-0.1c0-0.3,0.2-0.5,0.5-0.5s0.5,0.2,0.5,0.5s-0.2,0.5-0.5,0.5c0,0-0.1,0-0.1,0l7,7
		c0-0.1,0-0.1,0-0.2c0-0.3,0.2-0.5,0.5-0.5c0.3,0,0.5,0.2,0.5,0.5S45,107,44.7,107c-0.1,0-0.1,0-0.2,0l7.1,7.1
		c0-0.3,0.2-0.5,0.5-0.5c0.3,0,0.5,0.2,0.5,0.5c0,0.3-0.2,0.5-0.5,0.5l6.9,6.9c0,0,0,0,0-0.1c0-0.3,0.2-0.5,0.5-0.5s0.5,0.2,0.5,0.5
		s-0.2,0.5-0.5,0.5c0,0,0,0-0.1,0l14.4,14.4c-0.1-0.1-0.1-0.2-0.1-0.3c0-0.3,0.2-0.5,0.5-0.5s0.5,0.2,0.5,0.5c0,0.3-0.2,0.5-0.5,0.5
		c-0.1,0-0.2,0-0.3-0.1l14.5,14.5c0,0,0-0.1,0-0.1c0-0.3,0.2-0.5,0.5-0.5s0.5,0.2,0.5,0.5c0,0.3-0.2,0.5-0.5,0.5c0,0-0.1,0-0.1,0
		l7,7c0-0.1,0-0.1,0-0.2c0-0.3,0.2-0.5,0.5-0.5s0.5,0.2,0.5,0.5c0,0.3-0.2,0.5-0.5,0.5c-0.1,0-0.1,0-0.2,0l15.4,15.4
		c2.2,2.2,4.3,4.2,6.4,6c0.1-0.2,0.3-0.3,0.5-0.3c0.3,0,0.5,0.2,0.5,0.5c0,0.2-0.1,0.3-0.3,0.4c7.5,6.5,14.7,11.1,21.4,13.7
		c0.1-0.2,0.3-0.3,0.5-0.3c0.3,0,0.5,0.2,0.5,0.5c0,0.1,0,0.1,0,0.2c4.1,1.5,8.1,2.2,11.9,2.2c8.9,0,17.4-3.8,25.3-11.5
		c4.6-4.8,7.7-9.8,9.2-14.7c1.5-5,2-9.4,1.3-13.2C188,153.6,186,149.2,182.5,144.3z M44.7,77.6c-0.3,0-0.5-0.2-0.5-0.5
		s0.2-0.5,0.5-0.5c0.3,0,0.5,0.2,0.5,0.5S45,77.6,44.7,77.6z M44.7,70.3c-0.3,0-0.5-0.2-0.5-0.5s0.2-0.5,0.5-0.5
		c0.3,0,0.5,0.2,0.5,0.5S45,70.3,44.7,70.3z M44.7,63.3c-0.3,0-0.5-0.2-0.5-0.5s0.2-0.5,0.5-0.5c0.3,0,0.5,0.2,0.5,0.5
		S45,63.3,44.7,63.3z M44.7,56c-0.3,0-0.5-0.2-0.5-0.5s0.2-0.5,0.5-0.5c0.3,0,0.5,0.2,0.5,0.5S45,56,44.7,56z M44.7,48.4
		c-0.3,0-0.5-0.2-0.5-0.5s0.2-0.5,0.5-0.5c0.3,0,0.5,0.2,0.5,0.5S45,48.4,44.7,48.4z M125.7,150.3c0.3,0,0.5,0.2,0.5,0.5
		c0,0.3-0.2,0.5-0.5,0.5s-0.5-0.2-0.5-0.5C125.2,150.5,125.4,150.3,125.7,150.3z M118.3,142.7c0.3,0,0.5,0.2,0.5,0.5
		c0,0.3-0.2,0.5-0.5,0.5s-0.5-0.2-0.5-0.5C117.8,143,118.1,142.7,118.3,142.7z M118.3,150.3c0.3,0,0.5,0.2,0.5,0.5
		c0,0.3-0.2,0.5-0.5,0.5s-0.5-0.2-0.5-0.5C117.8,150.5,118.1,150.3,118.3,150.3z M111,135.4c0.3,0,0.5,0.2,0.5,0.5
		c0,0.3-0.2,0.5-0.5,0.5s-0.5-0.2-0.5-0.5C110.5,135.7,110.7,135.4,111,135.4z M111,142.7c0.3,0,0.5,0.2,0.5,0.5
		c0,0.3-0.2,0.5-0.5,0.5s-0.5-0.2-0.5-0.5C110.5,143,110.7,142.7,111,142.7z M103.6,120.9c0.3,0,0.5,0.2,0.5,0.5s-0.2,0.5-0.5,0.5
		s-0.5-0.2-0.5-0.5S103.3,120.9,103.6,120.9z M103.6,127.8c0.3,0,0.5,0.2,0.5,0.5s-0.2,0.5-0.5,0.5s-0.5-0.2-0.5-0.5
		S103.3,127.8,103.6,127.8z M103.6,135.4c0.3,0,0.5,0.2,0.5,0.5c0,0.3-0.2,0.5-0.5,0.5s-0.5-0.2-0.5-0.5
		C103.1,135.7,103.3,135.4,103.6,135.4z M96.2,113.6c0.3,0,0.5,0.2,0.5,0.5s-0.2,0.5-0.5,0.5s-0.5-0.2-0.5-0.5S96,113.6,96.2,113.6z
		 M96.2,120.9c0.3,0,0.5,0.2,0.5,0.5s-0.2,0.5-0.5,0.5s-0.5-0.2-0.5-0.5S96,120.9,96.2,120.9z M96.2,127.8c0.3,0,0.5,0.2,0.5,0.5
		s-0.2,0.5-0.5,0.5s-0.5-0.2-0.5-0.5S96,127.8,96.2,127.8z M88.9,106c0.3,0,0.5,0.2,0.5,0.5s-0.2,0.5-0.5,0.5s-0.5-0.2-0.5-0.5
		S88.6,106,88.9,106z M88.9,113.6c0.3,0,0.5,0.2,0.5,0.5s-0.2,0.5-0.5,0.5s-0.5-0.2-0.5-0.5S88.6,113.6,88.9,113.6z M88.9,120.9
		c0.3,0,0.5,0.2,0.5,0.5s-0.2,0.5-0.5,0.5s-0.5-0.2-0.5-0.5S88.6,120.9,88.9,120.9z M81.5,98.7c0.3,0,0.5,0.2,0.5,0.5
		s-0.2,0.5-0.5,0.5c-0.3,0-0.5-0.2-0.5-0.5S81.2,98.7,81.5,98.7z M81.5,106c0.3,0,0.5,0.2,0.5,0.5s-0.2,0.5-0.5,0.5
		c-0.3,0-0.5-0.2-0.5-0.5S81.2,106,81.5,106z M81.5,113.6c0.3,0,0.5,0.2,0.5,0.5s-0.2,0.5-0.5,0.5c-0.3,0-0.5-0.2-0.5-0.5
		S81.2,113.6,81.5,113.6z M74.1,91.5c0.3,0,0.5,0.2,0.5,0.5s-0.2,0.5-0.5,0.5s-0.5-0.2-0.5-0.5S73.9,91.5,74.1,91.5z M74.1,98.7
		c0.3,0,0.5,0.2,0.5,0.5s-0.2,0.5-0.5,0.5s-0.5-0.2-0.5-0.5S73.9,98.7,74.1,98.7z M74.1,106c0.3,0,0.5,0.2,0.5,0.5s-0.2,0.5-0.5,0.5
		s-0.5-0.2-0.5-0.5S73.9,106,74.1,106z M66.8,84.2c0.3,0,0.5,0.2,0.5,0.5s-0.2,0.5-0.5,0.5s-0.5-0.2-0.5-0.5S66.5,84.2,66.8,84.2z
		 M66.8,91.5c0.3,0,0.5,0.2,0.5,0.5s-0.2,0.5-0.5,0.5s-0.5-0.2-0.5-0.5S66.5,91.5,66.8,91.5z M66.8,98.7c0.3,0,0.5,0.2,0.5,0.5
		s-0.2,0.5-0.5,0.5s-0.5-0.2-0.5-0.5S66.5,98.7,66.8,98.7z M59.4,76.6c0.3,0,0.5,0.2,0.5,0.5s-0.2,0.5-0.5,0.5s-0.5-0.2-0.5-0.5
		S59.1,76.6,59.4,76.6z M59.4,84.2c0.3,0,0.5,0.2,0.5,0.5s-0.2,0.5-0.5,0.5s-0.5-0.2-0.5-0.5S59.1,84.2,59.4,84.2z M59.4,91.5
		c0.3,0,0.5,0.2,0.5,0.5s-0.2,0.5-0.5,0.5s-0.5-0.2-0.5-0.5S59.1,91.5,59.4,91.5z M52,69.3c0.3,0,0.5,0.2,0.5,0.5s-0.2,0.5-0.5,0.5
		s-0.5-0.2-0.5-0.5S51.8,69.3,52,69.3z M52,76.6c0.3,0,0.5,0.2,0.5,0.5s-0.2,0.5-0.5,0.5s-0.5-0.2-0.5-0.5S51.8,76.6,52,76.6z
		 M52,84.2c0.3,0,0.5,0.2,0.5,0.5s-0.2,0.5-0.5,0.5s-0.5-0.2-0.5-0.5S51.8,84.2,52,84.2z M52,48.4c-0.3,0-0.5-0.2-0.5-0.5
		s0.2-0.5,0.5-0.5s0.5,0.2,0.5,0.5S52.3,48.4,52,48.4z M52,41.1c-0.3,0-0.5-0.2-0.5-0.5s0.2-0.5,0.5-0.5s0.5,0.2,0.5,0.5
		S52.3,41.1,52,41.1z M59.4,41.1c-0.3,0-0.5-0.2-0.5-0.5s0.2-0.5,0.5-0.5s0.5,0.2,0.5,0.5S59.7,41.1,59.4,41.1z M66.8,41.1
		c-0.3,0-0.5-0.2-0.5-0.5s0.2-0.5,0.5-0.5s0.5,0.2,0.5,0.5S67.1,41.1,66.8,41.1z M74.1,48.4c-0.3,0-0.5-0.2-0.5-0.5s0.2-0.5,0.5-0.5
		s0.5,0.2,0.5,0.5S74.4,48.4,74.1,48.4z M74.1,41.1c-0.3,0-0.5-0.2-0.5-0.5s0.2-0.5,0.5-0.5s0.5,0.2,0.5,0.5S74.4,41.1,74.1,41.1z
		 M81.5,56c-0.3,0-0.5-0.2-0.5-0.5s0.2-0.5,0.5-0.5c0.3,0,0.5,0.2,0.5,0.5S81.8,56,81.5,56z M81.5,48.4c-0.3,0-0.5-0.2-0.5-0.5
		s0.2-0.5,0.5-0.5c0.3,0,0.5,0.2,0.5,0.5S81.8,48.4,81.5,48.4z M88.9,63.3c-0.3,0-0.5-0.2-0.5-0.5s0.2-0.5,0.5-0.5s0.5,0.2,0.5,0.5
		S89.1,63.3,88.9,63.3z M88.9,56c-0.3,0-0.5-0.2-0.5-0.5s0.2-0.5,0.5-0.5s0.5,0.2,0.5,0.5S89.1,56,88.9,56z M96.2,70.3
		c-0.3,0-0.5-0.2-0.5-0.5s0.2-0.5,0.5-0.5s0.5,0.2,0.5,0.5S96.5,70.3,96.2,70.3z M96.2,63.3c-0.3,0-0.5-0.2-0.5-0.5s0.2-0.5,0.5-0.5
		s0.5,0.2,0.5,0.5S96.5,63.3,96.2,63.3z M103.6,77.6c-0.3,0-0.5-0.2-0.5-0.5s0.2-0.5,0.5-0.5s0.5,0.2,0.5,0.5S103.9,77.6,103.6,77.6
		z M103.6,70.3c-0.3,0-0.5-0.2-0.5-0.5s0.2-0.5,0.5-0.5s0.5,0.2,0.5,0.5S103.9,70.3,103.6,70.3z M111,85.2c-0.3,0-0.5-0.2-0.5-0.5
		s0.2-0.5,0.5-0.5s0.5,0.2,0.5,0.5S111.2,85.2,111,85.2z M111,77.6c-0.3,0-0.5-0.2-0.5-0.5s0.2-0.5,0.5-0.5s0.5,0.2,0.5,0.5
		S111.2,77.6,111,77.6z M118.3,92.5c-0.3,0-0.5-0.2-0.5-0.5s0.2-0.5,0.5-0.5s0.5,0.2,0.5,0.5S118.6,92.5,118.3,92.5z M118.3,85.2
		c-0.3,0-0.5-0.2-0.5-0.5s0.2-0.5,0.5-0.5s0.5,0.2,0.5,0.5S118.6,85.2,118.3,85.2z M125.7,99.7c-0.3,0-0.5-0.2-0.5-0.5
		s0.2-0.5,0.5-0.5s0.5,0.2,0.5,0.5S126,99.7,125.7,99.7z M125.7,92.5c-0.3,0-0.5-0.2-0.5-0.5s0.2-0.5,0.5-0.5s0.5,0.2,0.5,0.5
		S126,92.5,125.7,92.5z M133.1,107c-0.3,0-0.5-0.2-0.5-0.5s0.2-0.5,0.5-0.5s0.5,0.2,0.5,0.5S133.3,107,133.1,107z M133.1,99.7
		c-0.3,0-0.5-0.2-0.5-0.5s0.2-0.5,0.5-0.5s0.5,0.2,0.5,0.5S133.3,99.7,133.1,99.7z M140.4,114.6c-0.3,0-0.5-0.2-0.5-0.5
		s0.2-0.5,0.5-0.5s0.5,0.2,0.5,0.5S140.7,114.6,140.4,114.6z M140.4,107c-0.3,0-0.5-0.2-0.5-0.5s0.2-0.5,0.5-0.5s0.5,0.2,0.5,0.5
		S140.7,107,140.4,107z M147.8,121.9c-0.3,0-0.5-0.2-0.5-0.5s0.2-0.5,0.5-0.5s0.5,0.2,0.5,0.5S148.1,121.9,147.8,121.9z
		 M147.8,114.6c-0.3,0-0.5-0.2-0.5-0.5s0.2-0.5,0.5-0.5s0.5,0.2,0.5,0.5S148.1,114.6,147.8,114.6z M155.2,128.8
		c-0.3,0-0.5-0.2-0.5-0.5s0.2-0.5,0.5-0.5s0.5,0.2,0.5,0.5S155.4,128.8,155.2,128.8z M155.2,121.9c-0.3,0-0.5-0.2-0.5-0.5
		s0.2-0.5,0.5-0.5s0.5,0.2,0.5,0.5S155.4,121.9,155.2,121.9z M162.5,136.4c-0.3,0-0.5-0.2-0.5-0.5c0-0.3,0.2-0.5,0.5-0.5
		s0.5,0.2,0.5,0.5C163,136.2,162.8,136.4,162.5,136.4z M162.5,128.8c-0.3,0-0.5-0.2-0.5-0.5s0.2-0.5,0.5-0.5s0.5,0.2,0.5,0.5
		S162.8,128.8,162.5,128.8z M169.9,143.7c-0.3,0-0.5-0.2-0.5-0.5c0-0.3,0.2-0.5,0.5-0.5s0.5,0.2,0.5,0.5
		C170.4,143.5,170.2,143.7,169.9,143.7z M169.9,136.4c-0.3,0-0.5-0.2-0.5-0.5c0-0.3,0.2-0.5,0.5-0.5s0.5,0.2,0.5,0.5
		C170.4,136.2,170.2,136.4,169.9,136.4z M169.9,106c0.3,0,0.5,0.2,0.5,0.5s-0.2,0.5-0.5,0.5s-0.5-0.2-0.5-0.5S169.6,106,169.9,106z
		 M162.5,98.7c0.3,0,0.5,0.2,0.5,0.5s-0.2,0.5-0.5,0.5s-0.5-0.2-0.5-0.5S162.2,98.7,162.5,98.7z M162.5,106c0.3,0,0.5,0.2,0.5,0.5
		s-0.2,0.5-0.5,0.5s-0.5-0.2-0.5-0.5S162.2,106,162.5,106z M155.2,91.5c0.3,0,0.5,0.2,0.5,0.5s-0.2,0.5-0.5,0.5s-0.5-0.2-0.5-0.5
		S154.9,91.5,155.2,91.5z M155.2,98.7c0.3,0,0.5,0.2,0.5,0.5s-0.2,0.5-0.5,0.5s-0.5-0.2-0.5-0.5S154.9,98.7,155.2,98.7z M147.8,84.2
		c0.3,0,0.5,0.2,0.5,0.5s-0.2,0.5-0.5,0.5s-0.5-0.2-0.5-0.5S147.5,84.2,147.8,84.2z M147.8,91.5c0.3,0,0.5,0.2,0.5,0.5
		s-0.2,0.5-0.5,0.5s-0.5-0.2-0.5-0.5S147.5,91.5,147.8,91.5z M140.4,76.6c0.3,0,0.5,0.2,0.5,0.5s-0.2,0.5-0.5,0.5s-0.5-0.2-0.5-0.5
		S140.1,76.6,140.4,76.6z M140.4,84.2c0.3,0,0.5,0.2,0.5,0.5s-0.2,0.5-0.5,0.5s-0.5-0.2-0.5-0.5S140.1,84.2,140.4,84.2z M133.1,69.3
		c0.3,0,0.5,0.2,0.5,0.5s-0.2,0.5-0.5,0.5s-0.5-0.2-0.5-0.5S132.8,69.3,133.1,69.3z M133.1,76.6c0.3,0,0.5,0.2,0.5,0.5
		s-0.2,0.5-0.5,0.5s-0.5-0.2-0.5-0.5S132.8,76.6,133.1,76.6z M125.7,62.3c0.3,0,0.5,0.2,0.5,0.5s-0.2,0.5-0.5,0.5s-0.5-0.2-0.5-0.5
		S125.4,62.3,125.7,62.3z M125.7,69.3c0.3,0,0.5,0.2,0.5,0.5s-0.2,0.5-0.5,0.5s-0.5-0.2-0.5-0.5S125.4,69.3,125.7,69.3z M118.3,55
		c0.3,0,0.5,0.2,0.5,0.5s-0.2,0.5-0.5,0.5s-0.5-0.2-0.5-0.5S118.1,55,118.3,55z M118.3,62.3c0.3,0,0.5,0.2,0.5,0.5s-0.2,0.5-0.5,0.5
		s-0.5-0.2-0.5-0.5S118.1,62.3,118.3,62.3z M111,47.4c0.3,0,0.5,0.2,0.5,0.5s-0.2,0.5-0.5,0.5s-0.5-0.2-0.5-0.5S110.7,47.4,111,47.4
		z M111,55c0.3,0,0.5,0.2,0.5,0.5S111.2,56,111,56s-0.5-0.2-0.5-0.5S110.7,55,111,55z M103.6,40.1c0.3,0,0.5,0.2,0.5,0.5
		s-0.2,0.5-0.5,0.5s-0.5-0.2-0.5-0.5S103.3,40.1,103.6,40.1z M103.6,47.4c0.3,0,0.5,0.2,0.5,0.5s-0.2,0.5-0.5,0.5s-0.5-0.2-0.5-0.5
		S103.3,47.4,103.6,47.4z M96.2,32.9c0.3,0,0.5,0.2,0.5,0.5s-0.2,0.5-0.5,0.5s-0.5-0.2-0.5-0.5S96,32.9,96.2,32.9z M96.2,40.1
		c0.3,0,0.5,0.2,0.5,0.5s-0.2,0.5-0.5,0.5s-0.5-0.2-0.5-0.5S96,40.1,96.2,40.1z M88.9,25.6c0.3,0,0.5,0.2,0.5,0.5s-0.2,0.5-0.5,0.5
		s-0.5-0.2-0.5-0.5S88.6,25.6,88.9,25.6z M88.9,32.9c0.3,0,0.5,0.2,0.5,0.5s-0.2,0.5-0.5,0.5s-0.5-0.2-0.5-0.5S88.6,32.9,88.9,32.9z
		 M81.5,18.1c0.3,0,0.5,0.2,0.5,0.5s-0.2,0.5-0.5,0.5c-0.3,0-0.5-0.2-0.5-0.5S81.2,18.1,81.5,18.1z M81.5,25.6
		c0.3,0,0.5,0.2,0.5,0.5s-0.2,0.5-0.5,0.5c-0.3,0-0.5-0.2-0.5-0.5S81.2,25.6,81.5,25.6z M74.1,10.8c0.3,0,0.5,0.2,0.5,0.5
		s-0.2,0.5-0.5,0.5s-0.5-0.2-0.5-0.5S73.9,10.8,74.1,10.8z M74.1,18.1c0.3,0,0.5,0.2,0.5,0.5s-0.2,0.5-0.5,0.5s-0.5-0.2-0.5-0.5
		S73.9,18.1,74.1,18.1z M66.8,10.8c0.3,0,0.5,0.2,0.5,0.5s-0.2,0.5-0.5,0.5s-0.5-0.2-0.5-0.5S66.5,10.8,66.8,10.8z M59.4,3.1
		c0.3,0,0.5,0.2,0.5,0.5s-0.2,0.5-0.5,0.5s-0.5-0.2-0.5-0.5S59.1,3.1,59.4,3.1z M59.4,10.8c0.3,0,0.5,0.2,0.5,0.5s-0.2,0.5-0.5,0.5
		s-0.5-0.2-0.5-0.5S59.1,10.8,59.4,10.8z M52,3.1c0.3,0,0.5,0.2,0.5,0.5S52.3,4.1,52,4.1s-0.5-0.2-0.5-0.5S51.8,3.1,52,3.1z
		 M52,10.8c0.3,0,0.5,0.2,0.5,0.5s-0.2,0.5-0.5,0.5s-0.5-0.2-0.5-0.5S51.8,10.8,52,10.8z M44.7,3.1c0.3,0,0.5,0.2,0.5,0.5
		S45,4.1,44.7,4.1c-0.3,0-0.5-0.2-0.5-0.5S44.4,3.1,44.7,3.1z M44.7,10.8c0.3,0,0.5,0.2,0.5,0.5s-0.2,0.5-0.5,0.5
		c-0.3,0-0.5-0.2-0.5-0.5S44.4,10.8,44.7,10.8z M37.3,10.8c0.3,0,0.5,0.2,0.5,0.5s-0.2,0.5-0.5,0.5s-0.5-0.2-0.5-0.5
		S37,10.8,37.3,10.8z M30,10.8c0.3,0,0.5,0.2,0.5,0.5s-0.2,0.5-0.5,0.5s-0.5-0.2-0.5-0.5S29.7,10.8,30,10.8z M30,18.1
		c0.3,0,0.5,0.2,0.5,0.5s-0.2,0.5-0.5,0.5s-0.5-0.2-0.5-0.5S29.7,18.1,30,18.1z M22.6,18.1c0.3,0,0.5,0.2,0.5,0.5s-0.2,0.5-0.5,0.5
		s-0.5-0.2-0.5-0.5S22.3,18.1,22.6,18.1z M22.6,25.6c0.3,0,0.5,0.2,0.5,0.5s-0.2,0.5-0.5,0.5s-0.5-0.2-0.5-0.5S22.3,25.6,22.6,25.6z
		 M7.9,56c-0.3,0-0.5-0.2-0.5-0.5S7.6,55,7.9,55c0.3,0,0.5,0.2,0.5,0.5S8.1,56,7.9,56z M7.9,48.4c-0.3,0-0.5-0.2-0.5-0.5
		s0.2-0.5,0.5-0.5c0.3,0,0.5,0.2,0.5,0.5S8.1,48.4,7.9,48.4z M7.9,41.1c-0.3,0-0.5-0.2-0.5-0.5s0.2-0.5,0.5-0.5
		c0.3,0,0.5,0.2,0.5,0.5S8.1,41.1,7.9,41.1z M15.2,70.3c-0.3,0-0.5-0.2-0.5-0.5s0.2-0.5,0.5-0.5s0.5,0.2,0.5,0.5
		S15.5,70.3,15.2,70.3z M15.2,63.3c-0.3,0-0.5-0.2-0.5-0.5s0.2-0.5,0.5-0.5s0.5,0.2,0.5,0.5S15.5,63.3,15.2,63.3z M15.2,56
		c-0.3,0-0.5-0.2-0.5-0.5S15,55,15.2,55s0.5,0.2,0.5,0.5S15.5,56,15.2,56z M15.2,48.4c-0.3,0-0.5-0.2-0.5-0.5s0.2-0.5,0.5-0.5
		s0.5,0.2,0.5,0.5S15.5,48.4,15.2,48.4z M15.2,41.1c-0.3,0-0.5-0.2-0.5-0.5s0.2-0.5,0.5-0.5s0.5,0.2,0.5,0.5S15.5,41.1,15.2,41.1z
		 M15.2,33.9c-0.3,0-0.5-0.2-0.5-0.5s0.2-0.5,0.5-0.5s0.5,0.2,0.5,0.5S15.5,33.9,15.2,33.9z M15.2,26.6c-0.3,0-0.5-0.2-0.5-0.5
		s0.2-0.5,0.5-0.5s0.5,0.2,0.5,0.5S15.5,26.6,15.2,26.6z M22.6,77.6c-0.3,0-0.5-0.2-0.5-0.5s0.2-0.5,0.5-0.5s0.5,0.2,0.5,0.5
		S22.9,77.6,22.6,77.6z M22.6,70.3c-0.3,0-0.5-0.2-0.5-0.5s0.2-0.5,0.5-0.5s0.5,0.2,0.5,0.5S22.9,70.3,22.6,70.3z M30,85.2
		c-0.3,0-0.5-0.2-0.5-0.5s0.2-0.5,0.5-0.5s0.5,0.2,0.5,0.5S30.2,85.2,30,85.2z M30,77.6c-0.3,0-0.5-0.2-0.5-0.5s0.2-0.5,0.5-0.5
		s0.5,0.2,0.5,0.5S30.2,77.6,30,77.6z M37.3,92.5c-0.3,0-0.5-0.2-0.5-0.5s0.2-0.5,0.5-0.5s0.5,0.2,0.5,0.5S37.6,92.5,37.3,92.5z
		 M37.3,85.2c-0.3,0-0.5-0.2-0.5-0.5s0.2-0.5,0.5-0.5s0.5,0.2,0.5,0.5S37.6,85.2,37.3,85.2z M44.7,99.7c-0.3,0-0.5-0.2-0.5-0.5
		s0.2-0.5,0.5-0.5c0.3,0,0.5,0.2,0.5,0.5S45,99.7,44.7,99.7z M44.7,92.5c-0.3,0-0.5-0.2-0.5-0.5s0.2-0.5,0.5-0.5
		c0.3,0,0.5,0.2,0.5,0.5S45,92.5,44.7,92.5z M52,107c-0.3,0-0.5-0.2-0.5-0.5s0.2-0.5,0.5-0.5s0.5,0.2,0.5,0.5S52.3,107,52,107z
		 M52,99.7c-0.3,0-0.5-0.2-0.5-0.5s0.2-0.5,0.5-0.5s0.5,0.2,0.5,0.5S52.3,99.7,52,99.7z M59.4,114.6c-0.3,0-0.5-0.2-0.5-0.5
		s0.2-0.5,0.5-0.5s0.5,0.2,0.5,0.5S59.7,114.6,59.4,114.6z M59.4,107c-0.3,0-0.5-0.2-0.5-0.5s0.2-0.5,0.5-0.5s0.5,0.2,0.5,0.5
		S59.7,107,59.4,107z M66.8,128.8c-0.3,0-0.5-0.2-0.5-0.5s0.2-0.5,0.5-0.5s0.5,0.2,0.5,0.5S67.1,128.8,66.8,128.8z M66.8,121.9
		c-0.3,0-0.5-0.2-0.5-0.5s0.2-0.5,0.5-0.5s0.5,0.2,0.5,0.5S67.1,121.9,66.8,121.9z M66.8,114.6c-0.3,0-0.5-0.2-0.5-0.5
		s0.2-0.5,0.5-0.5s0.5,0.2,0.5,0.5S67.1,114.6,66.8,114.6z M74.1,128.8c-0.3,0-0.5-0.2-0.5-0.5s0.2-0.5,0.5-0.5s0.5,0.2,0.5,0.5
		S74.4,128.8,74.1,128.8z M74.1,121.9c-0.3,0-0.5-0.2-0.5-0.5s0.2-0.5,0.5-0.5s0.5,0.2,0.5,0.5S74.4,121.9,74.1,121.9z M81.5,143.7
		c-0.3,0-0.5-0.2-0.5-0.5c0-0.3,0.2-0.5,0.5-0.5c0.3,0,0.5,0.2,0.5,0.5C82,143.5,81.8,143.7,81.5,143.7z M81.5,136.4
		c-0.3,0-0.5-0.2-0.5-0.5c0-0.3,0.2-0.5,0.5-0.5c0.3,0,0.5,0.2,0.5,0.5C82,136.2,81.8,136.4,81.5,136.4z M81.5,128.8
		c-0.3,0-0.5-0.2-0.5-0.5s0.2-0.5,0.5-0.5c0.3,0,0.5,0.2,0.5,0.5S81.8,128.8,81.5,128.8z M88.9,143.7c-0.3,0-0.5-0.2-0.5-0.5
		c0-0.3,0.2-0.5,0.5-0.5s0.5,0.2,0.5,0.5C89.4,143.5,89.1,143.7,88.9,143.7z M88.9,136.4c-0.3,0-0.5-0.2-0.5-0.5
		c0-0.3,0.2-0.5,0.5-0.5s0.5,0.2,0.5,0.5C89.4,136.2,89.1,136.4,88.9,136.4z M96.2,151.3c-0.3,0-0.5-0.2-0.5-0.5
		c0-0.3,0.2-0.5,0.5-0.5s0.5,0.2,0.5,0.5C96.7,151.1,96.5,151.3,96.2,151.3z M96.2,143.7c-0.3,0-0.5-0.2-0.5-0.5
		c0-0.3,0.2-0.5,0.5-0.5s0.5,0.2,0.5,0.5C96.7,143.5,96.5,143.7,96.2,143.7z M103.6,165.8c-0.3,0-0.5-0.2-0.5-0.5s0.2-0.5,0.5-0.5
		s0.5,0.2,0.5,0.5S103.9,165.8,103.6,165.8z M103.6,158.6c-0.3,0-0.5-0.2-0.5-0.5c0-0.3,0.2-0.5,0.5-0.5s0.5,0.2,0.5,0.5
		C104.1,158.4,103.9,158.6,103.6,158.6z M103.6,151.3c-0.3,0-0.5-0.2-0.5-0.5c0-0.3,0.2-0.5,0.5-0.5s0.5,0.2,0.5,0.5
		C104.1,151.1,103.9,151.3,103.6,151.3z M111,173.1c-0.3,0-0.5-0.2-0.5-0.5s0.2-0.5,0.5-0.5s0.5,0.2,0.5,0.5S111.2,173.1,111,173.1z
		 M111,165.8c-0.3,0-0.5-0.2-0.5-0.5s0.2-0.5,0.5-0.5s0.5,0.2,0.5,0.5S111.2,165.8,111,165.8z M111,158.6c-0.3,0-0.5-0.2-0.5-0.5
		c0-0.3,0.2-0.5,0.5-0.5s0.5,0.2,0.5,0.5C111.5,158.4,111.2,158.6,111,158.6z M118.3,173.1c-0.3,0-0.5-0.2-0.5-0.5s0.2-0.5,0.5-0.5
		s0.5,0.2,0.5,0.5S118.6,173.1,118.3,173.1z M118.3,165.8c-0.3,0-0.5-0.2-0.5-0.5s0.2-0.5,0.5-0.5s0.5,0.2,0.5,0.5
		S118.6,165.8,118.3,165.8z M125.7,180.7c-0.3,0-0.5-0.2-0.5-0.5s0.2-0.5,0.5-0.5s0.5,0.2,0.5,0.5S126,180.7,125.7,180.7z
		 M125.7,173.1c-0.3,0-0.5-0.2-0.5-0.5s0.2-0.5,0.5-0.5s0.5,0.2,0.5,0.5S126,173.1,125.7,173.1z M133.1,188c-0.3,0-0.5-0.2-0.5-0.5
		s0.2-0.5,0.5-0.5s0.5,0.2,0.5,0.5S133.3,188,133.1,188z M133.1,180.7c-0.3,0-0.5-0.2-0.5-0.5s0.2-0.5,0.5-0.5s0.5,0.2,0.5,0.5
		S133.3,180.7,133.1,180.7z M140.4,188c-0.3,0-0.5-0.2-0.5-0.5s0.2-0.5,0.5-0.5s0.5,0.2,0.5,0.5S140.7,188,140.4,188z M147.8,195
		c-0.3,0-0.5-0.2-0.5-0.5s0.2-0.5,0.5-0.5s0.5,0.2,0.5,0.5S148.1,195,147.8,195z M147.8,188c-0.3,0-0.5-0.2-0.5-0.5s0.2-0.5,0.5-0.5
		s0.5,0.2,0.5,0.5S148.1,188,147.8,188z M155.2,195c-0.3,0-0.5-0.2-0.5-0.5s0.2-0.5,0.5-0.5s0.5,0.2,0.5,0.5S155.4,195,155.2,195z
		 M155.2,188c-0.3,0-0.5-0.2-0.5-0.5s0.2-0.5,0.5-0.5s0.5,0.2,0.5,0.5S155.4,188,155.2,188z M162.5,195c-0.3,0-0.5-0.2-0.5-0.5
		s0.2-0.5,0.5-0.5s0.5,0.2,0.5,0.5S162.8,195,162.5,195z M162.5,188c-0.3,0-0.5-0.2-0.5-0.5s0.2-0.5,0.5-0.5s0.5,0.2,0.5,0.5
		S162.8,188,162.5,188z M169.9,188c-0.3,0-0.5-0.2-0.5-0.5s0.2-0.5,0.5-0.5s0.5,0.2,0.5,0.5S170.2,188,169.9,188z M169.9,180.7
		c-0.3,0-0.5-0.2-0.5-0.5s0.2-0.5,0.5-0.5s0.5,0.2,0.5,0.5S170.2,180.7,169.9,180.7z M177.2,180.7c-0.3,0-0.5-0.2-0.5-0.5
		s0.2-0.5,0.5-0.5s0.5,0.2,0.5,0.5S177.5,180.7,177.2,180.7z M177.2,173.1c-0.3,0-0.5-0.2-0.5-0.5s0.2-0.5,0.5-0.5s0.5,0.2,0.5,0.5
		S177.5,173.1,177.2,173.1z M177.2,165.8c-0.3,0-0.5-0.2-0.5-0.5s0.2-0.5,0.5-0.5s0.5,0.2,0.5,0.5S177.5,165.8,177.2,165.8z
		 M177.2,158.6c-0.3,0-0.5-0.2-0.5-0.5c0-0.3,0.2-0.5,0.5-0.5s0.5,0.2,0.5,0.5C177.7,158.4,177.5,158.6,177.2,158.6z M177.2,151.3
		c-0.3,0-0.5-0.2-0.5-0.5c0-0.3,0.2-0.5,0.5-0.5s0.5,0.2,0.5,0.5C177.7,151.1,177.5,151.3,177.2,151.3z M177.2,143.7
		c-0.3,0-0.5-0.2-0.5-0.5c0-0.3,0.2-0.5,0.5-0.5s0.5,0.2,0.5,0.5C177.7,143.5,177.5,143.7,177.2,143.7z M184.6,173.1
		c-0.3,0-0.5-0.2-0.5-0.5s0.2-0.5,0.5-0.5c0.3,0,0.5,0.2,0.5,0.5S184.9,173.1,184.6,173.1z M184.6,165.8c-0.3,0-0.5-0.2-0.5-0.5
		s0.2-0.5,0.5-0.5c0.3,0,0.5,0.2,0.5,0.5S184.9,165.8,184.6,165.8z M184.6,158.6c-0.3,0-0.5-0.2-0.5-0.5c0-0.3,0.2-0.5,0.5-0.5
		c0.3,0,0.5,0.2,0.5,0.5C185.1,158.4,184.9,158.6,184.6,158.6z M184.6,151.3c-0.3,0-0.5-0.2-0.5-0.5c0-0.3,0.2-0.5,0.5-0.5
		c0.3,0,0.5,0.2,0.5,0.5C185.1,151.1,184.9,151.3,184.6,151.3z"/>
	<path fill="#040404" d="M59.9,121.4c0-0.3-0.2-0.5-0.5-0.5s-0.5,0.2-0.5,0.5c0,0,0,0,0,0.1l0.4,0.4c0,0,0,0,0.1,0
		C59.7,121.9,59.9,121.6,59.9,121.4z"/>
	<circle fill="#040404" cx="66.8" cy="121.4" r="0.5"/>
	<circle fill="#040404" cx="74.1" cy="121.4" r="0.5"/>
	<circle fill="#040404" cx="88.9" cy="121.4" r="0.5"/>
	<circle fill="#040404" cx="96.2" cy="121.4" r="0.5"/>
	<circle fill="#040404" cx="103.6" cy="121.4" r="0.5"/>
	<circle fill="#040404" cx="147.8" cy="121.4" r="0.5"/>
	<circle fill="#040404" cx="155.2" cy="121.4" r="0.5"/>
	<path fill="#040404" d="M162,121.4c0,0.3,0.2,0.5,0.5,0.5s0.5-0.2,0.5-0.5c0,0,0,0,0,0l-0.5-0.5c0,0,0,0,0,0
		C162.2,120.9,162,121.1,162,121.4z"/>
	<path fill="#040404" d="M52.5,114.1c0-0.3-0.2-0.5-0.5-0.5c-0.3,0-0.5,0.2-0.5,0.5l0.5,0.5C52.3,114.5,52.5,114.3,52.5,114.1z"/>
	<circle fill="#040404" cx="59.4" cy="114.1" r="0.5"/>
	<circle fill="#040404" cx="66.8" cy="114.1" r="0.5"/>
	<circle fill="#040404" cx="81.5" cy="114.1" r="0.5"/>
	<circle fill="#040404" cx="88.9" cy="114.1" r="0.5"/>
	<circle fill="#040404" cx="96.2" cy="114.1" r="0.5"/>
	<circle fill="#040404" cx="140.4" cy="114.1" r="0.5"/>
	<circle fill="#040404" cx="147.8" cy="114.1" r="0.5"/>
	<path fill="#040404" d="M154.7,114.1c0,0.3,0.2,0.5,0.5,0.5s0.5-0.2,0.5-0.5c0,0,0,0,0-0.1l-0.4-0.4c0,0,0,0-0.1,0
		C154.9,113.6,154.7,113.8,154.7,114.1z"/>
	<path fill="#040404" d="M45.2,106.5c0-0.3-0.2-0.5-0.5-0.5c-0.3,0-0.5,0.2-0.5,0.5c0,0.1,0,0.1,0,0.2l0.3,0.3c0.1,0,0.1,0,0.2,0
		C45,107,45.2,106.8,45.2,106.5z"/>
	<circle fill="#040404" cx="52" cy="106.5" r="0.5"/>
	<circle fill="#040404" cx="59.4" cy="106.5" r="0.5"/>
	<circle fill="#040404" cx="74.1" cy="106.5" r="0.5"/>
	<circle fill="#040404" cx="81.5" cy="106.5" r="0.5"/>
	<circle fill="#040404" cx="88.9" cy="106.5" r="0.5"/>
	<circle fill="#040404" cx="133.1" cy="106.5" r="0.5"/>
	<circle fill="#040404" cx="140.4" cy="106.5" r="0.5"/>
	<path fill="#040404" d="M147.3,106.5c0,0.3,0.2,0.5,0.5,0.5c0.2,0,0.4-0.2,0.5-0.4l-0.6-0.6C147.4,106.1,147.3,106.3,147.3,106.5z"
		/>
	<circle fill="#040404" cx="162.5" cy="106.5" r="0.5"/>
	<circle fill="#040404" cx="169.9" cy="106.5" r="0.5"/>
	<path fill="#040404" d="M37.8,99.2c0-0.3-0.2-0.5-0.5-0.5s-0.5,0.2-0.5,0.5c0,0,0,0.1,0,0.1l0.4,0.4c0,0,0.1,0,0.1,0
		C37.6,99.7,37.8,99.5,37.8,99.2z"/>
	<circle fill="#040404" cx="44.7" cy="99.2" r="0.5"/>
	<circle fill="#040404" cx="52" cy="99.2" r="0.5"/>
	<circle fill="#040404" cx="66.8" cy="99.2" r="0.5"/>
	<circle fill="#040404" cx="74.1" cy="99.2" r="0.5"/>
	<circle fill="#040404" cx="81.5" cy="99.2" r="0.5"/>
	<circle fill="#040404" cx="125.7" cy="99.2" r="0.5"/>
	<circle fill="#040404" cx="133.1" cy="99.2" r="0.5"/>
	<path fill="#040404" d="M139.9,99.2c0,0.3,0.2,0.5,0.5,0.5c0.2,0,0.4-0.2,0.5-0.4l-0.6-0.6C140.1,98.8,139.9,99,139.9,99.2z"/>
	<circle fill="#040404" cx="155.2" cy="99.2" r="0.5"/>
	<circle fill="#040404" cx="162.5" cy="99.2" r="0.5"/>
	<path fill="#040404" d="M30.5,92c0-0.3-0.2-0.5-0.5-0.5c-0.3,0-0.5,0.2-0.5,0.5l0.5,0.5C30.2,92.5,30.5,92.2,30.5,92z"/>
	<circle fill="#040404" cx="37.3" cy="92" r="0.5"/>
	<circle fill="#040404" cx="44.7" cy="92" r="0.5"/>
	<circle fill="#040404" cx="59.4" cy="92" r="0.5"/>
	<circle fill="#040404" cx="66.8" cy="92" r="0.5"/>
	<circle fill="#040404" cx="74.1" cy="92" r="0.5"/>
	<circle fill="#040404" cx="118.3" cy="92" r="0.5"/>
	<circle fill="#040404" cx="125.7" cy="92" r="0.5"/>
	<path fill="#040404" d="M132.6,92c0,0.3,0.2,0.5,0.5,0.5c0.3,0,0.5-0.2,0.5-0.5l-0.5-0.5C132.8,91.5,132.6,91.7,132.6,92z"/>
	<circle fill="#040404" cx="147.8" cy="92" r="0.5"/>
	<circle fill="#040404" cx="155.2" cy="92" r="0.5"/>
	<path fill="#040404" d="M23.1,84.7c0-0.3-0.2-0.5-0.5-0.5c-0.2,0-0.4,0.1-0.5,0.3c0.2,0.2,0.4,0.4,0.6,0.7
		C22.9,85.1,23.1,84.9,23.1,84.7z"/>
	<circle fill="#040404" cx="30" cy="84.7" r="0.5"/>
	<circle fill="#040404" cx="37.3" cy="84.7" r="0.5"/>
	<circle fill="#040404" cx="52" cy="84.7" r="0.5"/>
	<circle fill="#040404" cx="59.4" cy="84.7" r="0.5"/>
	<circle fill="#040404" cx="66.8" cy="84.7" r="0.5"/>
	<circle fill="#040404" cx="111" cy="84.7" r="0.5"/>
	<circle fill="#040404" cx="118.3" cy="84.7" r="0.5"/>
	<path fill="#040404" d="M125.2,84.7c0,0.3,0.2,0.5,0.5,0.5c0.3,0,0.5-0.2,0.5-0.5l-0.5-0.5C125.4,84.2,125.2,84.4,125.2,84.7z"/>
	<circle fill="#040404" cx="140.4" cy="84.7" r="0.5"/>
	<circle fill="#040404" cx="147.8" cy="84.7" r="0.5"/>
	<circle fill="#040404" cx="22.6" cy="77.1" r="0.5"/>
	<circle fill="#040404" cx="30" cy="77.1" r="0.5"/>
	<circle fill="#040404" cx="44.7" cy="77.1" r="0.5"/>
	<circle fill="#040404" cx="52" cy="77.1" r="0.5"/>
	<circle fill="#040404" cx="59.4" cy="77.1" r="0.5"/>
	<circle fill="#040404" cx="103.6" cy="77.1" r="0.5"/>
	<circle fill="#040404" cx="111" cy="77.1" r="0.5"/>
	<path fill="#040404" d="M117.8,77.1c0,0.3,0.2,0.5,0.5,0.5c0.2,0,0.4-0.1,0.5-0.3l-0.6-0.6C118,76.8,117.8,76.9,117.8,77.1z"/>
	<circle fill="#040404" cx="133.1" cy="77.1" r="0.5"/>
	<circle fill="#040404" cx="140.4" cy="77.1" r="0.5"/>
	<circle fill="#040404" cx="15.2" cy="69.8" r="0.5"/>
	<circle fill="#040404" cx="22.6" cy="69.8" r="0.5"/>
	<circle fill="#040404" cx="44.7" cy="69.8" r="0.5"/>
	<circle fill="#040404" cx="52" cy="69.8" r="0.5"/>
	<circle fill="#040404" cx="96.2" cy="69.8" r="0.5"/>
	<circle fill="#040404" cx="103.6" cy="69.8" r="0.5"/>
	<path fill="#040404" d="M110.5,69.8c0,0.3,0.2,0.5,0.5,0.5c0.2,0,0.4-0.1,0.5-0.3l-0.6-0.6C110.6,69.4,110.5,69.6,110.5,69.8z"/>
	<circle fill="#040404" cx="125.7" cy="69.8" r="0.5"/>
	<circle fill="#040404" cx="133.1" cy="69.8" r="0.5"/>
	<circle fill="#040404" cx="15.2" cy="62.8" r="0.5"/>
	<path fill="#040404" d="M37.8,62.8c0-0.3-0.2-0.5-0.5-0.5c0,0,0,0,0,0c0,0.3,0.1,0.6,0.1,1C37.6,63.2,37.8,63,37.8,62.8z"/>
	<circle fill="#040404" cx="44.7" cy="62.8" r="0.5"/>
	<circle fill="#040404" cx="88.9" cy="62.8" r="0.5"/>
	<circle fill="#040404" cx="96.2" cy="62.8" r="0.5"/>
	<path fill="#040404" d="M103.1,62.8c0,0.3,0.2,0.5,0.5,0.5s0.5-0.2,0.5-0.5c0,0,0-0.1,0-0.1l-0.4-0.4c0,0-0.1,0-0.1,0
		C103.3,62.3,103.1,62.5,103.1,62.8z"/>
	<circle fill="#040404" cx="118.3" cy="62.8" r="0.5"/>
	<circle fill="#040404" cx="125.7" cy="62.8" r="0.5"/>
	<circle fill="#040404" cx="7.9" cy="55.5" r="0.5"/>
	<circle fill="#040404" cx="15.2" cy="55.5" r="0.5"/>
	<path fill="#040404" d="M37.8,55.5c0-0.2-0.2-0.4-0.4-0.5c-0.1,0.3-0.1,0.6-0.1,1c0,0,0,0,0,0C37.6,56,37.8,55.8,37.8,55.5z"/>
	<circle fill="#040404" cx="44.7" cy="55.5" r="0.5"/>
	<circle fill="#040404" cx="81.5" cy="55.5" r="0.5"/>
	<circle fill="#040404" cx="88.9" cy="55.5" r="0.5"/>
	<path fill="#040404" d="M95.7,55.5c0,0.3,0.2,0.5,0.5,0.5s0.5-0.2,0.5-0.5c0,0,0-0.1,0-0.1L96.4,55c0,0-0.1,0-0.1,0
		C96,55,95.7,55.2,95.7,55.5z"/>
	<circle fill="#040404" cx="111" cy="55.5" r="0.5"/>
	<circle fill="#040404" cx="118.3" cy="55.5" r="0.5"/>
	<circle fill="#040404" cx="7.9" cy="47.9" r="0.5"/>
	<circle fill="#040404" cx="15.2" cy="47.9" r="0.5"/>
	<circle fill="#040404" cx="44.7" cy="47.9" r="0.5"/>
	<circle fill="#040404" cx="52" cy="47.9" r="0.5"/>
	<circle fill="#040404" cx="74.1" cy="47.9" r="0.5"/>
	<circle fill="#040404" cx="81.5" cy="47.9" r="0.5"/>
	<path fill="#040404" d="M88.4,47.9c0,0.3,0.2,0.5,0.5,0.5c0.2,0,0.4-0.2,0.5-0.4l-0.6-0.6C88.6,47.5,88.4,47.7,88.4,47.9z"/>
	<circle fill="#040404" cx="103.6" cy="47.9" r="0.5"/>
	<circle fill="#040404" cx="111" cy="47.9" r="0.5"/>
	<circle fill="#040404" cx="7.9" cy="40.6" r="0.5"/>
	<circle fill="#040404" cx="15.2" cy="40.6" r="0.5"/>
	<circle fill="#040404" cx="52" cy="40.6" r="0.5"/>
	<circle fill="#040404" cx="59.4" cy="40.6" r="0.5"/>
	<circle fill="#040404" cx="66.8" cy="40.6" r="0.5"/>
	<circle fill="#040404" cx="74.1" cy="40.6" r="0.5"/>
	<path fill="#040404" d="M81,40.6c0,0.3,0.2,0.5,0.5,0.5c0.2,0,0.4-0.1,0.4-0.3c-0.2-0.2-0.5-0.4-0.7-0.6C81.1,40.3,81,40.5,81,40.6
		z"/>
	<circle fill="#040404" cx="96.2" cy="40.6" r="0.5"/>
	<circle fill="#040404" cx="103.6" cy="40.6" r="0.5"/>
	<path fill="#040404" d="M7.9,33.9c0.3,0,0.5-0.2,0.5-0.5s-0.2-0.5-0.5-0.5c0,0-0.1,0-0.1,0c-0.1,0.3-0.2,0.6-0.3,0.8
		C7.6,33.8,7.7,33.9,7.9,33.9z"/>
	<circle fill="#040404" cx="15.2" cy="33.4" r="0.5"/>
	<path fill="#040404" d="M59.4,33.9c0.2,0,0.4-0.2,0.5-0.4c-0.3,0-0.6,0.1-0.9,0.1C59,33.8,59.2,33.9,59.4,33.9z"/>
	<path fill="#040404" d="M66.8,33.9c0.2,0,0.3-0.1,0.4-0.2c-0.3,0-0.6-0.1-0.9-0.1C66.4,33.8,66.6,33.9,66.8,33.9z"/>
	<circle fill="#040404" cx="88.9" cy="33.4" r="0.5"/>
	<circle fill="#040404" cx="96.2" cy="33.4" r="0.5"/>
	<circle fill="#040404" cx="15.2" cy="26.1" r="0.5"/>
	<circle fill="#040404" cx="22.6" cy="26.1" r="0.5"/>
	<circle fill="#040404" cx="81.5" cy="26.1" r="0.5"/>
	<circle fill="#040404" cx="88.9" cy="26.1" r="0.5"/>
	<circle fill="#040404" cx="22.6" cy="18.6" r="0.5"/>
	<circle fill="#040404" cx="30" cy="18.6" r="0.5"/>
	<path fill="#040404" d="M67.3,18.6c0-0.3-0.2-0.5-0.5-0.5s-0.5,0.2-0.5,0.5c0,0,0,0.1,0,0.1c0.2,0.1,0.4,0.2,0.7,0.4
		C67.1,19,67.3,18.8,67.3,18.6z"/>
	<circle fill="#040404" cx="74.1" cy="18.6" r="0.5"/>
	<circle fill="#040404" cx="81.5" cy="18.6" r="0.5"/>
	<path fill="#040404" d="M23.1,11.4c-0.1,0.1-0.3,0.2-0.4,0.4C22.9,11.7,23,11.6,23.1,11.4z"/>
	<circle fill="#040404" cx="30" cy="11.3" r="0.5"/>
	<circle fill="#040404" cx="37.3" cy="11.3" r="0.5"/>
	<circle fill="#040404" cx="44.7" cy="11.3" r="0.5"/>
	<circle fill="#040404" cx="52" cy="11.3" r="0.5"/>
	<circle fill="#040404" cx="59.4" cy="11.3" r="0.5"/>
	<circle fill="#040404" cx="66.8" cy="11.3" r="0.5"/>
	<circle fill="#040404" cx="74.1" cy="11.3" r="0.5"/>
	<path fill="#040404" d="M37.3,4.1c0.3,0,0.5-0.2,0.5-0.5c0-0.2-0.1-0.3-0.3-0.4c-0.2,0.1-0.5,0.2-0.7,0.2c0,0.1,0,0.1,0,0.2
		C36.8,3.9,37,4.1,37.3,4.1z"/>
	<circle fill="#040404" cx="44.7" cy="3.6" r="0.5"/>
	<circle fill="#040404" cx="52" cy="3.6" r="0.5"/>
	<circle fill="#040404" cx="59.4" cy="3.6" r="0.5"/>
	<path fill="#040404" d="M140.4,194c-0.2,0-0.4,0.1-0.5,0.3c0.3,0.1,0.6,0.3,0.9,0.4c0-0.1,0-0.1,0-0.2
		C140.9,194.2,140.7,194,140.4,194z"/>
	<circle fill="#040404" cx="147.8" cy="194.5" r="0.5"/>
	<circle fill="#040404" cx="155.2" cy="194.5" r="0.5"/>
	<circle fill="#040404" cx="162.5" cy="194.5" r="0.5"/>
	<circle fill="#040404" cx="133.1" cy="187.5" r="0.5"/>
	<circle fill="#040404" cx="140.4" cy="187.5" r="0.5"/>
	<circle fill="#040404" cx="147.8" cy="187.5" r="0.5"/>
	<circle fill="#040404" cx="155.2" cy="187.5" r="0.5"/>
	<circle fill="#040404" cx="162.5" cy="187.5" r="0.5"/>
	<circle fill="#040404" cx="169.9" cy="187.5" r="0.5"/>
	<path fill="#040404" d="M118.8,180.2c0-0.3-0.2-0.5-0.5-0.5c-0.2,0-0.4,0.1-0.5,0.3c0.2,0.2,0.5,0.4,0.7,0.6
		C118.7,180.5,118.8,180.3,118.8,180.2z"/>
	<circle fill="#040404" cx="125.7" cy="180.2" r="0.5"/>
	<circle fill="#040404" cx="133.1" cy="180.2" r="0.5"/>
	<circle fill="#040404" cx="169.9" cy="180.2" r="0.5"/>
	<circle fill="#040404" cx="177.2" cy="180.2" r="0.5"/>
	<circle fill="#040404" cx="111" cy="172.6" r="0.5"/>
	<circle fill="#040404" cx="118.3" cy="172.6" r="0.5"/>
	<circle fill="#040404" cx="125.7" cy="172.6" r="0.5"/>
	<circle fill="#040404" cx="177.2" cy="172.6" r="0.5"/>
	<circle fill="#040404" cx="184.6" cy="172.6" r="0.5"/>
	<circle fill="#040404" cx="103.6" cy="165.3" r="0.5"/>
	<circle fill="#040404" cx="111" cy="165.3" r="0.5"/>
	<circle fill="#040404" cx="118.3" cy="165.3" r="0.5"/>
	<circle fill="#040404" cx="177.2" cy="165.3" r="0.5"/>
	<circle fill="#040404" cx="184.6" cy="165.3" r="0.5"/>
	<path fill="#040404" d="M96.7,158.1c0-0.3-0.2-0.5-0.5-0.5s-0.5,0.2-0.5,0.5c0,0.1,0,0.1,0,0.2l0.3,0.3c0.1,0,0.1,0,0.2,0
		C96.5,158.6,96.7,158.4,96.7,158.1z"/>
	<circle fill="#040404" cx="103.6" cy="158.1" r="0.5"/>
	<circle fill="#040404" cx="111" cy="158.1" r="0.5"/>
	<circle fill="#040404" cx="177.2" cy="158.1" r="0.5"/>
	<circle fill="#040404" cx="184.6" cy="158.1" r="0.5"/>
	<path fill="#040404" d="M89.4,150.8c0-0.3-0.2-0.5-0.5-0.5s-0.5,0.2-0.5,0.5c0,0,0,0.1,0,0.1l0.4,0.4c0,0,0.1,0,0.1,0
		C89.1,151.3,89.4,151.1,89.4,150.8z"/>
	<circle fill="#040404" cx="96.2" cy="150.8" r="0.5"/>
	<circle fill="#040404" cx="103.6" cy="150.8" r="0.5"/>
	<circle fill="#040404" cx="118.3" cy="150.8" r="0.5"/>
	<circle fill="#040404" cx="125.7" cy="150.8" r="0.5"/>
	<circle fill="#040404" cx="177.2" cy="150.8" r="0.5"/>
	<circle fill="#040404" cx="184.6" cy="150.8" r="0.5"/>
	<circle fill="#040404" cx="81.5" cy="143.2" r="0.5"/>
	<circle fill="#040404" cx="88.9" cy="143.2" r="0.5"/>
	<circle fill="#040404" cx="96.2" cy="143.2" r="0.5"/>
	<circle fill="#040404" cx="111" cy="143.2" r="0.5"/>
	<circle fill="#040404" cx="118.3" cy="143.2" r="0.5"/>
	<path fill="#040404" d="M125.2,143.2c0,0.3,0.2,0.5,0.5,0.5s0.5-0.2,0.5-0.5c0-0.1,0-0.1,0-0.2l-0.3-0.3c-0.1,0-0.1,0-0.2,0
		C125.4,142.7,125.2,143,125.2,143.2z"/>
	<circle fill="#040404" cx="169.9" cy="143.2" r="0.5"/>
	<circle fill="#040404" cx="177.2" cy="143.2" r="0.5"/>
	<path fill="#040404" d="M74.6,135.9c0-0.3-0.2-0.5-0.5-0.5s-0.5,0.2-0.5,0.5c0,0.1,0,0.2,0.1,0.3l0.1,0.1c0.1,0.1,0.2,0.1,0.3,0.1
		C74.4,136.4,74.6,136.2,74.6,135.9z"/>
	<circle fill="#040404" cx="81.5" cy="135.9" r="0.5"/>
	<circle fill="#040404" cx="88.9" cy="135.9" r="0.5"/>
	<circle fill="#040404" cx="103.6" cy="135.9" r="0.5"/>
	<circle fill="#040404" cx="111" cy="135.9" r="0.5"/>
	<path fill="#040404" d="M117.8,135.9c0,0.3,0.2,0.5,0.5,0.5s0.5-0.2,0.5-0.5c0-0.1-0.1-0.3-0.1-0.3l0,0c-0.1-0.1-0.2-0.1-0.3-0.1
		C118.1,135.4,117.8,135.7,117.8,135.9z"/>
	<circle fill="#040404" cx="162.5" cy="135.9" r="0.5"/>
	<circle fill="#040404" cx="169.9" cy="135.9" r="0.5"/>
	<circle fill="#040404" cx="66.8" cy="128.3" r="0.5"/>
	<circle fill="#040404" cx="74.1" cy="128.3" r="0.5"/>
	<circle fill="#040404" cx="81.5" cy="128.3" r="0.5"/>
	<circle fill="#040404" cx="96.2" cy="128.3" r="0.5"/>
	<circle fill="#040404" cx="103.6" cy="128.3" r="0.5"/>
	<path fill="#040404" d="M110.5,128.3c0,0.3,0.2,0.5,0.5,0.5c0.3,0,0.5-0.2,0.5-0.4l-0.5-0.5C110.7,127.9,110.5,128.1,110.5,128.3z"
		/>
	<circle fill="#040404" cx="155.2" cy="128.3" r="0.5"/>
	<circle fill="#040404" cx="162.5" cy="128.3" r="0.5"/>
	<path fill="#040404" d="M169.9,128.8c0.1,0,0.2,0,0.3-0.1c-0.2-0.2-0.4-0.5-0.7-0.7c-0.1,0.1-0.1,0.2-0.1,0.3
		C169.4,128.6,169.6,128.8,169.9,128.8z"/>
</g>
</svg>';

		return $html;
	}
}

if ( ! function_exists( 'borgholm_hand_svg' ) ) {
	function borgholm_hand_svg() {

		$html = '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="0 0 44 86" enable-background="new 0 0 44 86" xml:space="preserve">
<g>
	<g>
		<path fill="#414042" d="M12.1,74.8c-0.9,3.6-1.8,7.3-2.5,11c-0.1,0.5,0.3,0.8,0.9,0.8c8.9-1.2,17.7-2.8,26.4-4.7
			c2-0.4,6.4-0.6,7.1-2.6c0.4-1.1-0.3-2.8-0.4-3.9c-0.3-2.1-0.6-4.1-0.9-6.2c-0.1-0.8-1.6-0.8-1.5,0c0.2,1.6,0.5,3.1,0.7,4.7
			c0.2,1.2,0.8,2.9,0.6,4.1c-0.5,2.5-8.1,3.2-10.8,3.7c-7.1,1.5-14.3,2.7-21.5,3.7c0.3,0.3,0.6,0.5,0.9,0.8c0.7-3.7,1.5-7.3,2.5-11
			C13.8,74.3,12.3,74,12.1,74.8L12.1,74.8z"/>
	</g>
	<g>
		<path fill="#414042" d="M13.3,75.2c9-2.9,18.4-4.9,28.1-5.7c1-0.1,1-1.3,0-1.2C31.6,69.1,22.1,71,12.9,74
			C12,74.3,12.4,75.5,13.3,75.2L13.3,75.2z"/>
	</g>
	<g>
		<path fill="#414042" d="M13,77.7c2.4-0.5,4.9-1,7.2-1.6c2.8-0.7,5.4-1.8,8.3-2.1c1-0.1,1-1.3,0-1.2c-2.8,0.3-5.2,1.1-7.8,1.9
			c-2.7,0.7-5.5,1.2-8.2,1.8C11.7,76.7,12.1,77.9,13,77.7L13,77.7z"/>
	</g>
	<g>
		<path fill="#414042" d="M31.6,73.7C35,73,38.5,72.3,42,71.7c1-0.2,0.6-1.4-0.4-1.2c-3.5,0.6-6.9,1.2-10.4,1.9
			C30.2,72.7,30.6,73.9,31.6,73.7L31.6,73.7z"/>
	</g>
	<g>
		<path fill="#414042" d="M23.6,26.4c-2.4,4.8-7.2,8.3-13.2,10c-0.5,0.1-0.8,0.7-0.3,1c2.1,1.3,3.5,2.9,4.4,5
			c0.1,0.3,0.5,0.5,0.9,0.4c6.5-0.8,12,2.7,13,7.9c0.1,0.8,1.6,0.5,1.5-0.3c-1.1-6-7.6-9.6-14.8-8.8c0.3,0.1,0.6,0.3,0.9,0.4
			c-1-2.3-2.5-4-4.8-5.5c-0.1,0.3-0.2,0.7-0.3,1c6.6-1.8,11.6-5.6,14.2-10.8C25.5,26,24,25.7,23.6,26.4L23.6,26.4z"/>
	</g>
	<g>
		<path fill="#414042" d="M25,26c-1.8-1.3-4.6-0.5-6.8-0.2c-3.3,0.4-7.2,0.8-10.3,1.9c-2.3,0.9-3.3,2.6-4.2,4.5
			c-1.4,2.7-2.4,5.5-3.2,8.3c-0.6,2.3-0.6,3.9,1,5.9c2.2,2.7,4.6,5.3,6.9,8c2.3,2.7,5.1,5.3,6.9,8.2c2.2,3.6,1.3,7.4,0.3,11.2
			c-0.2,0.8,1.3,1.1,1.5,0.3c1.1-4.1,2-8.2-0.4-12.1c-2.9-4.6-7.3-8.7-10.9-12.9c-2.4-2.7-4.5-4.7-3.7-8.3c0.6-2.5,1.5-4.9,2.6-7.3
			c0.6-1.3,1.3-3.1,2.7-4.1c1.3-0.9,3.5-1.2,5.1-1.5c1.6-0.3,3.3-0.6,5-0.8c1.7-0.2,5.1-1.3,6.5-0.3C24.6,27.4,25.7,26.5,25,26
			L25,26z"/>
	</g>
	<g>
		<path fill="#414042" d="M39,27.3c1.4-3.7-4-5.9-7.6-6.6c-4.3-0.9-6.4,3-7.2,5.9c-0.6,2.1-0.9,4.3-1.1,6.4
			c-0.2,1.7-0.5,4.7,0.8,6.2c2.6,2.8,5.2-3.2,5.8-4.4c0.4-0.7-1.1-1.1-1.5-0.3c-0.3,0.7-1.1,2.8-1.9,3.3c-1.7,1-1.8-1.9-1.8-2.8
			c0-2.1,0.3-4.4,0.8-6.5c0.4-1.7,0.9-3.4,2-4.9c2.3-2.9,11.8-0.6,10.3,3.4C37.2,27.7,38.7,28,39,27.3L39,27.3z"/>
	</g>
	<g>
		<path fill="#414042" d="M35.4,34.9c-1,2.9-2.8,5.3-5.5,7.5c0.4,0.1,0.8,0.2,1.3,0.3c-1.7-3.6-1.3-7.3-0.4-11.1
			c0.6-2.6,2.7-3.7,5.9-3.7c4.1,0,4.6,2.4,4.7,5.1c0.1,3.7,0.5,7.5,0,11.2c-0.6,4.3-2.8,8.1-5,12c-0.4,0.7,0.9,1.4,1.3,0.6
			c3-5.2,5.4-10.3,5.4-16.2c0-2.8-0.1-5.7-0.2-8.5c-0.1-1.8-0.4-3.6-2.3-4.7c-2.6-1.5-8.8-1-10.4,1.4c-1.2,1.8-1.3,4.6-1.5,6.7
			c-0.3,2.6,0,5.1,1.2,7.5c0.2,0.4,0.8,0.6,1.3,0.3c2.8-2.3,4.8-4.9,5.9-8C37.1,34.4,35.7,34.1,35.4,34.9L35.4,34.9z"/>
	</g>
	<g>
		<path fill="#414042" d="M38,53.7c0.2,5.1,0.7,10.1,1.5,15.1c0.1,0.8,1.6,0.8,1.5,0c-0.8-5-1.3-10.1-1.5-15.1
			C39.5,52.9,38,52.9,38,53.7L38,53.7z"/>
	</g>
	<g>
		<path fill="#414042" d="M21.4,61c1.6,0,3.2-0.2,4.7-0.6c0.9-0.3,0.5-1.5-0.4-1.2c-1.4,0.4-2.8,0.6-4.3,0.6
			C20.4,59.7,20.4,60.9,21.4,61L21.4,61z"/>
	</g>
	<g>
		<path fill="#414042" d="M8.4,28c1.7-4.6,3.8-9,6.3-13.3c2.2-3.8,4.6-8,8.2-11.1c0.9-0.8,2.1-2.5,3-1.2c0.5,0.7-0.5,3.1-0.6,3.8
			c-0.5,2.3-1.4,4.6-2.3,6.9c-1.8,4.3-3.6,8.6-5.5,12.8c-0.3,0.8,1.1,1.1,1.5,0.3c2.5-5.6,5.3-11.4,7.1-17.2c0.4-1.3,3.4-9-0.7-8.6
			c-2,0.2-4.2,2.8-5.3,4c-2,2.2-3.6,4.6-5.1,7c-3.3,5.2-5.9,10.6-8,16.2C6.6,28.5,8.1,28.8,8.4,28L8.4,28z"/>
	</g>
	<g>
		<path fill="#414042" d="M15.6,12.4c-0.3-1.7-0.4-3.4-0.3-5.2c0-0.6,0.1-2.2,0.7-2.6c1.2-0.8,2.4,0.8,3.1,1.4
			c0.7,0.6,1.8-0.3,1.1-0.9c-1.7-1.3-4-3.5-5.6-0.9c-1.5,2.5-0.9,5.9-0.4,8.5C14.3,13.5,15.7,13.2,15.6,12.4L15.6,12.4z"/>
	</g>
	<g>
		<path fill="#414042" d="M23.4,13c1.1,3.2,2.1,6.5,3,9.7c0.2,0.8,1.7,0.4,1.5-0.3c-0.9-3.3-2-6.5-3-9.7
			C24.6,11.9,23.2,12.3,23.4,13L23.4,13z"/>
	</g>
	<g>
		<path fill="#414042" d="M14.6,45.2c0.1,0.3,0.2,0.6,0,0.9c-0.2,0.3-0.1,0.7,0.3,0.9c0.3,0.2,0.9,0.1,1-0.2
			c0.3-0.6,0.4-1.2,0.1-1.8c-0.1-0.3-0.5-0.5-0.9-0.4C14.8,44.5,14.5,44.9,14.6,45.2L14.6,45.2z"/>
	</g>
	<g>
		<path fill="#414042" d="M17.6,26.9C17.6,26.8,17.6,26.8,17.6,26.9c-0.1,0.2-0.1,0.5-0.1,0.8c0,0,0,0,0.1-0.1c-0.4,0-0.7,0-1.1,0
			c-0.2-0.1,0,0.2,0.1,0.3c0.1,0.2,0.3,0.4,0.5,0.5c0.4,0.3,0.8,0.4,1.3,0.5c1.5,0.3,3.4-0.2,4.9-0.5c1-0.1,0.6-1.3-0.4-1.2
			c-1.1,0.2-2.3,0.4-3.5,0.5c-0.7,0-1.1,0-1.5-0.5c-0.1-0.2-0.1-0.3-0.3-0.4c-0.3-0.2-0.8-0.3-1.1,0c0,0,0,0-0.1,0.1
			c-0.3,0.2-0.3,0.6,0,0.9c0,0,0,0,0.1,0C17.2,28.3,18.3,27.4,17.6,26.9L17.6,26.9z"/>
	</g>
	<g>
		<path fill="#414042" d="M24,33.5c1.3,0.2,1,1.7,1,2.5c0,0.9,0.1,1.7,0.1,2.6c0,0.8,1.6,0.8,1.5,0c0-1.4,0.1-2.9-0.2-4.3
			c-0.2-0.9-0.8-1.8-2-1.9C23.5,32.2,23.1,33.4,24,33.5L24,33.5z"/>
	</g>
	<g>
		<path fill="#414042" d="M30.6,38.1c0.1-0.1,0.4,0.7,0.4,0.7c0.1,0.3,0.2,0.6,0.2,0.9c0.1,0.6,0.2,1.3,0.1,1.9c0,0.8,1.5,0.8,1.5,0
			c0-1.2,0-2.9-0.8-3.9c-0.7-0.9-2-0.9-2.7-0.1C28.7,38.1,30,38.7,30.6,38.1L30.6,38.1z"/>
	</g>
	<g>
		<path fill="#414042" d="M18.4,15c0.6,0,1.1,0,1.7,0c1,0,1-1.2,0-1.2c-0.6,0-1.1,0-1.7,0C17.5,13.8,17.5,15,18.4,15L18.4,15z"/>
	</g>
	<g>
		<path fill="#414042" d="M16.1,21.2c0.4,0,0.9,0,1.3,0c1,0,1-1.2,0-1.2c-0.4,0-0.9,0-1.3,0C15.1,20,15.1,21.2,16.1,21.2L16.1,21.2z
			"/>
	</g>
</g>
</svg>';

		return $html;
	}
}

if ( ! function_exists( 'borgholm_star_svg' ) ) {
	function borgholm_star_svg() {

		$html = '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 30 30" enable-background="new 0 0 30 30" xml:space="preserve">
					<polygon points="30,12.5 21,12.5 27.4,6.2 23.8,2.6 17.5,9 17.5,0 12.5,0 12.5,9 6.2,2.6 2.6,6.2 9,12.5 0,12.5 0,17.5 9,17.5 2.6,23.8 6.2,27.4 12.5,21 12.5,30 17.5,30 17.5,21 23.8,27.4 27.4,23.8 21,17.5 30,17.5 "/>
				</svg>';

		return $html;
	}
}

if ( ! function_exists( 'borgholm_plus_sign_svg' ) ) {
	function borgholm_plus_sign_svg() {

		$html = '<svg xmlns="http://www.w3.org/2000/svg" width="17" height="17" viewBox="0 0 17 17">
				    <path fill="#f23801" d="M0,7.312H17V9.687H0V7.312ZM7.312,0H9.687V17H7.312V0Z"/>
				</svg>';

		return $html;
	}
}

if ( ! function_exists( 'borgholm_minus_sign_svg' ) ) {
	function borgholm_minus_sign_svg() {

		$html = '<svg xmlns="http://www.w3.org/2000/svg" width="17" height="17" viewBox="0 0 17 17">
				    <rect fill="#f23801" y="7.313" width="17" height="2.375"/>
				</svg>';

		return $html;
	}
}

if ( ! function_exists( 'borgholm_play_button_svg' ) ) {
	function borgholm_play_button_svg() {
		
		$html = '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 158 158">
                    <circle class="qodef-svg-circle-one" cx="79" cy="79" r="79"/>
                    <circle class="qodef-svg-circle-two" cx="79" cy="79" r="79"/>
                    <path class="qodef-svg-triangle" d="M74.75,65.6l17,16.5-17,16.5Z" transform="translate(-2.3 -3.1)"/>
                </svg>';
		
		return $html;
	}
}

if ( ! function_exists( 'borgholm_mobile_menu_opener_svg' ) ) {
	function borgholm_mobile_menu_opener_svg() {

		$html = '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="22" height="8" viewBox="0 0 22 8">
				    <image id="Rectangle_43" data-name="Rectangle 43" width="22" height="8" xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAABYAAAAICAYAAAD9aA/QAAAAMElEQVQokWOUlZX9z0ADwEQLQ0GA8f9/mjiYdi4eegZTnCoePXrEiE2cNi5mYGAAABFuCF45IN3EAAAAAElFTkSuQmCC"/>
				</svg>';

		return $html;
	}
}