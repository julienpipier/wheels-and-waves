<?php

// Include logo
borgholm_template_part( 'header', 'templates/parts/logo' );

// Include main navigation
borgholm_template_part( 'header', 'templates/parts/navigation' );