<?php

if ( ! function_exists( 'borgholm_include_justified_gallery_scripts' ) ) {
	/**
	 * Function that enqueue modules 3rd party scripts
	 *
	 * @param array $atts
	 */
	function borgholm_include_justified_gallery_scripts( $atts ) {
		
		if ( isset( $atts['behavior'] ) && $atts['behavior'] == 'justified-gallery' ) {
			wp_enqueue_script( 'justified-gallery', BORGHOLM_INC_ROOT . '/justified-gallery/assets/js/plugins/jquery.justifiedGallery.min.js', array( 'jquery' ), true );
		}
	}
	
	add_action( 'borgholm_core_action_list_shortcodes_load_assets', 'borgholm_include_justified_gallery_scripts' );
}

if ( ! function_exists( 'borgholm_register_justified_gallery_scripts_for_list_shortcodes' ) ) {
	/**
	 * Function that set module 3rd party scripts for list shortcodes
	 *
	 * @param array $scripts
	 *
	 * @return array
	 */
	function borgholm_register_justified_gallery_scripts_for_list_shortcodes( $scripts ) {

		$scripts['justified-gallery'] = array(
			'registered' => true
		);

		return $scripts;
	}

	add_filter( 'borgholm_core_filter_register_list_shortcode_scripts', 'borgholm_register_justified_gallery_scripts_for_list_shortcodes' );
}
