<?php

// Include mobile logo
borgholm_template_part( 'mobile-header', 'templates/parts/mobile-logo' );

// Include mobile navigation opener
borgholm_template_part( 'mobile-header', 'templates/parts/mobile-navigation-opener' );

// Include mobile navigation
borgholm_template_part( 'mobile-header', 'templates/parts/mobile-navigation' );