<?php

if ( ! function_exists( 'borgholm_load_page_mobile_header' ) ) {
	/**
	 * Function which loads page template module
	 */
	function borgholm_load_page_mobile_header() {
		// Include mobile header template
		echo apply_filters( 'borgholm_filter_mobile_header_template', borgholm_get_template_part( 'mobile-header', 'templates/mobile-header' ) );
	}
	
	add_action( 'borgholm_action_page_header_template', 'borgholm_load_page_mobile_header' );
}

if ( ! function_exists( 'borgholm_register_mobile_navigation_menus' ) ) {
	/**
	 * Function which registers navigation menus
	 */
	function borgholm_register_mobile_navigation_menus() {
		$navigation_menus = apply_filters( 'borgholm_filter_register_mobile_navigation_menus', array( 'mobile-navigation' => esc_html__( 'Mobile Navigation', 'borgholm' ) ) );
		
		if ( ! empty( $navigation_menus ) ) {
			register_nav_menus( $navigation_menus );
		}
	}
	
	add_action( 'borgholm_action_after_include_modules', 'borgholm_register_mobile_navigation_menus' );
}