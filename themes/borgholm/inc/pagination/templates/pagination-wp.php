<?php if ( get_the_posts_pagination() !== '' ): ?>

    <div class="qodef-m-pagination qodef--wp">
		<?php
		// Load posts pagination (in order to override template use navigation_markup_template filter hook)
		the_posts_pagination( array(
			'prev_text' => borgholm_arrow_slim_left_svg() . '<span class="qodef-m-pagination-label">' . esc_html__( 'Prev', 'borgholm' ) . '</span>',
			'next_text' => '<span class="qodef-m-pagination-label">' . esc_html__( 'Next', 'borgholm' ) . '</span>' . borgholm_arrow_slim_right_svg(),
		) ); ?>
    </div>

<?php endif; ?>