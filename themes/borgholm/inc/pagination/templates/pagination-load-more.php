<?php if ( isset( $query_result ) && intval( $query_result->max_num_pages ) > 1 ) { ?>
	<div class="qodef-m-pagination qodef--load-more" <?php echo borgholm_is_installed( 'framework' ) ? qode_framework_get_inline_style( $pagination_type_load_more_top_margin ) : ''; ?>>
		<div class="qodef-m-pagination-inner">
			<?php
			$button_params = array(
				'custom_class'  => 'qodef-load-more-button',
				'button_layout' => 'textual',
				'link'          => '#',
				'text'          => esc_html__( 'Load More', 'borgholm' ),
			);
			
			borgholm_render_button_element( $button_params ); ?>
		</div>
	</div>
	<?php
	// Include loading spinner ?>
    <span class="qodef-loading-spinner">
        <img src="<?php echo esc_url( BORGHOLM_ASSETS_ROOT . '/img/custom-decoration.svg' ); ?>" alt="<?php esc_attr_e( 'Loading Image', 'borgholm' ); ?>" />
    </span>
<?php } ?>