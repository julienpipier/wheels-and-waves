<div id="qodef-404-page">
	<h1 class="qodef-404-mark"><?php echo esc_html( $mark ); ?></h1>
	
	<h4 class="qodef-404-title"><?php echo esc_html( $title ); ?></h4>
	
	<h4 class="qodef-404-text"><?php echo esc_html( $text ); ?></h4>
	
	<div class="qodef-404-button">
		<?php
		$button_params = array(
			'link' => esc_url( home_url( '/' ) ),
			'text' => esc_html( $button_text ),
		);
		
		borgholm_render_button_element( $button_params ); ?>
	</div>
</div>