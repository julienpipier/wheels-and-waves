<?php

if ( ! function_exists( 'borgholm_set_404_page_inner_classes' ) ) {
	/**
	 * Function that return classes for the page inner div from header.php
	 *
	 * @param string $classes
	 *
	 * @return string
	 */
	function borgholm_set_404_page_inner_classes( $classes ) {
		
		if ( is_404() ) {
			$classes = 'qodef-content-full-width';
		}
		
		return $classes;
	}
	
	add_filter( 'borgholm_filter_page_inner_classes', 'borgholm_set_404_page_inner_classes' );
}

if ( ! function_exists( 'borgholm_get_404_page_parameters' ) ) {
	/**
	 * Function that set 404 page area content parameters
	 */
	function borgholm_get_404_page_parameters() {
		
		$params = array(
			'mark'        => esc_html__( '404', 'borgholm' ),
			'title'       => esc_html__( 'We can’t seem to find this page', 'borgholm' ),
			'text'        => esc_html__( 'The page you are looking for doesn\'t exist. It may have been moved or removed altogether. Please try searching for some other page', 'borgholm' ),
			'button_text' => esc_html__( 'Back to home', 'borgholm' ),
		);
		
		return apply_filters( 'borgholm_filter_404_page_template_params', $params );
	}
}
