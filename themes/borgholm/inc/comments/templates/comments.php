<div id="qodef-page-comments">
	<?php if ( have_comments() ) {
		$comments_number = get_comments_number();
		?>
		<div id="qodef-page-comments-list" class="qodef-m">
			<h2 class="qodef-m-title"><?php echo sprintf( _n( '%s Comment', '%s Comments', $comments_number, 'borgholm' ), $comments_number ); ?></h2>
			<ul class="qodef-m-comments">
				<?php wp_list_comments( array_unique( array_merge( array( 'callback' => 'borgholm_get_comments_list_template' ), apply_filters( 'borgholm_filter_comments_list_template_callback', array() ) ) ) ); ?>
			</ul>
			
			<?php if ( get_comment_pages_count() > 1 ) { ?>
				<div class="qodef-m-pagination qodef--wp">
					<?php the_comments_pagination( array(
						'prev_text' => borgholm_arrow_slim_left_svg() . '<span class="qodef-m-pagination-label">' . esc_html__( 'Prev', 'borgholm' ) . '</span>',
						'next_text' => '<span class="qodef-m-pagination-label">' . esc_html__( 'Next', 'borgholm' ) . '</span>' . borgholm_arrow_slim_right_svg()
					) ); ?>
				</div>
			<?php } ?>
		</div>
	<?php } ?>
	<?php if ( ! comments_open() && get_comments_number() && post_type_supports( get_post_type(), 'comments' ) ) { ?>
		<p class="qodef-page-comments-not-found"><?php esc_html_e( 'Comments are closed.', 'borgholm' ); ?></p>
	<?php } ?>
	
	<div id="qodef-page-comments-form">
		<?php comment_form( borgholm_get_comment_form_args() ); ?>
	</div>
</div>