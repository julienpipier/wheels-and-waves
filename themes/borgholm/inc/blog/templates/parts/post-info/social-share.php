<?php if ( class_exists( 'BorgholmCoreSocialShareShortcode' ) ) { ?>
	<div class="qodef-e-info-item qodef-e-info-social-share">
		<?php
		$params = array();
		$params['layout'] = 'list';
		
		echo BorgholmCoreSocialShareShortcode::call_shortcode( $params ); ?>
	</div>
<?php } ?>