<div class="qodef-e-media">
	<?php switch ( get_post_format() ) {
		case 'gallery':
			borgholm_template_part( 'blog', 'templates/parts/post-format/gallery' );
			break;
		case 'video':
			borgholm_template_part( 'blog', 'templates/parts/post-format/video' );
			break;
		case 'audio':
			borgholm_template_part( 'blog', 'templates/parts/post-format/audio' );
			break;
		default:
			borgholm_template_part( 'blog', 'templates/parts/post-info/image' );
			break;
	} ?>
</div>