<?php
$tags = get_the_tags();

if ( $tags ) { ?>
	<div class="qodef-e-info-tag-icon">
		<?php echo borgholm_get_icon( 'icon_ribbon_alt', 'elegant-icons', '' );?>
	</div>
	<div class="qodef-e-info-item qodef-e-info-tags">
		<?php the_tags( '', '', '' ); ?>
	</div>
<?php } ?>