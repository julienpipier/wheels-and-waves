<?php if ( isset( $enable_filter ) && $enable_filter === 'yes' ) {
	$filter_items = borgholm_get_filter_items( $params );
	?>
	<div class="qodef-m-filter">
		<?php if ( ! empty( $filter_items ) ) { ?>
			<div class="qodef-m-filter-items">
				<span class="qodef-m-filter-opener">
					<?php echo qode_framework_icons()->render_icon( 'dripicons-view-apps', 'dripicons', array( 'icon_attributes' => array( 'class' => 'qodef-m-icon' ) ) ); ?>
					<?php esc_html_e( 'Filter', 'borgholm' ) ?>
				</span>
				<span class="qodef-m-filter-opener-target">
					<a class="qodef-m-filter-item qodef--active" href="#" data-taxonomy="<?php echo esc_attr( $taxonomy_filter ); ?>" data-filter="*">
						<span class="qodef-m-filter-item-name"><?php esc_html_e( 'Show All', 'borgholm' ) ?></span>
					</a>
					<?php foreach ( $filter_items as $item ) {
						$filter_value = is_numeric( $item->slug ) ? $item->term_id : $item->slug;
						?>
						<a class="qodef-m-filter-item" href="#" data-taxonomy="<?php echo esc_attr( $taxonomy_filter ); ?>" data-filter="<?php echo esc_attr( $filter_value ); ?>">
							<span class="qodef-m-filter-item-name"><?php echo esc_html( $item->name ); ?></span>
						</a>
					<?php } ?>
				</span>
			</div>
		<?php } ?>
	</div>
<?php } ?>