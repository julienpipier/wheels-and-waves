<?php

if ( ! function_exists( 'borgholm_include_masonry_scripts' ) ) {
	/**
	 * Function that include modules 3rd party scripts
	 */
	function borgholm_include_masonry_scripts() {
		wp_enqueue_script( 'isotope', BORGHOLM_INC_ROOT . '/masonry/assets/js/plugins/isotope.pkgd.min.js', array( 'jquery' ), false, true );
		wp_enqueue_script( 'packery', BORGHOLM_INC_ROOT . '/masonry/assets/js/plugins/packery-mode.pkgd.min.js', array( 'jquery' ), false, true );
	}
}

if ( ! function_exists( 'borgholm_enqueue_masonry_scripts_for_templates' ) ) {
	/**
	 * Function that enqueue modules 3rd party scripts for templates
	 */
	function borgholm_enqueue_masonry_scripts_for_templates() {
		$post_type = apply_filters( 'borgholm_filter_allowed_post_type_to_enqueue_masonry_scripts', '' );
		
		if ( ! empty( $post_type ) && is_singular( $post_type ) ) {
			borgholm_include_masonry_scripts();
		}
	}
	
	add_action( 'borgholm_action_before_main_js', 'borgholm_enqueue_masonry_scripts_for_templates' );
}

if ( ! function_exists( 'borgholm_enqueue_masonry_scripts_for_shortcodes' ) ) {
	/**
	 * Function that enqueue modules 3rd party scripts for shortcodes
	 *
	 * @param array $atts
	 */
	function borgholm_enqueue_masonry_scripts_for_shortcodes( $atts ) {
		
		if ( isset( $atts['behavior'] ) && $atts['behavior'] == 'masonry' ) {
			borgholm_include_masonry_scripts();
		}
	}
	
	add_action( 'borgholm_core_action_list_shortcodes_load_assets', 'borgholm_enqueue_masonry_scripts_for_shortcodes' );
}

if ( ! function_exists( 'borgholm_register_masonry_scripts_for_list_shortcodes' ) ) {
	/**
	 * Function that set module 3rd party scripts for list shortcodes
	 *
	 * @param array $scripts
	 *
	 * @return array
	 */
	function borgholm_register_masonry_scripts_for_list_shortcodes( $scripts ) {

		$scripts['isotope'] = array(
			'registered' => true
		);
		$scripts['packery'] = array(
			'registered' => true
		);

		return $scripts;
	}

	add_filter( 'borgholm_core_filter_register_list_shortcode_scripts', 'borgholm_register_masonry_scripts_for_list_shortcodes' );
}