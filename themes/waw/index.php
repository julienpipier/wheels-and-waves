<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/style.css" type="text/css" charset="utf-8" />
	<title>Wheels and Waves</title>
</head>
<body>
	<div class="content">



		<?php $host = $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
				if($host == 'wheels-and-waves.com/en'): ?>
					<h1><span class="font-18";">Come back in a few days</span>
					 	<br/><span class="submenu">to live the <span style="color:#c82e27">experience...</span></span>
					</h1>
					<p class="presentation">
					 	We believe in the resurgence of a freedom lifestyle where our adventures fulfill our desire to escape.
					</p>
					<a href="https://www.wheels-and-waves.com/store/?id_lang=2" class="btn-shop">Shop</a>

				<?php
				
				else:
				?>
				<h1><span class="font-18";">Revenez dans quelques jours</span>
				 	<br/><span class="submenu">pour vivre l'<span style="color:#c82e27">experience...</span></span>
				 </h1>
				<p class="presentation">
				 	Wheels and Waves c’est un état d’esprit,
					un esthétisme particulier, inspirés par la culture
					de la moto et bousculé par nos diverses influences.
					Nous nous appuyons sur le passé, sur les savoir-faire
					pour vivre les instants présents et parler de demain.
				</p>
				<a href="https://www.wheels-and-waves.com/store/?id_lang=2" class="btn-shop">La boutique</a>

		<?php
				endif;
?>
	</div>
	<footer>
		<div id="footer">
			<div id="INFORMATION__Contact_Mentions_">
				<a href="https://www.facebook.com/wheelswaves" target='_blank'><img src="<?php echo get_template_directory_uri(); ?>/images/001-facebook.svg" class="fb"></a>
				<a href="https://www.instagram.com/Wheels_and_waves/" target='_blank'><img src="<?php echo get_template_directory_uri(); ?>/images/003-instagram.svg" class="insta"></a>
				<a href="https://www.youtube.com/channel/UCsYyzxkvo_cAUGSDA0N2xTg?utm_source=Unknown+List&utm_campaign=d6a83c5a78-EMAIL_CAMPAIGN_2018_11_27_10_13&utm_medium=email&utm_term=0_-d6a83c5a78-" target='_blank'><img src="<?php echo get_template_directory_uri(); ?>/images/002-youtube.svg" class="yt"></a>
				<a href="https://open.spotify.com/user/177a1ysxyiubwbwxnd8xpc2y5?si=3R8KnCeoT_yw1yfaSIRQ5w&utm_source=Unknown+List&utm_campaign=d6a83c5a78-EMAIL_CAMPAIGN_2018_11_27_10_13&utm_medium=email&utm_term=0_-d6a83c5a78-" target='_blank'><img src="<?php echo get_template_directory_uri(); ?>/images/004-spotify.svg" class="spotify"></a>
			</div>
			<div id="Wheels_and_waves">
				<div class="date">BIARRITZ  30.06-04.07  2021</div>
				<span>Wheels and waves</span>
			</div>
		</div>
	</footer>
</body>
</html>