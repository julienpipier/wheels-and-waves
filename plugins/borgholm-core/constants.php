<?php

define( 'BORGHOLM_CORE_VERSION', '1.0' );
define( 'BORGHOLM_CORE_ABS_PATH', dirname( __FILE__ ) );
define( 'BORGHOLM_CORE_REL_PATH', dirname( plugin_basename( __FILE__ ) ) );
define( 'BORGHOLM_CORE_URL_PATH', plugin_dir_url( __FILE__ ) );
define( 'BORGHOLM_CORE_ASSETS_PATH', BORGHOLM_CORE_ABS_PATH . '/assets' );
define( 'BORGHOLM_CORE_ASSETS_URL_PATH', BORGHOLM_CORE_URL_PATH . 'assets' );
define( 'BORGHOLM_CORE_INC_PATH', BORGHOLM_CORE_ABS_PATH . '/inc' );
define( 'BORGHOLM_CORE_INC_URL_PATH', BORGHOLM_CORE_URL_PATH . 'inc' );
define( 'BORGHOLM_CORE_CPT_PATH', BORGHOLM_CORE_INC_PATH . '/post-types' );
define( 'BORGHOLM_CORE_CPT_URL_PATH', BORGHOLM_CORE_INC_URL_PATH . '/post-types' );
define( 'BORGHOLM_CORE_SHORTCODES_PATH', BORGHOLM_CORE_INC_PATH . '/shortcodes' );
define( 'BORGHOLM_CORE_SHORTCODES_URL_PATH', BORGHOLM_CORE_INC_URL_PATH . '/shortcodes' );
define( 'BORGHOLM_CORE_PLUGINS_PATH', BORGHOLM_CORE_INC_PATH . '/plugins' );
define( 'BORGHOLM_CORE_PLUGINS_URL_PATH', BORGHOLM_CORE_INC_URL_PATH . '/plugins' );
define( 'BORGHOLM_CORE_HEADER_LAYOUTS_PATH', BORGHOLM_CORE_INC_PATH . '/header/layouts' );
define( 'BORGHOLM_CORE_HEADER_LAYOUTS_URL_PATH', BORGHOLM_CORE_INC_URL_PATH . '/header/layouts' );
define( 'BORGHOLM_CORE_HEADER_ASSETS_PATH', BORGHOLM_CORE_INC_PATH . '/header/assets' );
define( 'BORGHOLM_CORE_HEADER_ASSETS_URL_PATH', BORGHOLM_CORE_INC_URL_PATH . '/header/assets' );

define( 'BORGHOLM_CORE_MENU_NAME', 'borgholm_core_menu' );
define( 'BORGHOLM_CORE_OPTIONS_NAME', 'borgholm_core_options' );

define( 'BORGHOLM_CORE_PROFILE_SLUG', 'select' );