<?php

if ( ! function_exists( 'borgholm_core_dependency_for_mobile_menu_typography_options' ) ) {
	/**
	 * Function that set dependency values for module global options
	 *
	 * @return array
	 */
	function borgholm_core_dependency_for_mobile_menu_typography_options() {
		return apply_filters( 'borgholm_core_filter_mobile_menu_typography_hide_option', $hide_dep_options = array() );
	}
}