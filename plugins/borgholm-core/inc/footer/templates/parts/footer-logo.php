<div class="qodef-footer-logo">
    <?php $footer_logo = wp_get_attachment_image( $footer_logo_id, 'full' );
    $footer_logo_html = ! empty( $footer_logo ) ? $footer_logo : qode_framework_get_image_html_from_src( $footer_logo_id );
    echo qode_framework_wp_kses_html( 'img', $footer_logo_html ); ?>
</div>