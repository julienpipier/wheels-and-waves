<?php

if ( ! function_exists( 'borgholm_core_add_sticky_sidebar_widget' ) ) {
	/**
	 * Function that add widget into widgets list for registration
	 *
	 * @param array $widgets
	 *
	 * @return array
	 */
	function borgholm_core_add_sticky_sidebar_widget( $widgets ) {
		$widgets[] = 'BorgholmCoreStickySidebarWidget';
		
		return $widgets;
	}
	
	add_filter( 'borgholm_core_filter_register_widgets', 'borgholm_core_add_sticky_sidebar_widget' );
}

if ( class_exists( 'QodeFrameworkWidget' ) ) {
	class BorgholmCoreStickySidebarWidget extends QodeFrameworkWidget {
		
		public function map_widget() {
			$this->set_base( 'borgholm_core_sticky_sidebar' );
			$this->set_name( esc_html__( 'Borgholm Sticky Sidebar', 'borgholm-core' ) );
			$this->set_description( esc_html__( 'Use this widget to make the sidebar sticky. Drag it into the sidebar above the widget which you want to be the first element in the sticky sidebar', 'borgholm-core' ) );
		}
		
		public function render( $atts ) {
		}
	}
}
