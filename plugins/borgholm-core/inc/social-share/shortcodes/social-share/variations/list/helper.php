<?php

if ( ! function_exists( 'borgholm_core_add_social_share_variation_list' ) ) {
	/**
	 * Function that add variation layout for this module
	 *
	 * @param array $variations
	 *
	 * @return array
	 */
	function borgholm_core_add_social_share_variation_list( $variations ) {
		$variations['list'] = esc_html__( 'List', 'borgholm-core' );
		
		return $variations;
	}
	
	add_filter( 'borgholm_core_filter_social_share_layouts', 'borgholm_core_add_social_share_variation_list' );
}
