<?php

if ( ! function_exists( 'borgholm_core_add_social_share_variation_text' ) ) {
	/**
	 * Function that add variation layout for this module
	 *
	 * @param array $variations
	 *
	 * @return array
	 */
	function borgholm_core_add_social_share_variation_text( $variations ) {
		$variations['text'] = esc_html__( 'Text', 'borgholm-core' );
		
		return $variations;
	}
	
	add_filter( 'borgholm_core_filter_social_share_layouts', 'borgholm_core_add_social_share_variation_text' );
}
