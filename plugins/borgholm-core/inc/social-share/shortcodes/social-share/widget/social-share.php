<?php

if ( ! function_exists( 'borgholm_core_add_social_share_widget' ) ) {
	/**
	 * Function that add widget into widgets list for registration
	 *
	 * @param array $widgets
	 *
	 * @return array
	 */
	function borgholm_core_add_social_share_widget( $widgets ) {
		$widgets[] = 'BorgholmCoreSocialShareWidget';
		
		return $widgets;
	}
	
	add_filter( 'borgholm_core_filter_register_widgets', 'borgholm_core_add_social_share_widget' );
}

if ( class_exists( 'QodeFrameworkWidget' ) ) {
	class BorgholmCoreSocialShareWidget extends QodeFrameworkWidget {
		
		public function map_widget() {
			$widget_mapped = $this->import_shortcode_options( array(
				'shortcode_base' => 'borgholm_core_social_share'
			) );
			if( $widget_mapped ) {
				$this->set_base( 'borgholm_core_social_share' );
				$this->set_name( esc_html__( 'Borgholm Social Share', 'borgholm-core' ) );
				$this->set_description( esc_html__( 'Add a social share element into widget areas', 'borgholm-core' ) );
			}
		}
		
		public function render( $atts ) {
			$params = $this->generate_string_params( $atts );
			
			echo do_shortcode( "[borgholm_core_social_share $params]" ); // XSS OK
		}
	}
}