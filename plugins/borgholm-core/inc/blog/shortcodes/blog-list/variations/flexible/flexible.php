<?php

if ( ! function_exists( 'borgholm_core_add_blog_list_variation_flexible' ) ) {
	function borgholm_core_add_blog_list_variation_flexible( $variations ) {
		$variations['flexible'] = esc_html__( 'Flexible', 'borgholm-core' );
		
		return $variations;
	}
	
	add_filter( 'borgholm_core_filter_blog_list_layouts', 'borgholm_core_add_blog_list_variation_flexible' );
}

if ( ! function_exists( 'borgholm_core_load_blog_list_variation_flexible_assets' ) ) {
	function borgholm_core_load_blog_list_variation_flexible_assets( $is_enabled, $params ) {
		
		if ( $params['layout'] === 'flexible' ) {
			$is_enabled = true;
		}
		
		return $is_enabled;
	}
	
	add_filter( 'borgholm_core_filter_load_blog_list_assets', 'borgholm_core_load_blog_list_variation_flexible_assets', 10, 2 );
}

if ( ! function_exists( 'borgholm_core_add_blog_list_options_enable_author' ) ) {
	function borgholm_core_add_blog_list_options_enable_author( $options ) {
		$blog_list_options   = array();
		
		$blog_list_options[] = array(
			'field_type'    => 'select',
			'name'          => 'enable_author',
			'title'         => esc_html__( 'Enable Author', 'borgholm-core' ),
			'options'       => borgholm_core_get_select_type_options_pool( 'yes_no', false ),
			'default_value' => 'yes',
			'dependency'    => array(
				'show' => array(
					'layout' => array(
						'values'        => 'flexible',
						'default_value' => 'flexible'
					)
				)
			),
			'group'         => esc_html__( 'Additional Features', 'borgholm-core' )
		);
		
		$blog_list_options[] = array(
			'field_type'    => 'select',
			'name'          => 'enable_date',
			'title'         => esc_html__( 'Enable Date', 'borgholm-core' ),
			'options'       => borgholm_core_get_select_type_options_pool( 'yes_no', false ),
			'default_value' => 'yes',
			'dependency'    => array(
				'show' => array(
					'layout' => array(
						'values'        => 'flexible',
						'default_value' => 'flexible'
					)
				)
			),
			'group'         => esc_html__( 'Additional Features', 'borgholm-core' )
		);
		
		$blog_list_options[] = array(
			'field_type'    => 'select',
			'name'          => 'enable_image',
			'title'         => esc_html__( 'Enable Image', 'borgholm-core' ),
			'options'       => borgholm_core_get_select_type_options_pool( 'yes_no', false ),
			'default_value' => 'yes',
			'dependency'    => array(
				'show' => array(
					'layout' => array(
						'values'        => 'flexible',
						'default_value' => 'flexible'
					)
				)
			),
			'group'         => esc_html__( 'Additional Features', 'borgholm-core' )
		);
		
		$blog_list_options[] = array(
			'field_type'    => 'select',
			'name'          => 'enable_excerpt',
			'title'         => esc_html__( 'Enable Excerpt', 'borgholm-core' ),
			'options'       => borgholm_core_get_select_type_options_pool( 'yes_no', false ),
			'default_value' => 'yes',
			'dependency'    => array(
				'show' => array(
					'layout' => array(
						'values'        => 'flexible',
						'default_value' => 'flexible'
					)
				)
			),
			'group'         => esc_html__( 'Additional Features', 'borgholm-core' )
		);
		
		$blog_list_options[] = array(
			'field_type'    => 'select',
			'name'          => 'enable_category',
			'title'         => esc_html__( 'Enable Category', 'borgholm-core' ),
			'options'       => borgholm_core_get_select_type_options_pool( 'yes_no', false ),
			'default_value' => 'yes',
			'dependency'    => array(
				'show' => array(
					'layout' => array(
						'values'        => 'flexible',
						'default_value' => 'flexible'
					)
				)
			),
			'group'         => esc_html__( 'Additional Features', 'borgholm-core' )
		);
		
		$blog_list_options[] = array(
			'field_type'    => 'select',
			'name'          => 'enable_button',
			'title'         => esc_html__( 'Enable Button', 'borgholm-core' ),
			'options'       => borgholm_core_get_select_type_options_pool( 'yes_no', false ),
			'default_value' => 'yes',
			'dependency'    => array(
				'show' => array(
					'layout' => array(
						'values'        => 'flexible',
						'default_value' => 'flexible'
					)
				)
			),
			'group'         => esc_html__( 'Additional Features', 'borgholm-core' )
		);
		
		return array_merge( $options, $blog_list_options );
	}
	
	add_filter( 'borgholm_core_filter_blog_list_extra_options', 'borgholm_core_add_blog_list_options_enable_author', 10, 1 );
}