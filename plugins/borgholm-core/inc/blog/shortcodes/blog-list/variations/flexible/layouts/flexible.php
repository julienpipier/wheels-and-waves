<article <?php post_class( $item_classes ); ?>>
	<div class="qodef-e-inner">
		<?php
		// Include post media
		if ( $enable_image !== 'no' ) {
			borgholm_core_template_part( 'blog/shortcodes/blog-list', 'templates/post-info/media', '', $params );
		}
		?>
		<div class="qodef-e-content" <?php qode_framework_inline_style( $this_shortcode->get_content_styles( $params ) ); ?>>
			<div class="qodef-e-info qodef-info--top">
				<?php
				// Include post author info
				if ( $enable_author !== 'no' ) {
					borgholm_core_theme_template_part( 'blog', 'templates/parts/post-info/author' );
				}
				
				// Include post date info
				if ( $enable_date !== 'no' ) {
					borgholm_core_theme_template_part( 'blog', 'templates/parts/post-info/date' );
				}
				
				// Include post category info
				if ( $enable_category !== 'no' ) {
					borgholm_core_theme_template_part( 'blog', 'templates/parts/post-info/category' );
				}
				?>
			</div>
			<div class="qodef-e-text">
				<?php
				// Include post title
				borgholm_core_template_part( 'blog/shortcodes/blog-list', 'templates/post-info/title', '', $params );
				
				// Include post excerpt
				if ( $enable_excerpt !== 'no' ) {
					borgholm_core_theme_template_part( 'blog', 'templates/parts/post-info/excerpt', '', $params );
				}
				
				// Hook to include additional content after blog single content
				do_action( 'borgholm_action_after_blog_single_content' );
				?>
			</div>
			<?php if ( $enable_button !== 'no' ) { ?>
				<div class="qodef-e-info qodef-info--bottom">
					<div class="qodef-e-info-left">
						<?php
						// Include post read more
						borgholm_core_theme_template_part( 'blog', 'templates/parts/post-info/read-more' );
						?>
					</div>
				</div>
			<?php } ?>
		</div>
	</div>
</article>