<?php

if ( ! function_exists( 'borgholm_core_add_blog_list_variation_minimal' ) ) {
	/**
	 * Function that add variation layout for this module
	 *
	 * @param array $variations
	 *
	 * @return array
	 */
	function borgholm_core_add_blog_list_variation_minimal( $variations ) {
		$variations['minimal'] = esc_html__( 'Minimal', 'borgholm-core' );
		
		return $variations;
	}
	
	add_filter( 'borgholm_core_filter_blog_list_layouts', 'borgholm_core_add_blog_list_variation_minimal' );
}