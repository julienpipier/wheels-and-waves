<?php

if ( ! function_exists( 'borgholm_core_add_product_list_extra_options' ) ) {
	function borgholm_core_add_product_list_extra_options( $options ) {
		$product_list_options   = array();

		$product_list_options[] = array(
			'field_type'    => 'select',
			'name'          => 'skin',
			'title'         => esc_html__( 'Skin', 'borgholm-core' ),
			'options'       => array(
				''      => esc_html__( 'Default', 'borgholm-core' ),
				'light' => esc_html__( 'Light', 'borgholm-core' )
			),
			'default_value' => '',
			'group'         => esc_html__( 'Additional Features', 'borgholm-core' )
		);

		$product_list_options[] = array(
			'field_type'    => 'select',
			'name'          => 'slider_layout',
			'title'         => esc_html__( 'Slider Layout', 'borgholm-core' ),
			'options'       => array(
				''                    => esc_html__( 'Default', 'borgholm-core' ),
				'center-item-overlap' => esc_html__( 'Center Item Overlap', 'borgholm-core' ),
				'info-on-hover'       => esc_html__( 'Info On Hover', 'borgholm-core' )
			),
			'default_value' => '',
			'dependency'    => array(
				'show' => array(
					'behavior' => array(
						'values'        => 'slider',
						'default_value' => 'columns'
					)
				)
			),
			'group'         => esc_html__( 'Additional Features', 'borgholm-core' )
		);

		$product_list_options[] = array(
			'field_type'    => 'select',
			'name'          => 'item_layout_style',
			'title'         => esc_html__( 'Item Layout', 'borgholm-core' ),
			'options'       => array(
				''       => esc_html__( 'Default', 'borgholm-core' ),
				'custom' => esc_html__( 'Vertical Sequence', 'borgholm-core' )
			),
			'default_value' => '',
			'group'         => esc_html__( 'Additional Features', 'borgholm-core' )
		);

		$product_list_options[] = array(
			'field_type'    => 'select',
			'name'          => 'enable_rating',
			'title'         => esc_html__( 'Enable Rating', 'borgholm-core' ),
			'options'       => borgholm_core_get_select_type_options_pool( 'no_yes', false ),
			'default_value' => 'no',
			'group'         => esc_html__( 'Additional Features', 'borgholm-core' )
		);

		return array_merge( $options, $product_list_options );
	}

	add_filter( 'borgholm_core_filter_product_list_extra_options', 'borgholm_core_add_product_list_extra_options', 10, 1 );
}