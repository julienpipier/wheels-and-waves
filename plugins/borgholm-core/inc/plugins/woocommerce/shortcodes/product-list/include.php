<?php

include_once BORGHOLM_CORE_PLUGINS_PATH . '/woocommerce/shortcodes/product-list/helper.php';
include_once BORGHOLM_CORE_PLUGINS_PATH . '/woocommerce/shortcodes/product-list/product-list.php';

foreach ( glob( BORGHOLM_CORE_PLUGINS_PATH . '/woocommerce/shortcodes/product-list/variations/*/include.php' ) as $variation ) {
	include_once $variation;
}