(function ($) {
	"use strict";
	
	var shortcode = 'borgholm_core_product_list';
	
	qodefCore.shortcodes[shortcode] = {};
	
	if (typeof qodefCore.listShortcodesScripts === 'object') {
		$.each(qodefCore.listShortcodesScripts, function (key, value) {
			qodefCore.shortcodes[shortcode][key] = value;
		});
	}

	$(document).ready(function () {
		qodefProductListSlider.init();
	});

	var qodefProductListSlider = {
		init: function () {
			var $holder = $('.qodef-woo-product-list.qodef-slider-layout--center-item-overlap');

			if ($holder.length) {
				$holder.each(function() {
					var $thisHolder = $(this),
						$activeSlideImgHolder = $thisHolder.find('.swiper-slide-active .qodef-woo-product-image'),
						$contentHolder = $thisHolder.find('.qodef-woo-product-content'),
						augmentationFactor = 0,
						thisSwiper = $thisHolder[0].swiper;

					thisSwiper.on('touchMove', function() {
						$thisHolder.addClass('qodef-slider--dragging');
					});

					thisSwiper.on('touchEnd', function() {
						setTimeout(function() {
							$thisHolder.removeClass('qodef-slider--dragging');
						}, 100);
					});

					if($activeSlideImgHolder.length) {
						augmentationFactor = parseInt($activeSlideImgHolder.outerHeight() * 0.0965);
						if(isNaN(augmentationFactor) || augmentationFactor < 0) {
							augmentationFactor = 0;
						}
					}

					if($contentHolder.length && augmentationFactor > 0) {
						$contentHolder.css({paddingTop: augmentationFactor});
					}
				});
			}
		}
	};

	qodefCore.shortcodes[shortcode].qodefProductListSlider = qodefProductListSlider;

})(jQuery);