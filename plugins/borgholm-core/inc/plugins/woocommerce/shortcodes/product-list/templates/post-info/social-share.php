<?php if ( class_exists( 'BorgholmCoreSocialShareShortcode' ) ) { ?>
	<div class="qodef-woo-product-social-share">
		<?php
		$params = array();
		$params['title'] = esc_html__( 'Share:', 'borgholm-core' );
		
		echo BorgholmCoreSocialShareShortcode::call_shortcode( $params ); ?>
	</div>
<?php } ?>