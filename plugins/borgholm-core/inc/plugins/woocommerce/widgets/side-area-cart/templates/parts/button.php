<div class="qodef-m-action">
	<a itemprop="url" href="<?php echo esc_url( wc_get_cart_url() ); ?>" class="qodef-m-action-link qodef-cart"><?php esc_html_e( 'Shopping Bag', 'borgholm-core' ); ?></a>
	<a itemprop="url" href="<?php echo esc_url( wc_get_checkout_url() ); ?>" class="qodef-m-action-link qodef-checkout"><?php esc_html_e( 'Checkout', 'borgholm-core' ); ?></a>
</div>