<div class="qodef-m-content">
	<?php if ( ! WC()->cart->is_empty() ) {
		borgholm_core_template_part( 'plugins/woocommerce/widgets/side-area-cart', 'templates/parts/loop' );
		
		borgholm_core_template_part( 'plugins/woocommerce/widgets/side-area-cart', 'templates/parts/order-details' );
		
		borgholm_core_template_part( 'plugins/woocommerce/widgets/side-area-cart', 'templates/parts/button' );
	} else {
		// Include posts not found
		borgholm_core_template_part( 'plugins/woocommerce/widgets/side-area-cart', 'templates/parts/posts-not-found' );
	}
	
	borgholm_core_template_part( 'plugins/woocommerce/widgets/side-area-cart', 'templates/parts/close' );
	?>
</div>