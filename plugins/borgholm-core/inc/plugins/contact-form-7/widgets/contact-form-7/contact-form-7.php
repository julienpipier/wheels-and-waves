<?php

if ( ! function_exists( 'borgholm_core_add_contact_form_7_widget' ) ) {
	/**
	 * Function that add widget into widgets list for registration
	 *
	 * @param array $widgets
	 *
	 * @return array
	 */
	function borgholm_core_add_contact_form_7_widget( $widgets ) {
		$widgets[] = 'BorgholmCoreContactForm7Widget';
		
		return $widgets;
	}
	
	add_filter( 'borgholm_core_filter_register_widgets', 'borgholm_core_add_contact_form_7_widget' );
}

if ( class_exists( 'QodeFrameworkWidget' ) ) {
	class BorgholmCoreContactForm7Widget extends QodeFrameworkWidget {
		
		public function map_widget() {
			$this->set_base( 'borgholm_core_contact_form_7' );
			$this->set_name( esc_html__( 'Borgholm Contact Form 7', 'borgholm-core' ) );
			$this->set_description( esc_html__( 'Add contact form 7 to widget areas', 'borgholm-core' ) );
			$this->set_widget_option(
				array(
					'field_type' => 'text',
					'name'       => 'widget_title',
					'title'      => esc_html__( 'Widget Title', 'borgholm-core' )
				)
			);
			$this->set_widget_option(
				array(
					'field_type' => 'text',
					'name'       => 'custom_class',
					'title'      => esc_html__( 'Custom Class', 'borgholm-core' )
				)
			);
			$this->set_widget_option(
				array(
					'field_type' => 'select',
					'name'       => 'contact_form_id',
					'title'      => esc_html__( 'Select Contact Form 7', 'borgholm-core' ),
					'options'    => borgholm_core_get_contact_form_7_forms()
				)
			);
		}
		
		public function render( $atts ) {

			$holder_classes = $this->get_holder_classes( $atts );
			?>
			<div <?php qode_framework_class_attribute( $holder_classes ); ?>>
				<?php if ( ! empty( $atts['contact_form_id'] ) ) {
					echo do_shortcode( '[contact-form-7 id="' . esc_attr( $atts['contact_form_id'] ) . '"]' ); // XSS OK
				} ?>
			</div>
			<?php
		}

		private function get_holder_classes( $atts ) {
			$holder_classes = array('qodef-contact-form-7');

			$holder_classes[] = ! empty( $atts['custom_class'] ) ? esc_attr( $atts['custom_class'] ) : '';

			return implode( ' ', $holder_classes );
		}
	}
}