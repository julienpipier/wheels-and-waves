<?php

if ( qode_framework_is_installed( 'elementor' ) ) {
	include_once BORGHOLM_CORE_PLUGINS_PATH .'/elementor/helper.php';
	include_once BORGHOLM_CORE_PLUGINS_PATH .'/elementor/section-handler-class.php';
}