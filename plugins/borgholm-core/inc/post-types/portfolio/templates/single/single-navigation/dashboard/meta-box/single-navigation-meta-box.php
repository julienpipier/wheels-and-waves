<?php

if ( ! function_exists( 'borgholm_core_add_portfolio_single_navigation_meta_box' ) ) {
	/**
	 * Function that add general meta box options for this module
	 */
	function borgholm_core_add_portfolio_single_navigation_meta_box( $page, $general_tab ) {
		
		if ( $page ) {

			$general_tab->add_field_element(
				array(
					'field_type'    => 'select',
					'name'          => 'qodef_portfolio_enable_navigation',
					'title'         => esc_html__( 'Navigation', 'borgholm-core' ),
					'description'   => esc_html__( 'Enabling this option will turn on portfolio navigation functionality', 'borgholm-core' ),
					'options'       => array(
						''    => esc_html__( 'Default', 'borgholm-core' ),
						'yes' => esc_html__( 'Yes', 'borgholm-core' ),
						'no'  => esc_html__( 'No', 'borgholm-core' )
					),
					'default_value' => ''
				)
			);

			$general_tab->add_field_element(
				array(
					'field_type'  => 'select',
					'name'        => 'qodef_portfolio_single_navigation_in_grid',
					'title'       => esc_html__( 'Navigation in Grid', 'borgholm-core' ),
					'options'     => array(
						''       => esc_html__( 'Default', 'borgholm-core' ),
						'yes'    => esc_html__( 'Yes', 'borgholm-core' )
					),
				)
			);

			$general_tab->add_field_element(
				array(
					'field_type'  => 'select',
					'name'        => 'qodef_portfolio_single_navigation_skin',
					'title'       => esc_html__( 'Navigation Skin', 'borgholm-core' ),
					'options'     => array(
						''      => esc_html__( 'Default', 'borgholm-core' ),
						'light' => esc_html__( 'Light', 'borgholm-core' )
					),
				)
			);

			$general_tab->add_field_element(
				array(
					'field_type'  => 'select',
					'name'        => 'qodef_portfolio_single_back_to_link',
					'title'       => esc_html__( 'Back To Link', 'borgholm-core' ),
					'description' => esc_html__( 'Choose "Back To" page to link from portfolio single', 'borgholm-core' ),
					'options'     => qode_framework_get_pages( true ),
				)
			);
		}
	}
	
	add_action( 'borgholm_core_action_after_portfolio_meta_box_map', 'borgholm_core_add_portfolio_single_navigation_meta_box', 10, 2 );
}