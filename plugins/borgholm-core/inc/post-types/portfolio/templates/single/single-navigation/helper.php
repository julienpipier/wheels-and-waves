<?php

if ( ! function_exists( 'borgholm_core_include_portfolio_single_post_navigation_template' ) ) {
	/**
	 * Function which includes additional module on single portfolio page
	 */
	function borgholm_core_include_portfolio_single_post_navigation_template() {
		borgholm_core_template_part( 'post-types/portfolio', 'templates/single/single-navigation/templates/single-navigation' );
	}
	
	add_action( 'borgholm_core_action_after_portfolio_single_item', 'borgholm_core_include_portfolio_single_post_navigation_template' );
}