<div class="qodef-e qodef-info--date">
	<div class="qodef-e-title"><?php esc_html_e( 'Date: ', 'borgholm-core' ); ?></div>
	<p itemprop="dateCreated" class="entry-date updated"><?php the_time( get_option( 'date_format' ) ); ?></p>
	<meta itemprop="interactionCount" content="UserComments: <?php echo get_comments_number( get_the_ID() ); ?>"/>
</div>