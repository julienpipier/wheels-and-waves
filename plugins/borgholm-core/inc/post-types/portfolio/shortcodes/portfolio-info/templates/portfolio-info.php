<div <?php qode_framework_class_attribute( $holder_classes ); ?>>
	<?php borgholm_core_list_sc_template_part( 'post-types/portfolio/shortcodes/portfolio-info', 'post-info/custom-fields', '', $params ); ?>
	<?php borgholm_core_list_sc_template_part( 'post-types/portfolio/shortcodes/portfolio-info', 'post-info/date', '', $params ); ?>
	<?php borgholm_core_list_sc_template_part( 'post-types/portfolio/shortcodes/portfolio-info', 'post-info/social-share', '', $params ); ?>
</div>