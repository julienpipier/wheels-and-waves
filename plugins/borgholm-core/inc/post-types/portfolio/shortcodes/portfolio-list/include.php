<?php

include_once BORGHOLM_CORE_CPT_PATH . '/portfolio/shortcodes/portfolio-list/helper.php';
include_once BORGHOLM_CORE_CPT_PATH . '/portfolio/shortcodes/portfolio-list/portfolio-list.php';

foreach ( glob( BORGHOLM_CORE_CPT_PATH . '/portfolio/shortcodes/portfolio-list/variations/*/include.php' ) as $variation ) {
	include_once $variation;
}