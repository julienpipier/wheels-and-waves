<?php
if ( ! function_exists( 'borgholm_core_filter_portfolio_list_info_follow' ) ) {
	/**
	 * Function that add variation layout for this module
	 *
	 * @param array $variations
	 *
	 * @return array
	 */
	function borgholm_core_filter_portfolio_list_info_follow( $variations ) {

		$variations['follow'] = esc_html__( 'Follow', 'borgholm-core' );

		return $variations;
	}

	add_filter( 'borgholm_core_filter_portfolio_list_info_follow_animation_options', 'borgholm_core_filter_portfolio_list_info_follow' );
}