(function ($) {

    "use strict";

    $(document).ready(function () {
        qodefPortfolioInfoBelow.init();
    });

    var qodefPortfolioInfoBelow = {
        init: function () {
            this.holder = $('.qodef-portfolio-list.qodef-item-layout--info-below');

            if ( this.holder.length ) {
                this.holder.each( function () {
                    if ( $(this).hasClass('qodef-info-below-appear-animation') ) {
                        qodefPortfolioInfoBelow.appearAnimation( $(this) );
                    }
                });
            }
        },
        appearAnimation: function ( $holder ) {
            var $item = $holder.find('.qodef-e'),
                $delay = 0;

            function getRandomArbitrary(min, max) {
                return Math.floor(Math.random() * (max - min) + min);
            }

            if ( $holder.hasClass('qodef-delay') ) {
                $delay = 750;
            }

            setTimeout( function () {
                $item.each( function() {
                    var $thisItem = $(this),
                        $randomNum = getRandomArbitrary(10, 400);

                    $thisItem.appear(function () {
                        setTimeout(function () {
                            $thisItem.addClass('qodef--appear ');
                        }, $randomNum);
                    }, {accX: 0, accY: -50});
                })
            }, $delay);
        }
    };

})(jQuery);