<?php

if ( ! function_exists( 'borgholm_core_filter_portfolio_list_info_below_tilt' ) ) {
	/**
	 * Function that add variation layout for this module
	 *
	 * @param array $variations
	 *
	 * @return array
	 */
	function borgholm_core_filter_portfolio_list_info_below_tilt( $variations ) {
		$variations['tilt'] = esc_html__( 'Tilt', 'borgholm-core' );
		
		return $variations;
	}
	
	add_filter( 'borgholm_core_filter_portfolio_list_info_below_animation_options', 'borgholm_core_filter_portfolio_list_info_below_tilt' );
}