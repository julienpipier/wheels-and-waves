<?php

if ( ! function_exists( 'borgholm_core_add_portfolio_list_variation_info_below' ) ) {
	/**
	 * Function that add variation layout for this module
	 *
	 * @param array $variations
	 *
	 * @return array
	 */
	function borgholm_core_add_portfolio_list_variation_info_below( $variations ) {
		
		$variations['info-below'] = esc_html__( 'Info Below', 'borgholm-core' );
		
		return $variations;
	}
	
	add_filter( 'borgholm_core_filter_portfolio_list_layouts', 'borgholm_core_add_portfolio_list_variation_info_below' );
}

if ( ! function_exists( 'borgholm_core_add_portfolio_list_options_info_below' ) ) {
	/**
	 * Function that add additional options for variation layout
	 *
	 * @param array $options
	 *
	 * @return array
	 */
	function borgholm_core_add_portfolio_list_options_info_below( $options ) {
		$info_below_options   = array();
		$margin_option        = array(
			'field_type' => 'text',
			'name'       => 'info_below_content_padding_bottom',
			'title'      => esc_html__( 'Content Bottom Padding', 'borgholm-core' ),
			'dependency' => array(
				'show' => array(
					'layout' => array(
						'values'        => 'info-below',
						'default_value' => ''
					)
				)
			),
			'group'      => esc_html__( 'Layout', 'borgholm-core' )
		);
		$hover_option         = array(
            'field_type'    => 'select',
            'name'          => 'info_below_appear_animation',
            'title'         => esc_html__( 'Appear Animation', 'borgholm-core' ),
            'options'       => borgholm_core_get_select_type_options_pool( 'yes_no', false ),
            'default_value' => 'no',
            'dependency'    => array(
                'show' => array(
                    'layout' => array(
                        'values'        => 'info-below',
                        'default_value' => ''
                    )
                )
            ),
            'group'         => esc_html__( 'Layout', 'borgholm-core' )
        );
        $appear_option        = array(
            'field_type'    => 'select',
            'name'          => 'info_below_disable_hover_animation',
            'title'         => esc_html__( 'Disable Hover Animation', 'borgholm-core' ),
            'options'       => borgholm_core_get_select_type_options_pool( 'yes_no', false ),
            'default_value' => 'no',
            'dependency'    => array(
                'show' => array(
                    'layout' => array(
                        'values'        => 'info-below',
                        'default_value' => ''
                    )
                )
            ),
            'group'         => esc_html__( 'Layout', 'borgholm-core' )
        );
		$info_below_options[] = $margin_option;
		$info_below_options[] = $appear_option;
        $info_below_options[] = $hover_option;
		
		return array_merge( $options, $info_below_options );
	}
	
	add_filter( 'borgholm_core_filter_portfolio_list_extra_options', 'borgholm_core_add_portfolio_list_options_info_below' );
}