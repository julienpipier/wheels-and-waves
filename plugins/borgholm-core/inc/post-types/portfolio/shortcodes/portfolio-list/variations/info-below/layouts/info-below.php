<?php
$styles = array();
if ( ! empty( $info_below_content_padding_bottom ) ) {
	$padding_bottom = qode_framework_string_ends_with_space_units( $info_below_content_padding_bottom ) ? $info_below_content_padding_bottom : intval( $info_below_content_padding_bottom ) . 'px';
	$styles[] = 'padding-bottom:' . $padding_bottom;
}
?>
<article <?php post_class( $item_classes ); ?>>
	<div class="qodef-e-inner" <?php qode_framework_inline_style( $this_shortcode->get_list_item_style( $params ) ) ?>>
		<div class="qodef-e-image">
			<?php borgholm_core_list_sc_template_part( 'post-types/portfolio/shortcodes/portfolio-list', 'post-info/image', '', $params ); ?>
		</div>
		<div class="qodef-e-content" <?php qode_framework_inline_style( $styles ); ?>>
			<?php borgholm_core_list_sc_template_part( 'post-types/portfolio/shortcodes/portfolio-list', 'post-info/category', '', $params ); ?>
			<?php borgholm_core_list_sc_template_part( 'post-types/portfolio/shortcodes/portfolio-list', 'post-info/title', '', $params ); ?>
		</div>
	</div>
</article>