<?php

if ( ! function_exists( 'borgholm_core_add_portfolio_list_variation_info_follow' ) ) {
	/**
	 * Function that add variation layout for this module
	 *
	 * @param array $variations
	 *
	 * @return array
	 */
	function borgholm_core_add_portfolio_list_variation_info_follow( $variations ) {
		
		$variations['info-follow'] = esc_html__( 'Info Follow', 'borgholm-core' );
		
		return $variations;
	}
	
	add_filter( 'borgholm_core_filter_portfolio_list_layouts', 'borgholm_core_add_portfolio_list_variation_info_follow' );
}