<?php

if ( ! function_exists( 'borgholm_core_add_portfolio_list_extra_options' ) ) {
	function borgholm_core_add_portfolio_list_extra_options( $options ) {
		$portfolio_list_options   = array();

		$portfolio_list_options[] = array(
			'field_type'    => 'select',
			'name'          => 'slider_layout',
			'title'         => esc_html__( 'Slider Layout', 'borgholm-core' ),
			'options'       => array(
				''                  => esc_html__( 'Default', 'borgholm-core' ),
				'5-column-centered' => esc_html__( '5 Column Centered', 'borgholm-core' )
			),
			'default_value' => '',
			'dependency'    => array(
				'show' => array(
					'behavior' => array(
						'values'        => 'slider',
						'default_value' => 'columns'
					)
				)
			),
			'group'         => esc_html__( 'Additional Features', 'borgholm-core' )
		);

		return array_merge( $options, $portfolio_list_options );
	}

	add_filter( 'borgholm_core_filter_portfolio_list_extra_options', 'borgholm_core_add_portfolio_list_extra_options', 10, 1 );
}