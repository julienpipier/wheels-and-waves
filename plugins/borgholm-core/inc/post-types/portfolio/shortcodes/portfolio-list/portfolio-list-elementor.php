<?php

class BorgholmCoreElementorPortfolioList extends BorgholmCoreElementorWidgetBase {
	
	function __construct( array $data = [], $args = null ) {
		$this->set_shortcode_slug( 'borgholm_core_portfolio_list' );
		
		parent::__construct( $data, $args );
	}
}

borgholm_core_get_elementor_widgets_manager()->register_widget_type( new BorgholmCoreElementorPortfolioList() );