<div <?php qode_framework_class_attribute( $holder_classes ); ?> <?php qode_framework_inline_attr( $slider_attr, 'data-options' ); ?>>
	<div class="swiper-wrapper">
		<?php
		// Include items
		borgholm_core_template_part( 'post-types/portfolio/shortcodes/portfolio-list', 'templates/loop', '', $params );
		?>
	</div>
	<?php if ( $slider_navigation !== 'no' ) { ?>
		<div class="swiper-button-next"><?php echo borgholm_arrow_slim_right_svg(); ?></div>
		<div class="swiper-button-prev"><?php echo borgholm_arrow_slim_left_svg(); ?></div>
	<?php } ?>
	<?php if ( $slider_pagination !== 'no' ) { ?>
		<div class="swiper-pagination"></div>
	<?php } ?>
</div>