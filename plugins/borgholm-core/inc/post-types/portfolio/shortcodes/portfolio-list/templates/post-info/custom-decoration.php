<div class="qodef-e-custom-decoration">
    <img src="<?php echo esc_url( BORGHOLM_CORE_ASSETS_URL_PATH . '/img/custom-decoration.svg' ); ?>" alt="<?php esc_attr_e( 'Loading Image', 'borgholm' ); ?>" />
</div>