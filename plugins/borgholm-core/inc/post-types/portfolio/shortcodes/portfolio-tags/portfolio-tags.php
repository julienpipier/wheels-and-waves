<?php

if ( ! function_exists( 'borgholm_core_add_portfolio_tags_shortcode' ) ) {
	/**
	 * Function that isadding shortcode into shortcodes list for registration
	 *
	 * @param array $shortcodes - Array of registered shortcodes
	 *
	 * @return array
	 */
	function borgholm_core_add_portfolio_tags_shortcode( $shortcodes ) {
		$shortcodes[] = 'BorgholmCorePortfolioTagsShortcode';
		
		return $shortcodes;
	}
	
	add_filter( 'borgholm_core_filter_register_shortcodes', 'borgholm_core_add_portfolio_tags_shortcode' );
}

if ( class_exists( 'QodeFrameworkShortcode' ) ) {
	class BorgholmCorePortfolioTagsShortcode extends QodeFrameworkShortcode {
		
		public function __construct() {
			parent::__construct();
		}
		
		public function map_shortcode() {
			$this->set_shortcode_path( BORGHOLM_CORE_CPT_URL_PATH . '/portfolio/shortcodes/portfolio-tags' );
			$this->set_base( 'borgholm_core_portfolio_tags' );
			$this->set_name( esc_html__( 'Portfolio Tags', 'borgholm-core' ) );
			$this->set_description( esc_html__( 'Shortcode that displays portfolio tags', 'borgholm-core' ) );
			$this->set_category( esc_html__( 'Borgholm Core', 'borgholm-core' ) );

			$this->set_option( array(
				'field_type' => 'text',
				'name'       => 'custom_class',
				'title'      => esc_html__( 'Custom Class', 'borgholm-core' )
			) );
			$this->set_option( array(
				'field_type' => 'select',
				'name'       => 'skin',
				'title'      => esc_html__( 'Skin', 'borgholm-core' ),
				'options'    => array(
					''      => esc_html__( 'Default', 'borgholm-core' ),
					'light' => esc_html__( 'Light', 'borgholm-core' )
				),
			) );
			$this->set_option( array(
				'field_type'  => 'text',
				'name'        => 'portfolio_id',
				'title'       => esc_html__( 'Portfolio ID', 'borgholm-core' ),
			) );
		}
		
		public static function call_shortcode( $params ) {
			$html = qode_framework_call_shortcode( 'borgholm_core_portfolio_tags', $params );
			$html = str_replace( "\n", '', $html );
			
			return $html;
		}

		public function render( $options, $content = null ) {
			parent::render( $options );
			
			$atts = $this->get_atts();

			$atts['portfolio_id'] = intval( $atts['portfolio_id'] );
			if ( $atts['portfolio_id'] <= 0 ) {
				$atts['portfolio_id'] = get_the_ID();
			}

			$atts['holder_classes'] = $this->get_holder_classes( $atts );

			return borgholm_core_get_template_part( 'post-types/portfolio/shortcodes/portfolio-tags', 'templates/portfolio-tags', '', $atts );
		}
		
		private function get_holder_classes( $atts ) {
			$holder_classes = $this->init_holder_classes();
			
			$holder_classes[] = 'qodef-portfolio-tags';
			$holder_classes[] = isset( $atts['skin'] ) && ! empty( $atts['skin'] ) ? 'qodef-skin--' . $atts['skin'] : '';

			return implode( ' ', $holder_classes );
		}
	}
}