<?php

if ( ! function_exists( 'borgholm_core_add_portfolio_single_variation_custom_full_width' ) ) {
	/**
	 * Function that add variation layout for this module
	 *
	 * @param array $variations
	 *
	 * @return array
	 */
	function borgholm_core_add_portfolio_single_variation_custom_full_width( $variations ) {
		$variations['custom-full-width'] = esc_html__( 'Custom - Full Width', 'borgholm-core' );
		
		return $variations;
	}
	
	add_filter( 'borgholm_core_filter_portfolio_single_layout_options', 'borgholm_core_add_portfolio_single_variation_custom_full_width' );
}

if ( ! function_exists( 'borgholm_core_set_portfolio_single_variation_custom_full_width_holder_width' ) ) {
	function borgholm_core_set_portfolio_single_variation_custom_full_width_holder_width( $classes ) {

		if ( borgholm_core_get_post_value_through_levels( 'qodef_portfolio_single_layout' ) == 'custom-full-width' ) {
			$classes = 'qodef-content-full-width';
		}

		return $classes;
	}

	add_filter( 'borgholm_filter_page_inner_classes', 'borgholm_core_set_portfolio_single_variation_custom_full_width_holder_width' );
}