<?php

if ( ! function_exists( 'borgholm_core_add_portfolio_single_variation_slider' ) ) {
	/**
	 * Function that add variation layout for this module
	 *
	 * @param array $variations
	 *
	 * @return array
	 */
	function borgholm_core_add_portfolio_single_variation_slider( $variations ) {
		$variations['slider'] = esc_html__( 'Slider', 'borgholm-core' );
		
		return $variations;
	}
	
	add_filter( 'borgholm_core_filter_portfolio_single_layout_options', 'borgholm_core_add_portfolio_single_variation_slider' );
}

if ( ! function_exists( 'borgholm_core_add_portfolio_single_slider' ) ) {
	/**
	 * Function that include slider module before page content
	 */
	function borgholm_core_add_portfolio_single_slider() {
		if ( borgholm_core_get_post_value_through_levels( 'qodef_portfolio_single_layout' ) == 'slider' ) {
			borgholm_core_template_part( 'post-types/portfolio', 'variations/slider/layout/parts/slider' );
		}
	}
	
	add_action( 'borgholm_action_before_page_inner', 'borgholm_core_add_portfolio_single_slider' );
}