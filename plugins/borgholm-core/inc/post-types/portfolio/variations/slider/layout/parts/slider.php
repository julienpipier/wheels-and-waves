<div class="qodef-media qodef-portfolio-single-slider qodef-swiper-container" data-options='{"slidesPerView":"2", "centeredSlides":"true", "slidesPerView1024":"1", "customStages":"true"}'>
    <div class="swiper-wrapper">
		<?php borgholm_core_template_part( 'post-types/portfolio', 'templates/parts/post-info/media', 'slider'); ?>
    </div>
    <div class="swiper-pagination"></div>
</div>