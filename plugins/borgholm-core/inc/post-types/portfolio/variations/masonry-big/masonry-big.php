<?php

if ( ! function_exists( 'borgholm_core_add_portfolio_single_variation_masonry_big' ) ) {
	/**
	 * Function that add variation layout for this module
	 *
	 * @param array $variations
	 *
	 * @return array
	 */
	function borgholm_core_add_portfolio_single_variation_masonry_big( $variations ) {
		$variations['masonry-big'] = esc_html__( 'Masonry - Big', 'borgholm-core' );
		
		return $variations;
	}
	
	add_filter( 'borgholm_core_filter_portfolio_single_layout_options', 'borgholm_core_add_portfolio_single_variation_masonry_big' );
}

if ( ! function_exists( 'borgholm_core_include_masonry_for_portfolio_single_variation_masonry_big' ) ) {
	/**
	 * Function that include masonry scripts for current module layout
	 *
	 * @param string $post_type
	 *
	 * @return string
	 */
	function borgholm_core_include_masonry_for_portfolio_single_variation_masonry_big( $post_type ) {
		$portfolio_template = borgholm_core_get_post_value_through_levels( 'qodef_portfolio_single_layout' );
		
		if ( $portfolio_template === 'masonry-big' ) {
			$post_type = 'portfolio-item';
		}
		
		return $post_type;
	}
	
	add_filter( 'borgholm_filter_allowed_post_type_to_enqueue_masonry_scripts', 'borgholm_core_include_masonry_for_portfolio_single_variation_masonry_big' );
}