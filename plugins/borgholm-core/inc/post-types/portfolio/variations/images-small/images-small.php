<?php

if ( ! function_exists( 'borgholm_core_add_portfolio_single_variation_images_small' ) ) {
	/**
	 * Function that add variation layout for this module
	 *
	 * @param array $variations
	 *
	 * @return array
	 */
	function borgholm_core_add_portfolio_single_variation_images_small( $variations ) {
		$variations['images-small'] = esc_html__( 'Images - Small', 'borgholm-core' );
		
		return $variations;
	}
	
	add_filter( 'borgholm_core_filter_portfolio_single_layout_options', 'borgholm_core_add_portfolio_single_variation_images_small' );
}