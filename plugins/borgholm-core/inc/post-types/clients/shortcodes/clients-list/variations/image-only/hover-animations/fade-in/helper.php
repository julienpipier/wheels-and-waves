<?php

if ( ! function_exists( 'borgholm_core_filter_clients_list_image_only_fade_in' ) ) {
	/**
	 * Function that add variation layout for this module
	 *
	 * @param array $variations
	 *
	 * @return array
	 */
	function borgholm_core_filter_clients_list_image_only_fade_in( $variations ) {
		$variations['fade-in'] = esc_html__( 'Fade In', 'borgholm-core' );
		
		return $variations;
	}
	
	add_filter( 'borgholm_core_filter_clients_list_image_only_animation_options', 'borgholm_core_filter_clients_list_image_only_fade_in' );
}