<?php

if ( ! function_exists( 'borgholm_core_add_clients_list_variation_image_only' ) ) {
	/**
	 * Function that add variation layout for this module
	 *
	 * @param array $variations
	 *
	 * @return array
	 */
	function borgholm_core_add_clients_list_variation_image_only( $variations ) {
		$variations['image-only'] = esc_html__( 'Image Only', 'borgholm-core' );
		
		return $variations;
	}
	
	add_filter( 'borgholm_core_filter_clients_list_layouts', 'borgholm_core_add_clients_list_variation_image_only' );
}