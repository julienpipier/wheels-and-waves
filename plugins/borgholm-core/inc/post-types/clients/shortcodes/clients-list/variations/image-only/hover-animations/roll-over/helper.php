<?php

if ( ! function_exists( 'borgholm_core_filter_clients_list_image_only_roll_over' ) ) {
    /**
     * Function that add variation layout for this module
     *
     * @param array $variations
     *
     * @return array
     */
    function borgholm_core_filter_clients_list_image_only_roll_over( $variations ) {
        $variations['roll-over'] = esc_html__( 'Roll Over', 'borgholm-core' );

        return $variations;
    }

    add_filter( 'borgholm_core_filter_clients_list_image_only_animation_options', 'borgholm_core_filter_clients_list_image_only_roll_over' );
}