<article <?php post_class( 'qodef-team-list-item qodef-e' ); ?>>
	<div class="qodef-e-inner">
		<?php borgholm_core_template_part( 'post-types/team', 'templates/parts/post-info/image' ); ?>
		<div class="qodef-e-content">
			<?php borgholm_core_template_part( 'post-types/team', 'templates/parts/post-info/title' ); ?>
			<?php borgholm_core_template_part( 'post-types/team', 'templates/parts/post-info/role' ); ?>
			<?php borgholm_core_template_part( 'post-types/team', 'templates/parts/post-info/social-icons' ); ?>
			<?php borgholm_core_template_part( 'post-types/team', 'templates/parts/post-info/address' ); ?>
			<?php borgholm_core_template_part( 'post-types/team', 'templates/parts/post-info/birth-date' ); ?>
			<?php borgholm_core_template_part( 'post-types/team', 'templates/parts/post-info/education' ); ?>
			<?php borgholm_core_template_part( 'post-types/team', 'templates/parts/post-info/email' ); ?>
			<?php borgholm_core_template_part( 'post-types/team', 'templates/parts/post-info/resume' ); ?>
			<?php borgholm_core_template_part( 'post-types/team', 'templates/parts/post-info/excerpt' ); ?>
		</div>
	</div>
</article>