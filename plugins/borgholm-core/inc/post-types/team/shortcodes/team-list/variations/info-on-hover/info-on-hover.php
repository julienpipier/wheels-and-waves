<?php

if ( ! function_exists( 'borgholm_core_add_team_list_variation_info_on_hover' ) ) {
	/**
	 * Function that add variation layout for this module
	 *
	 * @param array $variations
	 *
	 * @return array
	 */
	function borgholm_core_add_team_list_variation_info_on_hover( $variations ) {
		$variations['info-on-hover'] = esc_html__( 'Info on Hover', 'borgholm-core' );
		
		return $variations;
	}
	
	add_filter( 'borgholm_core_filter_team_list_layouts', 'borgholm_core_add_team_list_variation_info_on_hover' );
}