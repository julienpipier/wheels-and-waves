<?php

if ( ! function_exists( 'borgholm_core_add_testimonials_list_variation_info_on_side' ) ) {
	/**
	 * Function that add variation layout for this module
	 *
	 * @param array $variations
	 *
	 * @return array
	 */
	function borgholm_core_add_testimonials_list_variation_info_on_side( $variations ) {
		$variations['info-on-side'] = esc_html__( 'Info On Side', 'borgholm-core' );
		
		return $variations;
	}
	
	add_filter( 'borgholm_core_filter_testimonials_list_layouts', 'borgholm_core_add_testimonials_list_variation_info_on_side' );
}