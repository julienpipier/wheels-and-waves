<?php

if ( ! function_exists( 'borgholm_core_add_general_options' ) ) {
	/**
	 * Function that add general options for this module
	 */
	function borgholm_core_add_general_options( $page ) {

		if ( $page ) {
			$page->add_field_element(
				array(
					'field_type'  => 'color',
					'name'        => 'qodef_main_color',
					'title'       => esc_html__( 'Main Color', 'borgholm-core' ),
					'description' => esc_html__( 'Choose the most dominant theme color', 'borgholm-core' )
				)
			);

			$page->add_field_element(
				array(
					'field_type'  => 'color',
					'name'        => 'qodef_page_background_color',
					'title'       => esc_html__( 'Page Background Color', 'borgholm-core' ),
					'description' => esc_html__( 'Set background color', 'borgholm-core' )
				)
			);

			$page->add_field_element(
				array(
					'field_type'  => 'image',
					'name'        => 'qodef_page_background_image',
					'title'       => esc_html__( 'Page Background Image', 'borgholm-core' ),
					'description' => esc_html__( 'Set background image', 'borgholm-core' )
				)
			);

			$page->add_field_element(
				array(
					'field_type'  => 'select',
					'name'        => 'qodef_page_background_repeat',
					'title'       => esc_html__( 'Page Background Image Repeat', 'borgholm-core' ),
					'description' => esc_html__( 'Set background image repeat', 'borgholm-core' ),
					'options'     => array(
						''          => esc_html__( 'Default', 'borgholm-core' ),
						'no-repeat' => esc_html__( 'No Repeat', 'borgholm-core' ),
						'repeat'    => esc_html__( 'Repeat', 'borgholm-core' ),
						'repeat-x'  => esc_html__( 'Repeat-x', 'borgholm-core' ),
						'repeat-y'  => esc_html__( 'Repeat-y', 'borgholm-core' )
					)
				)
			);

			$page->add_field_element(
				array(
					'field_type'  => 'select',
					'name'        => 'qodef_page_background_size',
					'title'       => esc_html__( 'Page Background Image Size', 'borgholm-core' ),
					'description' => esc_html__( 'Set background image size', 'borgholm-core' ),
					'options'     => array(
						''        => esc_html__( 'Default', 'borgholm-core' ),
						'contain' => esc_html__( 'Contain', 'borgholm-core' ),
						'cover'   => esc_html__( 'Cover', 'borgholm-core' )
					)
				)
			);

			$page->add_field_element(
				array(
					'field_type'  => 'select',
					'name'        => 'qodef_page_background_attachment',
					'title'       => esc_html__( 'Page Background Image Attachment', 'borgholm-core' ),
					'description' => esc_html__( 'Set background image attachment', 'borgholm-core' ),
					'options'     => array(
						''       => esc_html__( 'Default', 'borgholm-core' ),
						'fixed'  => esc_html__( 'Fixed', 'borgholm-core' ),
						'scroll' => esc_html__( 'Scroll', 'borgholm-core' )
					)
				)
			);

			$page->add_field_element(
				array(
					'field_type'  => 'text',
					'name'        => 'qodef_page_content_padding',
					'title'       => esc_html__( 'Page Content Padding', 'borgholm-core' ),
					'description' => esc_html__( 'Set padding that will be applied for page content in format: top right bottom left (e.g. 10px 5px 10px 5px)', 'borgholm-core' )
				)
			);

			$page->add_field_element(
				array(
					'field_type'  => 'text',
					'name'        => 'qodef_page_content_padding_mobile',
					'title'       => esc_html__( 'Page Content Padding Mobile', 'borgholm-core' ),
					'description' => esc_html__( 'Set padding that will be applied for page content on mobile screens (1024px and below) in format: top right bottom left (e.g. 10px 5px 10px 5px)', 'borgholm-core' )
				)
			);

			$page->add_field_element(
				array(
					'field_type'    => 'yesno',
					'name'          => 'qodef_boxed',
					'title'         => esc_html__( 'Boxed Layout', 'borgholm-core' ),
					'description'   => esc_html__( 'Set boxed layout', 'borgholm-core' ),
					'default_value' => 'no'
				)
			);

			$boxed_section = $page->add_section_element(
				array(
					'name'       => 'qodef_boxed_section',
					'title'      => esc_html__( 'Boxed Layout Section', 'borgholm-core' ),
					'dependency' => array(
						'hide' => array(
							'qodef_boxed' => array(
								'values'        => 'no',
								'default_value' => ''
							)
						)
					)
				)
			);

			$boxed_section->add_field_element(
				array(
					'field_type'  => 'color',
					'name'        => 'qodef_boxed_background_color',
					'title'       => esc_html__( 'Boxed Background Color', 'borgholm-core' ),
					'description' => esc_html__( 'Set boxed background color', 'borgholm-core' )
				)
			);

            $boxed_section->add_field_element(
                array(
                    'field_type'  => 'image',
                    'name'        => 'qodef_boxed_background_pattern',
                    'title'       => esc_html__( 'Boxed Background Pattern', 'borgholm-core' ),
                    'description' => esc_html__( 'Set boxed background pattern', 'borgholm-core' )
                )
            );

            $boxed_section->add_field_element(
                array(
                    'field_type'  => 'select',
                    'name'        => 'qodef_boxed_background_pattern_behavior',
                    'title'       => esc_html__( 'Boxed Background Pattern Behavior', 'borgholm-core' ),
                    'description' => esc_html__( 'Set boxed background pattern behavior', 'borgholm-core' ),
                    'options'     => array(
                        'fixed'  => esc_html__( 'Fixed', 'borgholm-core' ),
                        'scroll' => esc_html__( 'Scroll', 'borgholm-core' )
                    ),
                )
            );

			$page->add_field_element(
				array(
					'field_type'    => 'yesno',
					'name'          => 'qodef_passepartout',
					'title'         => esc_html__( 'Passepartout', 'borgholm-core' ),
					'description'   => esc_html__( 'Enabling this option will display a passepartout around website content', 'borgholm-core' ),
					'default_value' => 'no'
				)
			);

			$passepartout_section = $page->add_section_element(
				array(
					'name'       => 'qodef_passepartout_section',
					'title'      => esc_html__( 'Passepartout Section', 'borgholm-core' ),
					'dependency' => array(
						'hide' => array(
							'qodef_passepartout' => array(
								'values'        => 'no',
								'default_value' => ''
							)
						)
					)
				)
			);

			$passepartout_section->add_field_element(
				array(
					'field_type'  => 'color',
					'name'        => 'qodef_passepartout_color',
					'title'       => esc_html__( 'Passepartout Color', 'borgholm-core' ),
					'description' => esc_html__( 'Choose background color for passepartout', 'borgholm-core' )
				)
			);

			$passepartout_section->add_field_element(
				array(
					'field_type'  => 'image',
					'name'        => 'qodef_passepartout_image',
					'title'       => esc_html__( 'Passepartout Background Image', 'borgholm-core' ),
					'description' => esc_html__( 'Set background image for passepartout', 'borgholm-core' )
				)
			);

			$passepartout_section->add_field_element(
				array(
					'field_type'  => 'text',
					'name'        => 'qodef_passepartout_size',
					'title'       => esc_html__( 'Passepartout Size', 'borgholm-core' ),
					'description' => esc_html__( 'Enter size amount for passepartout', 'borgholm-core' ),
					'args'        => array(
						'suffix' => esc_html__( 'px or %', 'borgholm-core' )
					)
				)
			);

			$passepartout_section->add_field_element(
				array(
					'field_type'  => 'text',
					'name'        => 'qodef_passepartout_size_responsive',
					'title'       => esc_html__( 'Passepartout Responsive Size', 'borgholm-core' ),
					'description' => esc_html__( 'Enter size amount for passepartout for smaller screens (1024px and below)', 'borgholm-core' ),
					'args'        => array(
						'suffix' => esc_html__( 'px or %', 'borgholm-core' )
					)
				)
			);

			$page->add_field_element(
				array(
					'field_type'    => 'select',
					'name'          => 'qodef_content_width',
					'title'         => esc_html__( 'Initial Width of Content', 'borgholm-core' ),
					'description'   => esc_html__( 'Choose the initial width of content which is in grid (applies to pages set to "Default Template" and rows set to "In Grid")', 'borgholm-core' ),
					'options'       => borgholm_core_get_select_type_options_pool( 'content_width', false ),
					'default_value' => '1100'
				)
			);
			
			$page->add_field_element(
				array(
					'field_type'    => 'yesno',
					'name'          => 'qodef_content_lines',
					'title'         => esc_html__( 'Display lines in content background', 'borgholm-core' ),
					'description'   => esc_html__( 'Enabling this option will display vertical lines in website content background', 'borgholm-core' ),
					'default_value' => 'no'
				)
			);
			
			$page->add_field_element(
				array(
					'field_type'    => 'yesno',
					'name'          => 'qodef_content_center_line',
					'title'         => esc_html__( 'Display additional center line', 'borgholm-core' ),
					'description'   => esc_html__( 'Enabling this option will display additional vertical line in the center of website content background', 'borgholm-core' ),
					'default_value' => 'no',
					'dependency' => array(
						'show' => array(
							'qodef_content_lines' => array(
								'values'        => 'yes',
								'default_value' => 'no'
							)
						)
					)
				)
			);
			
			$page->add_field_element(
				array(
					'field_type'    => 'yesno',
					'name'          => 'qodef_content_lines_in_grid',
					'title'         => esc_html__( 'Display lines in grid layout', 'borgholm-core' ),
					'description'   => esc_html__( 'Enabling this option will place content background lines in grid', 'borgholm-core' ),
					'default_value' => 'no',
					'dependency' => array(
						'show' => array(
							'qodef_content_lines' => array(
								'values'        => 'yes',
								'default_value' => 'no'
							)
						)
					)
				)
			);
			
			$page->add_field_element(
				array(
					'field_type'  => 'select',
					'name'        => 'qodef_content_lines_in_grid_type',
					'title'       => esc_html__( 'Display lines type', 'borgholm-core' ),
					'description' => esc_html__( 'Selecting this option will change content background lines type in grid layout', 'borgholm-core' ),
					'options'     => array(
						''     => esc_html__( 'Default', 'borgholm-core' ),
						'wide' => esc_html__( 'Wide', 'borgholm-core' ),
						'inside' => esc_html__( 'Inside', 'borgholm-core' )
					),
					'dependency'  => array(
						'show' => array(
							'qodef_content_lines_in_grid' => array(
								'values'        => 'yes',
								'default_value' => 'no'
							)
						)
					)
				)
			);
			
			$page->add_field_element(
				array(
					'field_type'  => 'select',
					'name'        => 'qodef_content_lines_skin',
					'title'       => esc_html__( 'Lines Skin', 'borgholm-core' ),
					'description' => esc_html__( 'Choose a predefined style for line elements', 'borgholm-core' ),
					'options'     => array(
						'dark'  => esc_html__( 'Dark', 'borgholm-core' ),
						'light' => esc_html__( 'Light', 'borgholm-core' )
					),
					'dependency' => array(
						'show' => array(
							'qodef_content_lines' => array(
								'values'        => 'yes',
								'default_value' => 'no'
							)
						)
					)
				)
			);
			 
			// Hook to include additional options after module options
			do_action( 'borgholm_core_action_after_general_options_map', $page );
			
			$page->add_field_element(
				array(
					'field_type'  => 'textarea',
					'name'        => 'qodef_custom_js',
					'title'       => esc_html__( 'Custom JS', 'borgholm-core' ),
					'description' => esc_html__( 'Enter your custom JavaScript here', 'borgholm-core' )
				)
			);
		}
	}

	add_action( 'borgholm_core_action_default_options_init', 'borgholm_core_add_general_options', borgholm_core_get_admin_options_map_position( 'general' ) );
}