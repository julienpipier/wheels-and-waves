<div id="qodef-fullscreen-area">
	<?php if ( $fullscreen_menu_in_grid ) { ?>
		<div class="qodef-content-grid">
	<?php } ?>
		
		<div id="qodef-fullscreen-area-inner">
			<div class="qodef-fullscreen-menu-holder">
				<?php if ( has_nav_menu( 'fullscreen-menu-navigation' ) ) { ?>
					<nav class="qodef-fullscreen-menu">
						<?php wp_nav_menu(
							array(
								'theme_location' => 'fullscreen-menu-navigation',
								'container'         => '',
								'link_before'       => '<span class="qodef-menu-item-text">',
								'link_after'        => '</span>',
								'walker'         => new BorgholmCoreRootMainMenuWalker()
							)
						); ?>
					</nav>
				<?php } ?>
				<?php borgholm_core_get_header_widget_area( '', 'two' ); ?>
			</div>
		</div>
		
	<?php if ( $fullscreen_menu_in_grid ) { ?>
		</div>
	<?php } ?>
</div>