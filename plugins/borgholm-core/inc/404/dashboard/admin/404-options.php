<?php

if ( ! function_exists( 'borgholm_core_add_404_page_options' ) ) {
	/**
	 * Function that add general options for this module
	 */
	function borgholm_core_add_404_page_options() {
		$qode_framework = qode_framework_get_framework_root();

		$page = $qode_framework->add_options_page(
			array(
				'scope'       => BORGHOLM_CORE_OPTIONS_NAME,
				'type'        => 'admin',
				'slug'        => '404',
				'icon'        => 'fa fa-book',
				'title'       => esc_html__( '404', 'borgholm-core' ),
				'description' => esc_html__( 'Global 404 Page Options', 'borgholm-core' )
			)
		);
		
		if ( $page ) {
			$page->add_field_element(
				array(
					'field_type'    => 'select',
					'name'          => 'qodef_404_header_layout',
					'title'         => esc_html__( 'Header Layout', 'borgholm-core' ),
					'description'   => esc_html__( 'Choose a header layout to set on 404 page', 'borgholm-core' ),
					'args'          => array( 'images' => true ),
					'options'       => borgholm_core_header_radio_to_select_options( apply_filters( 'borgholm_core_filter_header_layout_option', $header_layout_options = array() ) ),
				)
			);

			$page->add_field_element(
				array(
					'field_type'    => 'yesno',
					'name'          => 'qodef_show_404_header_widget_areas',
					'title'         => esc_html__( 'Show Header Widget Areas', 'borgholm-core' ),
					'description'   => esc_html__( 'Choose if you want to show or hide header widget areas on 404 page', 'borgholm-core' ),
					'default_value' => 'yes'
				)
			);

			$page->add_field_element(
				array(
					'field_type'    => 'yesno',
					'name'          => 'qodef_enable_404_page_title',
					'title'         => esc_html__( 'Enable Page Title', 'borgholm-core' ),
					'description'   => esc_html__( 'Use this option to enable/disable page title on 404 page', 'borgholm-core' ),
					'default_value' => 'no'
				)
			);
			
			$page->add_field_element(
				array(
					'field_type'    => 'yesno',
					'name'          => 'qodef_enable_404_page_footer',
					'title'         => esc_html__( 'Enable Page Footer', 'borgholm-core' ),
					'description'   => esc_html__( 'Use this option to enable/disable page footer on 404 page', 'borgholm-core' ),
					'default_value' => 'no'
				)
			);
			
			$page->add_field_element(
				array(
					'field_type'  => 'color',
					'name'        => 'qodef_404_page_background_color',
					'title'       => esc_html__( 'Background Color', 'borgholm-core' ),
					'description' => esc_html__( 'Enter 404 page area background color', 'borgholm-core' )
				)
			);
			
			$page->add_field_element(
				array(
					'field_type'  => 'image',
					'name'        => 'qodef_404_page_background_image',
					'title'       => esc_html__( 'Background Image', 'borgholm-core' ),
					'description' => esc_html__( 'Enter 404 page area background image', 'borgholm-core' )
				)
			);

			$page->add_field_element(
				array(
					'field_type' => 'text',
					'name'       => 'qodef_404_page_mark',
					'title'      => esc_html__( 'Mark Label', 'marra-core' )
				)
			);

			$page->add_field_element(
				array(
					'field_type' => 'color',
					'name'       => 'qodef_404_page_mark_color',
					'title'      => esc_html__( 'Mark Color', 'marra-core' )
				)
			);

			$page->add_field_element(
				array(
					'field_type' => 'text',
					'name'       => 'qodef_404_page_title',
					'title'      => esc_html__( 'Title Label', 'borgholm-core' )
				)
			);
			
			$page->add_field_element(
				array(
					'field_type' => 'color',
					'name'       => 'qodef_404_page_title_color',
					'title'      => esc_html__( 'Title Color', 'borgholm-core' )
				)
			);
			
			$page->add_field_element(
				array(
					'field_type' => 'text',
					'name'       => 'qodef_404_page_text',
					'title'      => esc_html__( 'Text Label', 'borgholm-core' )
				)
			);
			
			$page->add_field_element(
				array(
					'field_type' => 'color',
					'name'       => 'qodef_404_page_text_color',
					'title'      => esc_html__( 'Text Color', 'borgholm-core' )
				)
			);
			
			$page->add_field_element(
				array(
					'field_type' => 'text',
					'name'       => 'qodef_404_page_button_text',
					'title'      => esc_html__( 'Button Text', 'borgholm-core' )
				)
			);
			
			// Hook to include additional options after module options
			do_action( 'borgholm_core_action_after_404_page_options_map', $page );
		}
	}
	
	add_action( 'borgholm_core_action_default_options_init', 'borgholm_core_add_404_page_options', borgholm_core_get_admin_options_map_position( '404' ) );
}