<?php

if ( ! function_exists( 'borgholm_core_add_page_header_meta_box' ) ) {
	/**
	 * Function that add general meta box options for this module
	 *
	 * @param object $page
	 */
	function borgholm_core_add_page_header_meta_box( $page ) {
		
		if ( $page ) {
			
			$header_tab = $page->add_tab_element(
				array(
					'name'        => 'tab-header',
					'icon'        => 'fa fa-cog',
					'title'       => esc_html__( 'Header Settings', 'borgholm-core' ),
					'description' => esc_html__( 'Header layout settings', 'borgholm-core' )
				)
			);
			
			$header_tab->add_field_element(
				array(
					'field_type'  => 'select',
					'name'        => 'qodef_header_layout',
					'title'       => esc_html__( 'Header Layout', 'borgholm-core' ),
					'description' => esc_html__( 'Choose a header layout to set for your website', 'borgholm-core' ),
					'args'        => array( 'images' => true ),
					'options'     => borgholm_core_header_radio_to_select_options( apply_filters( 'borgholm_core_filter_header_layout_option', $header_layout_options = array() ) )
				)
			);
			
			$header_tab->add_field_element(
				array(
					'field_type'  => 'select',
					'name'        => 'qodef_header_skin',
					'title'       => esc_html__( 'Header Skin', 'borgholm-core' ),
					'description' => esc_html__( 'Choose a predefined header style for header elements', 'borgholm-core' ),
					'options'     => array(
						''      => esc_html__( 'Default', 'borgholm-core' ),
						'none'  => esc_html__( 'None', 'borgholm-core' ),
						'light' => esc_html__( 'Light', 'borgholm-core' ),
						'dark'  => esc_html__( 'Dark', 'borgholm-core' )
					)
				)
			);
			
			$header_tab->add_field_element(
				array(
					'field_type'  => 'select',
					'name'        => 'qodef_enable_custom_side_area_opener',
					'title'       => esc_html__( 'Enable Custom Side Areas Opener', 'borgholm-core' ),
					'description' => esc_html__( 'Enabling this option will show custom styled side area opener', 'borgholm-core' ),
					'options'     => array(
						''    => esc_html__( 'Default', 'borgholm-core' ),
						'yes' => esc_html__( 'Yes', 'borgholm-core' ),
						'no'  => esc_html__( 'No', 'borgholm-core' )
					),
					'dependency' => array(
						'show' => array(
							'qodef_header_layout' => array(
								'values'        => 'standard',
								'default_value' => ''
							)
						)
					)
				)
			);
			
			$header_tab->add_field_element(
				array(
					'field_type'  => 'select',
					'name'        => 'qodef_custom_side_area_opener_skin',
					'title'       => esc_html__( 'Custom Side Areas Opener Skin', 'borgholm-core' ),
					'description' => esc_html__( 'Choose skin for custom styled side area opener', 'borgholm-core' ),
					'options'     => array(
						''      => esc_html__( 'Default', 'borgholm-core' ),
						'dark'  => esc_html__( 'Dark', 'borgholm-core' ),
						'light' => esc_html__( 'Light', 'borgholm-core' )
					),
					'dependency' => array(
						'show' => array(
							'qodef_header_layout' => array(
								'values'        => 'standard',
								'default_value' => ''
							)
						)
					)
				)
			);
			
			$header_tab->add_field_element(
				array(
					'field_type'    => 'yesno',
					'name'          => 'qodef_show_header_widget_areas',
					'title'         => esc_html__( 'Show Header Widget Areas', 'borgholm-core' ),
					'description'   => esc_html__( 'Choose if you want to show or hide header widget areas', 'borgholm-core' ),
					'default_value' => 'yes'
				)
			);
			
			$custom_sidebars = borgholm_core_get_custom_sidebars();
			if ( ! empty( $custom_sidebars ) && count( $custom_sidebars ) > 1 ) {
				
				$section = $header_tab->add_section_element(
					array(
						'name'       => 'qodef_header_custom_widget_area_section',
						'dependency' => array(
							'show' => array(
								'qodef_show_header_widget_areas' => array(
									'values'        => 'yes',
									'default_value' => 'yes'
								)
							)
						)
					)
				);
				
				$section->add_field_element(
					array(
						'field_type'  => 'select',
						'name'        => 'qodef_header_custom_widget_area_one',
						'title'       => esc_html__( 'Choose Custom Header Widget Area One', 'borgholm-core' ),
						'description' => esc_html__( 'Choose custom widget area to display in header widget area one', 'borgholm-core' ),
						'options'     => $custom_sidebars
					)
				);
				
				$section->add_field_element(
					array(
						'field_type'  => 'select',
						'name'        => 'qodef_header_custom_widget_area_two',
						'title'       => esc_html__( 'Choose Custom Header Widget Area Two', 'borgholm-core' ),
						'description' => esc_html__( 'Choose custom widget area to display in header widget area two', 'borgholm-core' ),
						'options'     => $custom_sidebars
					)
				);
				
				// Hook to include additional options after module options
				do_action( 'borgholm_core_action_after_custom_widget_area_header_meta_map', $section, $custom_sidebars );
			}
			
			// Hook to include additional options after module options
			do_action( 'borgholm_core_action_after_page_header_meta_map', $header_tab, $custom_sidebars );
		}
	}
	
	add_action( 'borgholm_core_action_after_general_meta_box_map', 'borgholm_core_add_page_header_meta_box' );
}

if ( ! function_exists( 'borgholm_core_add_general_header_meta_box_callback' ) ) {
	/**
	 * Function that set current meta box callback as general callback functions
	 *
	 * @param array $callbacks
	 *
	 * @return array
	 */
	function borgholm_core_add_general_header_meta_box_callback( $callbacks ) {
		$callbacks['header'] = 'borgholm_core_add_page_header_meta_box';
		
		return $callbacks;
	}
	
	add_filter( 'borgholm_core_filter_general_meta_box_callbacks', 'borgholm_core_add_general_header_meta_box_callback' );
}