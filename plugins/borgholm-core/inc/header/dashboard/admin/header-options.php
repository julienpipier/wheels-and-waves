<?php

if ( ! function_exists( 'borgholm_core_add_header_options' ) ) {
	/**
	 * Function that add header options for this module
	 */
	function borgholm_core_add_header_options() {
		$qode_framework = qode_framework_get_framework_root();

		$page = $qode_framework->add_options_page(
			array(
				'scope'       => BORGHOLM_CORE_OPTIONS_NAME,
				'type'        => 'admin',
				'layout'      => 'tabbed',
				'slug'        => 'header',
				'icon'        => 'fa fa-cog',
				'title'       => esc_html__( 'Header', 'borgholm-core' ),
				'description' => esc_html__( 'Global Header Options', 'borgholm-core' )
			)
		);

		if ( $page ) {
			$general_tab = $page->add_tab_element(
				array(
					'name'  => 'tab-header-general',
					'icon'  => 'fa fa-cog',
					'title' => esc_html__( 'General Settings', 'borgholm-core' )
				)
			);
			
			$general_tab->add_field_element(
				array(
					'field_type'    => 'radio',
					'name'          => 'qodef_header_layout',
					'title'         => esc_html__( 'Header Layout', 'borgholm-core' ),
					'description'   => esc_html__( 'Choose a header layout to set for your website', 'borgholm-core' ),
					'args'          => array( 'images' => true ),
					'options'       => apply_filters( 'borgholm_core_filter_header_layout_option', $header_layout_options = array() ),
					'default_value' => apply_filters( 'borgholm_core_filter_header_layout_default_option_value', '' ),
				)
			);

			$general_tab->add_field_element(
				array(
					'field_type'  => 'select',
					'name'        => 'qodef_header_skin',
					'title'       => esc_html__( 'Header Skin', 'borgholm-core' ),
					'description' => esc_html__( 'Choose a predefined header style for header elements', 'borgholm-core' ),
					'options'     => array(
						'none'  => esc_html__( 'None', 'borgholm-core' ),
						'light' => esc_html__( 'Light', 'borgholm-core' ),
						'dark'  => esc_html__( 'Dark', 'borgholm-core' )
					)
				)
			);

			$general_tab->add_field_element(
				array(
					'field_type'    => 'yesno',
					'name'          => 'qodef_enable_custom_side_area_opener',
					'title'         => esc_html__( 'Enable Custom Side Areas Opener', 'borgholm-core' ),
					'description'   => esc_html__( 'Enabling this option will show custom styled side area opener', 'borgholm-core' ),
					'default_value' => 'no',
					'dependency'    => array(
						'show' => array(
							'qodef_header_layout' => array(
								'values'        => 'standard',
								'default_value' => ''
							)
						)
					)
				)
			);

			$general_tab->add_field_element(
				array(
					'field_type'  => 'select',
					'name'        => 'qodef_custom_side_area_opener_skin',
					'title'       => esc_html__( 'Custom Side Areas Opener Skin', 'borgholm-core' ),
					'description' => esc_html__( 'Choose skin for custom styled side area opener', 'borgholm-core' ),
					'options'     => array(
						'dark'  => esc_html__( 'Dark', 'borgholm-core' ),
						'light' => esc_html__( 'Light', 'borgholm-core' )
					),
					'dependency' => array(
						'show' => array(
							'qodef_header_layout' => array(
								'values'        => 'standard',
								'default_value' => ''
							)
						)
					)
				)
			);

			// Hook to include additional options after module options
			do_action( 'borgholm_core_action_after_header_options_map', $page, $general_tab );
		}
	}

	add_action( 'borgholm_core_action_default_options_init', 'borgholm_core_add_header_options', borgholm_core_get_admin_options_map_position( 'header' ) );
}