<?php

if ( ! function_exists( 'borgholm_core_add_logo_options' ) ) {
	/**
	 * Function that add general options for this module
	 */
	function borgholm_core_add_logo_options() {
		$qode_framework = qode_framework_get_framework_root();

		$page = $qode_framework->add_options_page(
			array(
				'scope'       => BORGHOLM_CORE_OPTIONS_NAME,
				'type'        => 'admin',
				'slug'        => 'logo',
				'icon'        => 'fa fa-cog',
				'title'       => esc_html__( 'Logo', 'borgholm-core' ),
				'description' => esc_html__( 'Global Logo Options', 'borgholm-core' ),
				'layout'      => 'tabbed'
			)
		);

		if ( $page ) {

			$header_tab = $page->add_tab_element(
				array(
					'name'        => 'tab-header',
					'icon'        => 'fa fa-cog',
					'title'       => esc_html__( 'Header Logo Options', 'borgholm-core' ),
					'description' => esc_html__( 'Set options for initial headers', 'borgholm-core' )
				)
			);

			$header_tab->add_field_element(
				array(
					'field_type'  => 'text',
					'name'        => 'qodef_logo_height',
					'title'       => esc_html__( 'Logo Height', 'borgholm-core' ),
					'description' => esc_html__( 'Enter logo height', 'borgholm-core' ),
					'args'        => array(
						'suffix' => esc_html__( 'px', 'borgholm-core' )
					)
				)
			);

			$header_tab->add_field_element(
				array(
					'field_type'    => 'image',
					'name'          => 'qodef_logo_main',
					'title'         => esc_html__( 'Logo - Main', 'borgholm-core' ),
					'description'   => esc_html__( 'Choose main logo image', 'borgholm-core' ),
					'default_value' => defined( 'BORGHOLM_ASSETS_ROOT' ) ? BORGHOLM_ASSETS_ROOT . '/img/logo.png' : '',
					'multiple'      => 'no'
				)
			);

			$header_tab->add_field_element(
				array(
					'field_type'  => 'image',
					'name'        => 'qodef_logo_dark',
					'title'       => esc_html__( 'Logo - Dark', 'borgholm-core' ),
					'description' => esc_html__( 'Choose dark logo image', 'borgholm-core' ),
					'multiple'    => 'no'
				)
			);

			$header_tab->add_field_element(
				array(
					'field_type'  => 'image',
					'name'        => 'qodef_logo_light',
					'title'       => esc_html__( 'Logo - Light', 'borgholm-core' ),
					'description' => esc_html__( 'Choose light logo image', 'borgholm-core' ),
					'multiple'    => 'no'
				)
			);

			// Hook to include additional options after module options
			do_action( 'borgholm_core_action_after_header_logo_options_map', $page, $header_tab );
		}
	}

	add_action( 'borgholm_core_action_default_options_init', 'borgholm_core_add_logo_options', borgholm_core_get_admin_options_map_position( 'logo' ) );
}