<?php
// Include logo
borgholm_core_get_header_logo_image();

// Include main navigation
borgholm_core_template_part( 'header', 'templates/parts/navigation' );

// Include widget area one
borgholm_core_get_header_widget_area();