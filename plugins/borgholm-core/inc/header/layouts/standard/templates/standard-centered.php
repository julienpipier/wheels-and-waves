<div class="qodef-header-wrapper">
	<div class="qodef-header-logo">
		<?php
		// Include logo
		borgholm_core_get_header_logo_image(); ?>
	</div>
	<?php
	// Include main navigation
	borgholm_core_template_part( 'header', 'templates/parts/navigation' );
	
	// Include widget area one ?>
	<div class="qodef-widget-holder">
		<?php borgholm_core_get_header_widget_area(); ?>
	</div>
</div>