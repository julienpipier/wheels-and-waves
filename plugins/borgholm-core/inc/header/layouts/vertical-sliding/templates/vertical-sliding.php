<?php do_action( 'borgholm_action_before_page_header' ); ?>

<header id="qodef-page-header">
	<div id="qodef-page-header-inner" <?php borgholm_class_attribute( apply_filters( 'borgholm_filter_header_inner_class', '' ) ); ?>>
		<div class="qodef-vertical-sliding-area qodef--static">
			<?php
			// include logo
			borgholm_core_get_header_logo_image();
			
			// include opener
			borgholm_core_get_opener_icon_html( array(
				'option_name'  => 'vertical_sliding_menu',
				'custom_class' => 'qodef-vertical-sliding-menu-opener'
			), true );
			
			// include widget area one
			?>
			<div class="qodef-vertical-sliding-widget-holder">
				<?php borgholm_core_get_header_widget_area(); ?>
			</div>
		</div>
		<div class="qodef-vertical-sliding-area qodef--dynamic">
			<?php
			// include logo
			borgholm_core_get_header_logo_image( array( 'vertical_sliding_logo' => true ) );
			
			// include vertical sliding navigation
			borgholm_core_template_part( 'header', 'layouts/vertical-sliding/templates/navigation' );
			
			// include widget area two
			?>
			<div class="qodef-vertical-sliding-widget-holder">
				<?php borgholm_core_get_header_widget_area( '', 'two' ); ?>
			</div>
		</div>
	</div>
</header>