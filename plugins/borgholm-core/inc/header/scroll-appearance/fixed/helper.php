<?php

if ( ! function_exists( 'borgholm_core_add_fixed_header_option' ) ) {
	/**
	 * This function set header scrolling appearance value for global header option map
	 */
	function borgholm_core_add_fixed_header_option( $options ) {
		$options['fixed'] = esc_html__( 'Fixed', 'borgholm-core' );

		return $options;
	}

	add_filter( 'borgholm_core_filter_header_scroll_appearance_option', 'borgholm_core_add_fixed_header_option' );
}