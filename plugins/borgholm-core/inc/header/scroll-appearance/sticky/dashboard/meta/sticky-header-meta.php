<?php

if ( ! function_exists( 'borgholm_core_add_sticky_header_meta_options' ) ) {
	/**
	 * Function that add additional meta box options for current module
	 *
	 * @param object $section
	 * @param array $custom_sidebars
	 */
	function borgholm_core_add_sticky_header_meta_options( $section, $custom_sidebars ) {
		
		if ( $section ) {
			
			$section->add_field_element(
				array(
					'field_type'  => 'text',
					'name'        => 'qodef_sticky_header_scroll_amount',
					'title'       => esc_html__( 'Sticky Scroll Amount', 'borgholm-core' ),
					'description' => esc_html__( 'Enter scroll amount for sticky header to appear', 'borgholm-core' ),
					'args'        => array(
						'suffix' => esc_html__( 'px', 'borgholm-core' )
					),
					'dependency'  => array(
						'show' => array(
							'qodef_header_scroll_appearance' => array(
								'values'        => 'sticky',
								'default_value' => ''
							)
						)
					)
				)
			);
			
			$section->add_field_element(
				array(
					'field_type'  => 'text',
					'name'        => 'qodef_sticky_header_side_padding',
					'title'       => esc_html__( 'Sticky Header Side Padding', 'borgholm-core' ),
					'description' => esc_html__( 'Enter side padding for sticky header area', 'borgholm-core' ),
					'args'        => array(
						'suffix' => esc_html__( 'px or %', 'borgholm-core' )
					),
					'dependency'  => array(
						'show' => array(
							'qodef_header_scroll_appearance' => array(
								'values'        => 'sticky',
								'default_value' => ''
							)
						)
					)
				)
			);
			
			$section->add_field_element(
				array(
					'field_type'  => 'color',
					'name'        => 'qodef_sticky_header_background_color',
					'title'       => esc_html__( 'Sticky Header Background Color', 'borgholm-core' ),
					'description' => esc_html__( 'Enter sticky header background color', 'borgholm-core' ),
					'dependency'  => array(
						'show' => array(
							'qodef_header_scroll_appearance' => array(
								'values'        => 'sticky',
								'default_value' => ''
							)
						)
					)
				)
			);
			
			$section->add_field_element(
				array(
					'field_type'  => 'select',
					'name'        => 'qodef_sticky_header_custom_widget_area_one',
					'title'       => esc_html__( 'Choose Custom Sticky Header Widget Area One', 'borgholm-core' ),
					'description' => esc_html__( 'Choose custom widget area to display in sticky header widget area one', 'borgholm-core' ),
					'options'     => $custom_sidebars,
					'dependency'  => array(
						'show' => array(
							'qodef_header_scroll_appearance' => array(
								'values'        => 'sticky',
								'default_value' => ''
							)
						)
					)
				)
			);

            $section->add_field_element(
                array(
                    'field_type'  => 'select',
                    'name'        => 'qodef_sticky_header_custom_widget_area_two',
                    'title'       => esc_html__( 'Choose Custom Sticky Header Widget Area Two', 'borgholm-core' ),
                    'description' => esc_html__( 'Choose custom widget area to display in sticky header widget area two', 'borgholm-core' ),
                    'options'     => $custom_sidebars,
                    'dependency'  => array(
                        'show' => array(
                            'qodef_header_scroll_appearance' => array(
                                'values'        => 'sticky',
                                'default_value' => ''
                            )
                        )
                    )
                )
            );
		}
	}
	
	add_action( 'borgholm_core_action_after_header_scroll_appearance_meta_options_map', 'borgholm_core_add_sticky_header_meta_options', 10, 2 );
}

if ( ! function_exists( 'borgholm_core_add_sticky_header_logo_meta_options' ) ) {
	/**
	 * Function that add additional header logo meta box options
	 *
	 * @param object $logo_tab
	 * @param array $header_logo_section
	 */
	function borgholm_core_add_sticky_header_logo_meta_options( $logo_tab, $header_logo_section ) {
		
		if ( $header_logo_section ) {
			
			$header_logo_section->add_field_element(
				array(
					'field_type'  => 'image',
					'name'        => 'qodef_logo_sticky',
					'title'       => esc_html__( 'Logo - Sticky', 'borgholm-core' ),
					'description' => esc_html__( 'Choose sticky logo image', 'borgholm-core' ),
					'multiple'    => 'no'
				)
			);
		}
	}
	
	add_action( 'borgholm_core_action_after_page_logo_meta_map', 'borgholm_core_add_sticky_header_logo_meta_options', 10, 2 );
}