<?php

if ( ! function_exists( 'borgholm_core_add_working_hours_list_widget' ) ) {
	/**
	 * Function that add widget into widgets list for registration
	 *
	 * @param array $widgets
	 *
	 * @return array
	 */
	function borgholm_core_add_working_hours_list_widget( $widgets ) {
		$widgets[] = 'BorgholmCoreWorkingHoursListWidget';
		
		return $widgets;
	}
	
	add_filter( 'borgholm_core_filter_register_widgets', 'borgholm_core_add_working_hours_list_widget' );
}

if ( class_exists( 'QodeFrameworkWidget' ) ) {
	class BorgholmCoreWorkingHoursListWidget extends QodeFrameworkWidget {
		
		public function map_widget() {
			$this->set_widget_option(
				array(
					'field_type' => 'text',
					'name'       => 'widget_title',
					'title'      => esc_html__( 'Title', 'borgholm-core' )
				)
			);
			$widget_mapped = $this->import_shortcode_options( array(
				'shortcode_base' => 'borgholm_core_working_hours_list'
			) );
			if ( $widget_mapped ) {
				$this->set_base( 'borgholm_core_working_hours_list' );
				$this->set_name( esc_html__( 'Borgholm Working Hours List', 'borgholm-core' ) );
				$this->set_description( esc_html__( 'Add a working hours list element into widget areas', 'borgholm-core' ) );
			}
		}
		
		public function render( $atts ) {
			$params = $this->generate_string_params( $atts );
			
			echo do_shortcode( "[borgholm_core_working_hours_list $params]" ); // XSS OK
		}
	}
}