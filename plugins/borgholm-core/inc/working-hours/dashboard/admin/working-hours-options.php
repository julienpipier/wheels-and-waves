<?php

if ( ! function_exists( 'borgholm_core_add_working_hours_options' ) ) {
	/**
	 * Function that add general options for this module
	 */
	function borgholm_core_add_working_hours_options() {
		$qode_framework = qode_framework_get_framework_root();

		$page = $qode_framework->add_options_page(
			array(
				'scope'       => BORGHOLM_CORE_OPTIONS_NAME,
				'type'        => 'admin',
				'slug'        => 'working-hours',
				'icon'        => 'fa fa-book',
				'title'       => esc_html__( 'Working Hours', 'borgholm-core' ),
				'description' => esc_html__( 'Global Working Hours Options', 'borgholm-core' )
			)
		);

		if ( $page ) {
			$page->add_field_element(
				array(
					'field_type' => 'text',
					'name'       => 'qodef_working_hours_monday',
					'title'      => esc_html__( 'Working Hours For Monday', 'borgholm-core' )
				)
			);

			$page->add_field_element(
				array(
					'field_type' => 'text',
					'name'       => 'qodef_working_hours_tuesday',
					'title'      => esc_html__( 'Working Hours For Tuesday', 'borgholm-core' )
				)
			);

			$page->add_field_element(
				array(
					'field_type' => 'text',
					'name'       => 'qodef_working_hours_wednesday',
					'title'      => esc_html__( 'Working Hours For Wednesday', 'borgholm-core' )
				)
			);

			$page->add_field_element(
				array(
					'field_type' => 'text',
					'name'       => 'qodef_working_hours_thursday',
					'title'      => esc_html__( 'Working Hours For Thursday', 'borgholm-core' )
				)
			);

			$page->add_field_element(
				array(
					'field_type' => 'text',
					'name'       => 'qodef_working_hours_friday',
					'title'      => esc_html__( 'Working Hours For Friday', 'borgholm-core' )
				)
			);

			$page->add_field_element(
				array(
					'field_type' => 'text',
					'name'       => 'qodef_working_hours_saturday',
					'title'      => esc_html__( 'Working Hours For Saturday', 'borgholm-core' )
				)
			);

			$page->add_field_element(
				array(
					'field_type' => 'text',
					'name'       => 'qodef_working_hours_sunday',
					'title'      => esc_html__( 'Working Hours For Sunday', 'borgholm-core' )
				)
			);

			$page->add_field_element(
				array(
					'field_type' => 'checkbox',
					'name'       => 'qodef_working_hours_special_days',
					'title'      => esc_html__( 'Special Days', 'borgholm-core' ),
					'options'    => array(
						'monday'    => esc_html__( 'Monday', 'borgholm-core' ),
						'tuesday'   => esc_html__( 'Tuesday', 'borgholm-core' ),
						'wednesday' => esc_html__( 'Wednesday', 'borgholm-core' ),
						'thursday'  => esc_html__( 'Thursday', 'borgholm-core' ),
						'friday'    => esc_html__( 'Friday', 'borgholm-core' ),
						'saturday'  => esc_html__( 'Saturday', 'borgholm-core' ),
						'sunday'    => esc_html__( 'Sunday', 'borgholm-core' ),
					)
				)
			);

			$page->add_field_element(
				array(
					'field_type' => 'text',
					'name'       => 'qodef_working_hours_special_text',
					'title'      => esc_html__( 'Featured Text For Special Days', 'borgholm-core' )
				)
			);

			// Hook to include additional options after module options
			do_action( 'borgholm_core_action_after_working_hours_options_map', $page );
		}
	}

	add_action( 'borgholm_core_action_default_options_init', 'borgholm_core_add_working_hours_options', borgholm_core_get_admin_options_map_position( 'working-hours' ) );
}