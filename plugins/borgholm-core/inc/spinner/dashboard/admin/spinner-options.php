<?php

if ( ! function_exists( 'borgholm_core_add_page_spinner_options' ) ) {
	/**
	 * Function that add general options for this module
	 */
	function borgholm_core_add_page_spinner_options( $page ) {
		
		if ( $page ) {
			$page->add_field_element(
				array(
					'field_type'    => 'yesno',
					'name'          => 'qodef_enable_page_spinner',
					'title'         => esc_html__( 'Enable Page Spinner', 'borgholm-core' ),
					'description'   => esc_html__( 'Enable Page Spinner Effect', 'borgholm-core' ),
					'default_value' => 'no'
				)
			);
			
			$spinner_section = $page->add_section_element(
				array(
					'name'       => 'qodef_page_spinner_section',
					'title'      => esc_html__( 'Page Spinner Section', 'borgholm-core' ),
					'dependency' => array(
						'show' => array(
							'qodef_enable_page_spinner' => array(
								'values'        => 'yes',
								'default_value' => 'no'
							)
						)
					)
				)
			);
			
			$spinner_section->add_field_element(
				array(
					'field_type'    => 'select',
					'name'          => 'qodef_page_spinner_type',
					'title'         => esc_html__( 'Select Page Spinner Type', 'borgholm-core' ),
					'description'   => esc_html__( 'Choose a page spinner animation style', 'borgholm-core' ),
					'options'       => apply_filters( 'borgholm_core_filter_page_spinner_layout_options', array() ),
					'default_value' => apply_filters( 'borgholm_core_filter_page_spinner_default_layout_option', '' ),
				)
			);
			
			$spinner_section->add_field_element(
				array(
					'field_type'  => 'color',
					'name'        => 'qodef_page_spinner_background_color',
					'title'       => esc_html__( 'Spinner Background Color', 'borgholm-core' ),
					'description' => esc_html__( 'Choose the spinner background color', 'borgholm-core' )
				)
			);
			
			$spinner_section->add_field_element(
				array(
					'field_type'  => 'color',
					'name'        => 'qodef_page_spinner_color',
					'title'       => esc_html__( 'Spinner Color', 'borgholm-core' ),
					'description' => esc_html__( 'Choose the spinner color', 'borgholm-core' )
				)
			);
			
			$spinner_section->add_field_element(
				array(
					'field_type'    => 'yesno',
					'name'          => 'qodef_page_spinner_fade_out_animation',
					'title'         => esc_html__( 'Enable Fade Out Animation', 'borgholm-core' ),
					'description'   => esc_html__( 'Enabling this option will turn on fade out animation when leaving page', 'borgholm-core' ),
					'default_value' => 'no',
				)
			);
		}
	}
	
	add_action( 'borgholm_core_action_after_general_options_map', 'borgholm_core_add_page_spinner_options' );
}