<?php

if ( ! function_exists( 'borgholm_core_add_stripes_spinner_layout_option' ) ) {
	/**
	 * Function that set new value into page spinner layout options map
	 *
	 * @param array $layouts  - module layouts
	 *
	 * @return array
	 */
	function borgholm_core_add_stripes_spinner_layout_option( $layouts ) {
		$layouts['stripes'] = esc_html__( 'Stripes', 'borgholm-core' );
		
		return $layouts;
	}
	
	add_filter( 'borgholm_core_filter_page_spinner_layout_options', 'borgholm_core_add_stripes_spinner_layout_option' );
}