<?php

if ( ! function_exists( 'borgholm_core_add_clock_spinner_layout_option' ) ) {
	/**
	 * Function that set new value into page spinner layout options map
	 *
	 * @param array $layouts  - module layouts
	 *
	 * @return array
	 */
	function borgholm_core_add_clock_spinner_layout_option( $layouts ) {
		$layouts['clock'] = esc_html__( 'Clock', 'borgholm-core' );
		
		return $layouts;
	}
	
	add_filter( 'borgholm_core_filter_page_spinner_layout_options', 'borgholm_core_add_clock_spinner_layout_option' );
}