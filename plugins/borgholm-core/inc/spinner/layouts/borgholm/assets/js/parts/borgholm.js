(function ($) {
    "use strict";

    $(document).ready(function () {
        qodefBorgholmSpinner.init();
    });

    $(window).on('elementor/frontend/init', function () {
        var isEditMode = Boolean(elementorFrontend.isEditMode());

        if ( isEditMode ) {
            qodefBorgholmSpinner.init( isEditMode );
        }
    });

    var qodefBorgholmSpinner = {
        init: function ( isEditMode ) {
            var $holder = $('#qodef-page-spinner.qodef-layout--borgholm');

            if ( $holder.length ) {
                if ( isEditMode ) {
                    qodefBorgholmSpinner.finishAnimation( $holder );
                } else {
                    qodefBorgholmSpinner.animateSpinner( $holder );

                    $(window).on('load', function () {
                        qodefBorgholmSpinner.finishAnimation( $holder );
                    });
                }
            }
        },
        finishAnimation: function ( $holder ) {
            var $rev = $('.qodef-landing-rev-holder');

            setTimeout(function () {
                $holder.addClass('qodef-finished');

                setTimeout(function () {
                    qodefBorgholmSpinner.fadeOutLoader( $holder );

                    if ( $rev.length ) {
                        $rev.find('rs-module').revstart();
                    }
                }, 300);
            }, 3000);
        },
        animateSpinner: function ( $holder ) {
            $holder.addClass('qodef-init');
        },
        fadeOutLoader: function ($holder, speed, delay, easing) {
            speed = speed ? speed : 500;
            delay = delay ? delay : 0;
            easing = easing ? easing : 'swing';

            if ($holder.length) {
                $holder.delay(delay).fadeOut(speed, easing);
                $(window).on('bind', 'pageshow', function (event) {
                    if (event.originalEvent.persisted) {
                        $holder.fadeOut(speed, easing);
                    }
                });
            }
        }
    };

})(jQuery);