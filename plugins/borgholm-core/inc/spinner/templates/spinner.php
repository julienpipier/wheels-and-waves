<div id="qodef-page-spinner" <?php qode_framework_class_attribute( implode( ' ', apply_filters( 'borgholm_core_filter_page_spinner_classes', array( 'qodef-m' ) ) ) ); ?>>
	<div class="qodef-m-inner">
		<div class="qodef-m-spinner">
			<?php borgholm_core_get_spinners_type(); ?>
		</div>
	</div>
</div>