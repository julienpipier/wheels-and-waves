<form action="<?php echo esc_url( home_url( '/' ) ); ?>" class="qodef-search-cover-form" method="get">
	<div class="qodef-m-inner">
		<span class="qodef-additional-icon-search"><?php echo borgholm_get_icon( 'icon_search', 'elegant-icons', '' );?></span>
		<input type="text" placeholder="<?php esc_attr_e( 'Search now...', 'borgholm-core' ); ?>" name="s" class="qodef-m-form-field" autocomplete="off" required/>
		<?php borgholm_core_get_opener_icon_html( array(
			'option_name'  => 'search',
			'custom_icon'  => 'search',
			'custom_class' => 'qodef-m-close'
		), false, true ); ?>
	</div>
</form>