<?php

include_once BORGHOLM_CORE_SHORTCODES_PATH . '/countdown/countdown.php';

foreach ( glob( BORGHOLM_CORE_SHORTCODES_PATH . '/countdown/variations/*/include.php' ) as $variation ) {
	include_once $variation;
}