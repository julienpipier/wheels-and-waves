<?php

include_once BORGHOLM_CORE_SHORTCODES_PATH . '/accordion/accordion.php';
include_once BORGHOLM_CORE_SHORTCODES_PATH . '/accordion/accordion-child.php';

foreach ( glob( BORGHOLM_CORE_SHORTCODES_PATH . '/accordion/variations/*/include.php' ) as $variation ) {
	include_once $variation;
}