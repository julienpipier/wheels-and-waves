<?php

class BorgholmCoreElementorIconWithText extends BorgholmCoreElementorWidgetBase {
	
	function __construct( array $data = [], $args = null ) {
		$this->set_shortcode_slug( 'borgholm_core_icon_with_text' );
		
		parent::__construct( $data, $args );
	}
}

borgholm_core_get_elementor_widgets_manager()->register_widget_type( new BorgholmCoreElementorIconWithText() );