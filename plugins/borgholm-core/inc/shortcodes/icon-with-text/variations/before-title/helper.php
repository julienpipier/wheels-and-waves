<?php

if ( ! function_exists( 'borgholm_core_add_icon_with_text_variation_before_title' ) ) {
	/**
	 * Function that add variation layout for this module
	 *
	 * @param array $variations
	 *
	 * @return array
	 */
	function borgholm_core_add_icon_with_text_variation_before_title( $variations ) {
		$variations['before-title'] = esc_html__( 'Before Title', 'borgholm-core' );
		
		return $variations;
	}
	
	add_filter( 'borgholm_core_filter_icon_with_text_layouts', 'borgholm_core_add_icon_with_text_variation_before_title' );
}
