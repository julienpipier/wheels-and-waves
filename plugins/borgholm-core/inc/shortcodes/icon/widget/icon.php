<?php

if ( ! function_exists( 'borgholm_core_add_icon_widget' ) ) {
	/**
	 * Function that add widget into widgets list for registration
	 *
	 * @param array $widgets
	 *
	 * @return array
	 */
	function borgholm_core_add_icon_widget( $widgets ) {
		$widgets[] = 'BorgholmCoreIconWidget';
		
		return $widgets;
	}
	
	add_filter( 'borgholm_core_filter_register_widgets', 'borgholm_core_add_icon_widget' );
}

if ( class_exists( 'QodeFrameworkWidget' ) ) {
	class BorgholmCoreIconWidget extends QodeFrameworkWidget {
		
		public function map_widget() {
			$widget_mapped = $this->import_shortcode_options( array(
				'shortcode_base' => 'borgholm_core_icon'
			) );
			if( $widget_mapped ) {
				$this->set_base( 'borgholm_core_icon' );
				$this->set_name( esc_html__( 'Borgholm Icon', 'borgholm-core' ) );
				$this->set_description( esc_html__( 'Add a icon element into widget areas', 'borgholm-core' ) );
			}
		}
		
		public function render( $atts ) {
			
			$params = $this->generate_string_params( $atts );
			
			echo do_shortcode( "[borgholm_core_icon $params]" ); // XSS OK
		}
	}
}
