(function($) {
    "use strict";

    qodefCore.shortcodes.borgholm_core_cards_gallery = {};

    $(document).ready(function () {
        qodefTripleFrameImageHighlight.init();
    });

    var qodefTripleFrameImageHighlight = {
        init: function () {
            var $holder = $('.qodef-triple-frame-image-highlight');

            if ( $holder.length ) {
                $holder.each(function() {
                    var $thisHolder = $(this),
                        $inner = $thisHolder.find('.qodef-e-inner'),
                        $centeredH = $thisHolder.find('.qodef-m-centered-image-holder'),
                        $leftH = $thisHolder.find('.qodef-m-left-image-holder'),
                        $rightH = $thisHolder.find('.qodef-m-right-image-holder');

                    //state
                    $thisHolder
                        .data('animating', false)
                        .data('autoplay', false);

                    qodefTripleFrameImageHighlight.initClasses( $centeredH, $leftH, $rightH );
                    qodefTripleFrameImageHighlight.resetIndexes( $centeredH, $leftH, $rightH );
                    qodefTripleFrameImageHighlight.setTriggerSize( $thisHolder, $inner );

                    $thisHolder.waitForImages( function () {
                        $thisHolder.appear( function () {
                            qodefTripleFrameImageHighlight.setPositioning( $thisHolder, $inner );
                            $thisHolder.data('autoplay', setInterval( function () {
                                qodefTripleFrameImageHighlight.navigate( $thisHolder, $inner );
                            }, 3000));
                        }, {accX: 0, accY: -50});
                    });

                    $thisHolder.on('click', function ( $event ) {
                        if ( !$thisHolder.data('animating') ) {
                            clearInterval( $thisHolder.data('autoplay') );
                            qodefTripleFrameImageHighlight.navigate( $thisHolder, $inner, $event );
                        }
                    });

                    $(window).on('resize', function() {
                        qodefTripleFrameImageHighlight.setPositioning( $thisHolder, $inner );
                        qodefTripleFrameImageHighlight.setTriggerSize( $thisHolder, $inner );
                        $inner.find('>div').css('transition', 'none');
                    })
                });
            }
        },
        initClasses: function ( $c, $l, $r ) {
            $c.addClass('qodef-c');
            $l.addClass('qodef-l');
            $r.addClass('qodef-r');
        },
        resetIndexes: function ( $c, $l, $r ) {

            if ( $c !== 'undefined' ) {
                $c.css('z-index', 30);
                $l.css('z-index', 20);
                $r.css('z-index', 20);
            }
        },
        setTriggerSize: function ( $holder, $inner ) {
            setTimeout( function () {
                var $holderWidth = $holder.width(),
                    $innerWidth = $inner.width();

                $holder.find('div[class*="trigger"]').width(($holderWidth - $innerWidth) / 2);
            }, 100);
        },
        setPositioning: function ( $holder, $inner ) {
            var $left = $holder.find('.qodef-l'),
                $right = $holder.find('.qodef-r'),
                $centered = $holder.find('.qodef-c');

            var $xOffset = $inner.position().left;

            $left.css({
                'visibility': 'visible',
                'transform-origin': '0% 50%',
                'transform': 'matrix(.68,0,0,.68,-' + $xOffset + ',0)'
            });
            $right.css({
                'visibility': 'visible',
                'transform-origin': '100% 50%',
                'transform': 'matrix(.68,0,0,.68,' + $xOffset + ',0)'
            });
            $centered.css({
                'transform': 'matrix(1, 0, 0, 1, 0, 0)'
            });
        },
        rotateAnimation: function ( $holder, $inner, $direction, $clickedBullet ) {
            $holder.data('animating', true);

            var $bullet = $holder.find('.qodef-m-bullet'),
                $activeBullet = $holder.find('.qodef-m-bullet.qodef--active'),
                $toFront,
                $toBack,
                $toPrep;

            if ( $direction === 'bullet' ) {
                if ( $activeBullet.hasClass('qodef--centered') ) {
                    if ($clickedBullet.closest('.qodef-m-bullet').hasClass('qodef--left') ) {
                        $toFront = $holder.find('.qodef-l');
                        $toBack = $holder.find('.qodef-c');
                        $toPrep = $holder.find('.qodef-r');

                        $toPrep.removeClass('qodef-r').addClass('qodef-l');
                        $toBack.removeClass('qodef-c').addClass('qodef-r');
                        $toFront.removeClass('qodef-l').addClass('qodef-c');

                        $activeBullet.removeClass('qodef--active');
                        $clickedBullet.closest('.qodef-m-bullet').addClass('qodef--active');
                    } else if ($clickedBullet.closest('.qodef-m-bullet').hasClass('qodef--right') ) {
                        $toFront = $holder.find('.qodef-r');
                        $toBack = $holder.find('.qodef-c');
                        $toPrep = $holder.find('.qodef-l');

                        $toPrep.removeClass('qodef-l').addClass('qodef-r');
                        $toBack.removeClass('qodef-c').addClass('qodef-l');
                        $toFront.removeClass('qodef-r').addClass('qodef-c');

                        $activeBullet.removeClass('qodef--active');
                        $clickedBullet.closest('.qodef-m-bullet').addClass('qodef--active');
                    }
                } else if ( $activeBullet.hasClass('qodef--left') ) {
                    if ($clickedBullet.closest('.qodef-m-bullet').hasClass('qodef--centered') ) {
                        $toFront = $holder.find('.qodef-r');
                        $toBack = $holder.find('.qodef-c');
                        $toPrep = $holder.find('.qodef-l');

                        $toPrep.removeClass('qodef-l').addClass('qodef-r');
                        $toBack.removeClass('qodef-c').addClass('qodef-l');
                        $toFront.removeClass('qodef-r').addClass('qodef-c');

                        $activeBullet.removeClass('qodef--active');
                        $clickedBullet.closest('.qodef-m-bullet').addClass('qodef--active');
                    } else if ($clickedBullet.closest('.qodef-m-bullet').hasClass('qodef--right') ) {
                        $toFront = $holder.find('.qodef-l');
                        $toBack = $holder.find('.qodef-c');
                        $toPrep = $holder.find('.qodef-r');

                        $toPrep.removeClass('qodef-r').addClass('qodef-l');
                        $toBack.removeClass('qodef-c').addClass('qodef-r');
                        $toFront.removeClass('qodef-l').addClass('qodef-c');

                        $activeBullet.removeClass('qodef--active');
                        $clickedBullet.closest('.qodef-m-bullet').addClass('qodef--active');
                    }
                } else {
                    if ($clickedBullet.closest('.qodef-m-bullet').hasClass('qodef--centered') ) {
                        $toFront = $holder.find('.qodef-l');
                        $toBack = $holder.find('.qodef-c');
                        $toPrep = $holder.find('.qodef-r');

                        $toPrep.removeClass('qodef-r').addClass('qodef-l');
                        $toBack.removeClass('qodef-c').addClass('qodef-r');
                        $toFront.removeClass('qodef-l').addClass('qodef-c');

                        $activeBullet.removeClass('qodef--active');
                        $clickedBullet.closest('.qodef-m-bullet').addClass('qodef--active');
                    } else if ($clickedBullet.closest('.qodef-m-bullet').hasClass('qodef--left') ) {
                        $toFront = $holder.find('.qodef-r');
                        $toBack = $holder.find('.qodef-c');
                        $toPrep = $holder.find('.qodef-l');

                        $toPrep.removeClass('qodef-l').addClass('qodef-r');
                        $toBack.removeClass('qodef-c').addClass('qodef-l');
                        $toFront.removeClass('qodef-r').addClass('qodef-c');

                        $activeBullet.removeClass('qodef--active');
                        $clickedBullet.closest('.qodef-m-bullet').addClass('qodef--active');
                    }
                }
            } else if ( $direction === 'left' ) {
                $toFront = $holder.find('.qodef-l');
                $toBack = $holder.find('.qodef-c');
                $toPrep = $holder.find('.qodef-r');

                $toPrep.removeClass('qodef-r').addClass('qodef-l');
                $toBack.removeClass('qodef-c').addClass('qodef-r');
                $toFront.removeClass('qodef-l').addClass('qodef-c');

                if ( $activeBullet.hasClass('qodef--left' ) ) {
                    $bullet.last().addClass('qodef--active');
                    $activeBullet.removeClass('qodef--active');
                } else {
                    $activeBullet.prev().addClass('qodef--active');
                    $activeBullet.removeClass('qodef--active');
                }
            } else {
                $toFront = $holder.find('.qodef-r');
                $toBack = $holder.find('.qodef-c');
                $toPrep = $holder.find('.qodef-l');

                $toPrep.removeClass('qodef-l').addClass('qodef-r');
                $toBack.removeClass('qodef-c').addClass('qodef-l');
                $toFront.removeClass('qodef-r').addClass('qodef-c');

                if ( $activeBullet.hasClass('qodef--right' ) ) {
                    $bullet.first().addClass('qodef--active');
                    $activeBullet.removeClass('qodef--active');
                } else {
                    $activeBullet.next().addClass('qodef--active');
                    $activeBullet.removeClass('qodef--active');
                }
            }

            if ( $toPrep === undefined ) {
                $holder.data('animating', false);
                clearInterval($holder.data('autoplay'));
                $holder.data('autoplay', setInterval(function () {
                    qodefTripleFrameImageHighlight.navigate($holder, $inner);
                }, 3000));
            } else {
                $toPrep.css({
                    'z-index': 15,
                    'transform-origin': '0% 50%',
                    'transition': 'transform .5s, transform-origin .5s'
                });
                $toBack.css({
                    'z-index': 25,
                    'transform-origin': '100% 50%',
                    'transition': 'transform 1s cubic-bezier(0.19, 1, 0.22, 1) .2s, \
                                    transform-origin 1s cubic-bezier(0.19, 1, 0.22, 1) .2s'
                });
                $toFront.css({
                    'z-index': 20,
                    'transform-origin': '50% 50%',
                    'transition': 'transform .75s cubic-bezier(0.86, 0, 0.07, 1) .5s, \
                                    transform-origin .75s cubic-bezier(0.86, 0, 0.07, 1) .5s'
                });

                $holder.find('a').css('pointer-events', 'none');
                setTimeout(function () {
                    $holder.find('a').css('pointer-events', 'auto');
                    qodefTripleFrameImageHighlight.resetIndexes($toFront, $toPrep, $toBack);
                }, 500);

                $toFront.one(qodef.transitionEnd, function () {
                    $holder.data('animating', false);
                    clearInterval($holder.data('autoplay'));
                    $holder.data('autoplay', setInterval(function () {
                        qodefTripleFrameImageHighlight.navigate($holder, $inner);
                    }, 3000));
                });
            }
        },
        navigate: function ( $holder, $inner, $event ) {
            var $direction,
                $clickedBullet,
                $linkActive = false;

            if ( typeof $event !== 'undefined' ) {

                if ( $event.target.className.baseVal === 'qodef-svg-circle' ) {
                    $direction = 'bullet';
                    $clickedBullet = $($event.target);
                }

                switch ( $event.target.className ) {
                    case 'qodef-svg-circle':
                        $direction = 'bullet';
                        break;
                    case 'qodef-m-trigger--left':
                        $direction = 'left';
                        break;
                    case 'qodef-m-trigger--right':
                        $direction = 'right';
                        break;
                    case 'qodef-m-link':
                        $linkActive = true;
                        $holder.data('animating', false);
                        clearInterval($holder.data('autoplay'));
                        break;
                }
            } else {
                $direction = 'right';
            }

            if ( !$linkActive ) {
                qodefTripleFrameImageHighlight.rotateAnimation( $holder, $inner, $direction, $clickedBullet );
                qodefTripleFrameImageHighlight.setPositioning( $holder, $inner );
            }
        }
    }
})(jQuery);
