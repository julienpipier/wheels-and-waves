<?php

if ( ! function_exists( 'borgholm_core_add_triple_frame_image_highlight_shortcode' ) ) {
    /**
     * Function that add shortcode into shortcodes list for registration
     *
     * @param array $shortcodes
     *
     * @return array
     */
    function borgholm_core_add_triple_frame_image_highlight_shortcode( $shortcodes ) {
        $shortcodes[] = 'BorgholmCoreTripleFrameImageHighlightShortcode';

        return $shortcodes;
    }

    add_filter( 'borgholm_core_filter_register_shortcodes', 'borgholm_core_add_triple_frame_image_highlight_shortcode' );
}

if ( class_exists( 'BorgholmCoreShortcode' ) ) {
    class BorgholmCoreTripleFrameImageHighlightShortcode extends BorgholmCoreShortcode {

        public function map_shortcode() {
            $this->set_shortcode_path( BORGHOLM_CORE_SHORTCODES_URL_PATH . '/triple-frame-image-highlight' );
            $this->set_base( 'borgholm_core_triple_frame_image_highlight' );
            $this->set_name( esc_html__( 'Triple Frame Image Highlight', 'borgholm-core' ) );
            $this->set_description( esc_html__( 'Shortcode that adds triple frame image highlight element', 'borgholm-core' ) );
            $this->set_category( esc_html__( 'Borgholm Core', 'borgholm-core' ) );
            $this->set_option( array(
                'field_type' => 'text',
                'name'       => 'custom_class',
                'title'      => esc_html__( 'Custom Class', 'borgholm-core' ),
            ) );
            $this->set_option( array(
                'field_type' => 'image',
                'name'       => 'centered_image',
                'title'      => esc_html__( 'Centered Image', 'borgholm-core' ),
            ) );
            $this->set_option( array(
                'field_type' => 'text',
                'name'       => 'centered_image_link',
                'title'      => esc_html__( 'Centered Image Link', 'borgholm-core' ),
            ) );
            $this->set_option( array(
                'field_type' => 'image',
                'name'       => 'left_image',
                'title'      => esc_html__( 'Left Image', 'borgholm-core' ),
            ) );
            $this->set_option( array(
                'field_type' => 'text',
                'name'       => 'left_image_link',
                'title'      => esc_html__( 'Left Image Link', 'borgholm-core' ),
            ) );
            $this->set_option( array(
                'field_type' => 'image',
                'name'       => 'right_image',
                'title'      => esc_html__( 'Right Image', 'borgholm-core' ),
            ) );
            $this->set_option( array(
                'field_type' => 'text',
                'name'       => 'right_image_link',
                'title'      => esc_html__( 'Right Image Link', 'borgholm-core' ),
            ) );
            $this->set_option( array(
                'field_type'    => 'select',
                'name'          => 'link_target',
                'title'         => esc_html__( 'Link Target', 'borgholm-core' ),
                'options'       => borgholm_core_get_select_type_options_pool( 'link_target' ),
                'default_value' => '_self'
            ) );
            $this->map_extra_options();
        }

        public static function call_shortcode( $params ) {
            $html = qode_framework_call_shortcode( 'borgholm_core_triple_frame_image_highlight', $params );
            $html = str_replace( "\n", '', $html );

            return $html;
        }

        public function render( $options, $content = null ) {
            parent::render( $options );
            $atts = $this->get_atts();

            $atts['holder_classes'] = $this->get_holder_classes( $atts );

            return borgholm_core_get_template_part( 'shortcodes/triple-frame-image-highlight', 'templates/triple-frame-image-highlight', '', $atts );
        }

        private function get_holder_classes( $atts ) {
            $holder_classes = $this->init_holder_classes();

            $holder_classes[] = 'qodef-triple-frame-image-highlight';

            return implode( ' ', $holder_classes );
        }
    }
}