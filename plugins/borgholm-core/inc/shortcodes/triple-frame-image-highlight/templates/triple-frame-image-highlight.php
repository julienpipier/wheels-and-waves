<div <?php qode_framework_class_attribute( $holder_classes ); ?>>
    <div class="qodef-e-holder">
        <div class="qodef-e-inner">
            <?php
                $images = array('centered', 'left', 'right');
            ?>
            <?php foreach ( $images as $image ) { ?>
                <div class="qodef-m-image-holder qodef-m-<?php echo "{$image}" ?>-image-holder">
                    <?php if ( !empty( ${$image.'_image_link'} ) ) { ?>
                        <a class="qodef-m-link" href="<?php echo esc_url( ${$image.'_image_link'} ) ?>" target="<?php echo esc_attr( $link_target ) ?>"></a>
                    <?php } ?>
                    <img class="qodef-m-<?php echo "{$image}" ?>-image" src="<?php echo wp_get_attachment_url( ${$image.'_image'} ); ?>" alt="<?php echo get_the_title( ${$image.'_image'} ) ?>" />
                </div>
            <?php } ?>
        </div>
        <div class="qodef-m-trigger--left"></div>
        <div class="qodef-m-trigger--right"></div>
    </div>
    <div class="qodef-m-pagination">
        <span class="qodef-m-bullet qodef--left">
            <svg class="qodef-svg-circle"><circle cx="50%" cy="50%" r="45%"></circle></svg>
        </span>
        <span class="qodef-m-bullet qodef--centered qodef--active">
            <svg class="qodef-svg-circle"><circle cx="50%" cy="50%" r="45%"></circle></svg>
        </span>
        <span class="qodef-m-bullet qodef--right">
            <svg class="qodef-svg-circle"><circle cx="50%" cy="50%" r="45%"></circle></svg>
        </span>
    </div>
</div>