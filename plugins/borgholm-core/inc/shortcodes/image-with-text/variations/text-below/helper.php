<?php

if ( ! function_exists( 'borgholm_core_add_image_with_text_variation_text_below' ) ) {
	/**
	 * Function that add variation layout for this module
	 *
	 * @param array $variations
	 *
	 * @return array
	 */
	function borgholm_core_add_image_with_text_variation_text_below( $variations ) {
		$variations['text-below'] = esc_html__( 'Text Below', 'borgholm-core' );
		
		return $variations;
	}
	
	add_filter( 'borgholm_core_filter_image_with_text_layouts', 'borgholm_core_add_image_with_text_variation_text_below' );
}
