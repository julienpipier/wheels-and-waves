<div <?php qode_framework_class_attribute( $holder_classes ); ?>>
	<?php borgholm_core_template_part( 'shortcodes/image-with-text', 'templates/parts/image', '', $params ) ?>
	<div class="qodef-m-content">
		<?php borgholm_core_template_part( 'shortcodes/image-with-text', 'templates/parts/title', '', $params ) ?>
		<?php borgholm_core_template_part( 'shortcodes/image-with-text', 'templates/parts/text', '', $params ) ?>
		<?php borgholm_core_template_part( 'shortcodes/image-with-text', 'templates/parts/background-label', '', $params ) ?>
	</div>
</div>