<?php if ( ! empty( $button_params ) && ! empty ( $button_params['text'] ) && class_exists( 'BorgholmCoreButtonShortcode' ) ) { ?>
	<div class="qodef-m-button">
		<?php echo BorgholmCoreButtonShortcode::call_shortcode( $button_params ); ?>
	</div>
<?php } ?>