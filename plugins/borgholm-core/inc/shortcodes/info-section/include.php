<?php

include_once BORGHOLM_CORE_SHORTCODES_PATH . '/info-section/info-section.php';

foreach ( glob( BORGHOLM_CORE_SHORTCODES_PATH . '/info-section/variations/*/include.php' ) as $variation ) {
	include_once $variation;
}