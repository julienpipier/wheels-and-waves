<?php

include_once BORGHOLM_CORE_SHORTCODES_PATH . '/counter/counter.php';

foreach ( glob( BORGHOLM_CORE_SHORTCODES_PATH . '/counter/variations/*/include.php' ) as $variation ) {
	include_once $variation;
}