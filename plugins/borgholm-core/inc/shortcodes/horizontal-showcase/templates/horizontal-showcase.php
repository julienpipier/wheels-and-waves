<div <?php qode_framework_class_attribute( $holder_classes ); ?>>
    <div class="qodef-slides-holder">
        <?php if ( ! empty( $featured_image ) ) { ?>
            <div class="qodef-m-item qodef-horizontal-slide qodef-featured">
                <?php echo wp_get_attachment_image( $featured_image, 'full' ); ?>
            </div>
        <?php } ?>
        <?php $counter = 1; $max_count = count( $items );
        foreach ( $items as $item ) { ?>
            <div <?php qode_framework_class_attribute( $this_shortcode->get_slide_classes( $item ) ); ?> <?php qode_framework_inline_style( $this_shortcode->get_slide_styles( $item ) ); ?>>
                <div class="qodef-slide-content">
                    <?php if ( ! empty( $item['item_tagline'] ) || ! empty( $item['item_title'] ) || ! empty( $item['item_text'] ) ) { ?>
                        <div class="qodef-slide-title">
                            <?php echo BorgholmCoreSectionTitleShortcode::call_shortcode( array(
                                'tagline'                   => $item['item_tagline'],
                                'tagline_margin_bottom'     => '11px',
                                'title'                     => $item['item_title'],
                                'line_break_positions'      => $item['item_line_break_positions'],
                                'disable_title_break_words' => $item['item_disable_title_break_words'],
                                'special_style_positions'   => $item['item_special_style_positions'],
                                'title_tag'                 => 'h1',
                                'link'                      => $item['item_link'],
                                'target'                    => $item['item_target'],
                                'animate_special_style'     => $item['item_animate_special_style'],
                                'text'                      => $item['item_text'],
                                'enable_text_custom_styles' => $item['item_enable_text_custom_styles'],
                                'text_margin_top'           => '12px'
                            ) ); ?>
                        </div>
                    <?php } ?>
                    <?php if ( ! empty( $item['item_quote'] ) ) { ?>
                        <div class="qodef-slide-quote">
                            <?php
                            if ( $special_style_positions = explode( ',', str_replace( ' ', '', $item['item_quote_special_style_positions'] ) ) ) {
                                $item['item_quote'] = borgholm_modify_text_special_style_words($item['item_quote'], $special_style_positions);
                            }
                            echo qode_framework_wp_kses_html( 'content', $item['item_quote'] );
                            ?>
                        </div>
                        <?php if ( ! empty( $item['item_author'] ) ) { ?>
                            <div class="qodef-slide-author-holder">
                                <span class="qodef-slide-author"><?php echo esc_html( $item['item_author'] ); ?></span>
                                <span class="qodef-slide-author-label"><?php echo esc_html__( 'Author', 'borgholm-core' ); ?></span>
                            </div>
                        <?php } ?>
                    <?php } ?>
                    <?php if ( ! empty( $item['item_contact_address'] ) || ! empty( $item['item_general_inquiries'] ) || ! empty ( $item['item_about_us'] ) ) { ?>
                        <div class="qodef-slide-additional-info">
                            <?php if ( ! empty( $item['item_contact_address'] ) || ! empty( $item['item_general_inquiries'] ) ) { ?>
                                <div class="qodef-slide-additional-info-part">
                                    <?php if ( ! empty( $item['item_contact_address'] ) ) { ?>
                                        <div class="qodef-e qodef-slide-contact-address">
                                            <div class="qodef-label"><?php echo esc_html__( 'Contact Address', 'borgholm-core' ); ?></div>
                                            <div class="qodef-value"><?php echo esc_html( $item['item_contact_address'] ); ?></div>
                                        </div>
                                    <?php } ?>
                                    <?php if ( ! empty( $item['item_general_inquiries'] ) ) { ?>
                                        <div class="qodef-e qodef-slide-general-inquiries">
                                            <div class="qodef-label"><?php echo esc_html__( 'General Inquiries', 'borgholm-core' ); ?></div>
                                            <div class="qodef-value"><?php echo esc_html( $item['item_general_inquiries'] ); ?></div>
                                        </div>
                                    <?php } ?>
                                </div>
                            <?php } ?>
                            <?php if ( ! empty ( $item['item_about_us'] ) ) { ?>
                                <div class="qodef-slide-additional-info-part">
                                    <div class="qodef-e qodef-slide-about-us">
                                        <div class="qodef-label"><?php echo esc_html__( 'About Us', 'borgholm-core' ); ?></div>
                                        <div class="qodef-value"><?php echo esc_html( $item['item_about_us'] ); ?></div>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    <?php } ?>
                </div>
                <div class="qodef-slide-images">
                    <?php foreach ( array( 1, 2 ) as $index ) {
                        if ( ! empty( $item[ 'item_image' . $index ] ) ) {
                            if ( $index == 2 ) { ?>
                                <div class="qodef-slide-image-separator"></div>
                            <?php } ?>
                            <div class="qodef-slide-image">
                                <?php if ( ! empty( $item[ 'item_image' . $index . '_link' ] ) ) { ?>
                                    <a itemprop="url" class="qodef-img-holder" href="<?php echo esc_url( $item[ 'item_image' . $index . '_link' ] ); ?>" target="<?php echo esc_attr( $item[ 'item_image' . $index . '_target' ] ); ?>">
                                        <?php echo wp_get_attachment_image( $item[ 'item_image' . $index ], 'full', false, array('loading' => 'eager') ); ?></a>
                                <?php } else { ?>
                                    <span class="qodef-img-holder">
								        <?php echo wp_get_attachment_image( $item[ 'item_image' . $index ], 'full', false, array('loading' => 'eager') ); ?></span>
                                <?php } ?>
                                <?php if ( ! empty( $item[ 'item_image' . $index . '_title' ] ) || ! empty( $item[ 'item_image' . $index . '_subtitle' ] ) ) { ?>
                                    <div class="qodef-slide-image-meta">
                                        <?php if ( ! empty( $item[ 'item_image' . $index . '_subtitle' ] ) ) { ?>
                                            <div class="qodef-slide-image-subtitle"><?php echo esc_html( $item[ 'item_image' . $index . '_subtitle' ] ); ?></div><?php } ?>
                                        <?php if ( ! empty( $item[ 'item_image' . $index . '_title' ] ) ) { ?>
                                            <h5 class="qodef-slide-image-title"><?php echo esc_html( $item[ 'item_image' . $index . '_title' ] ); ?></h5><?php } ?>
                                    </div>
                                <?php } ?>
                            </div>
                        <?php }
                    } ?>
                </div>
            </div>
            <?php if ( $counter == $max_count ) { ?>
                <div class="qodef-scroll-back">
                    <span class="qodef-mark"><?php echo borgholm_arrow_slim_left_svg(); ?></span>
                    <span class="qodef-label"><?php echo esc_html__( 'Back to Start', 'borgholm-core' ); ?></span>
                </div>
            <?php }

            $counter++;
        } ?>
    </div>
    <?php if ( ! empty( $featured_image ) ) { ?>
        <div class="qodef-featured-fixed">
            <?php echo wp_get_attachment_image( $featured_image, 'full' ); ?>
        </div>
    <?php } ?>
</div>