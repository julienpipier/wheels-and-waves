(function ($) {
	"use strict";
	qodefCore.shortcodes.borgholm_core_horizontal_showcase = {};

	$(document).ready(function () {
		// qodefCustomHorizontalSlider.init();
	});

	$(window).on('load', function () {
		qodefCustomHorizontalSlider.init();
	});

	var qodefCustomHorizontalSlider = {
		currentHolder: '',
		currentScrollBackHolder: '',
		scrollBackAppearAfter: 10,
		init: function () {
			var holder = $('.qodef-custom-horizontal-slider');

			if (qodef.windowWidth > 680) {
				if(holder.length) {
					holder.each(function() {
						qodefCustomHorizontalSlider.currentHolder = $(this);
						qodefCustomHorizontalSlider.currentScrollBackHolder = $(this).find('.qodef-scroll-back');

						qodefCustomHorizontalSlider.animateSlider($(this));
					});
				}
			} else {
				var $specialStyle = holder.find('.qodef-slide-quote .qodef-special-style');

				if ( $specialStyle.length ) {
					$specialStyle.appear(function () {
						qodefCustomHorizontalSlider.animateSpecialText( $specialStyle, 'qodef-animate', 0 );
					}, {accX: 0, accY: -50});
				}
			}
		},
		animateSlider: function ($thisHolder) {
			var $itemsHolder = $thisHolder.find('.qodef-slides-holder'),
				$items = $itemsHolder.find('.qodef-horizontal-slide'),
				scrollItemOffset = 50,
				scrollInitItemOffset = 50;

			if (qodef.windowWidth < 1025) {

				var mobileHeader = $('#qodef-page-mobile-header'),
					mobileHeaderHeight = mobileHeader.length ? mobileHeader.height() : 0;

				$thisHolder
					.height(qodef.windowHeight - mobileHeaderHeight);
			}

			if (qodef.windowWidth < 769) {
				scrollItemOffset = (qodef.windowWidth / 2) * -1;
				scrollInitItemOffset = 350;
				console.log('test');
			}

			if ( qodef.windowWidth < 681 ) {

			}

			var Scrollbar = window.Scrollbar;

			Scrollbar.use(HorizontalScrollPlugin);
			Scrollbar.use(window.OverscrollPlugin);

			var myScrollbar = Scrollbar.init(document.querySelector('.qodef-custom-horizontal-slider .qodef-slides-holder'),
				{
					damping: 0.05,
					plugins: {
						overscroll: {
							damping: 0.1,
							maxOverscroll: 0
						}
					}
				}
			);

			setTimeout( function () {
				var $itemWidth = $items.width();

				$items.each(function (i) {
					var thisOffsetLeft = $itemWidth * (i + 1);
					$(this).attr('data-offset-left', thisOffsetLeft);
					$(this).data('offset-left', thisOffsetLeft);
				});
			}, 10);

			$items.each(function (i) {
				var thisItem = $(this);
				if (qodef.windowWidth / 2 + scrollInitItemOffset > $(this).data('offset-left')) {
					setTimeout(function () {
						thisItem.addClass('qodef--appear');
					}, i * 200)
				}
			});

			myScrollbar.addListener(function (status) {
				var scrollbarOffset = this.offset.x,
					windowOffset = qodef.windowWidth / 2 - scrollItemOffset;

				if(status.offset.x >= qodefCustomHorizontalSlider.scrollBackAppearAfter && ! qodefCustomHorizontalSlider.currentScrollBackHolder.hasClass('qodef-appear')) {
					qodefCustomHorizontalSlider.currentScrollBackHolder.addClass('qodef-appear');
				}

				if(status.offset.x < qodefCustomHorizontalSlider.scrollBackAppearAfter && qodefCustomHorizontalSlider.currentScrollBackHolder.hasClass('qodef-appear')) {
					qodefCustomHorizontalSlider.currentScrollBackHolder.removeClass('qodef-appear');
				}

				$items.each(function () {
					if ( scrollbarOffset + windowOffset > $(this).prev().data('offset-left') ) {
						$(this).addClass('qodef--appear');

						var $specialStyle = $(this).find('.qodef-slide-quote .qodef-special-style');

						if ( $specialStyle.length ) {
							qodefCustomHorizontalSlider.animateSpecialText( $specialStyle, 'qodef-animate', 0 );
						}
					}
				});
			});

			if(qodefCustomHorizontalSlider.currentScrollBackHolder.length) {
				qodefCustomHorizontalSlider.currentScrollBackHolder.bind('click', function() {
					myScrollbar.scrollTo(0, 0, 1500);
				});
			}
		},
		animateSpecialText: function ( $selector, $class, $delay ) {
			setTimeout(function () {
				$selector.addClass($class);
			}, $delay );
		}
	};

	qodefCore.shortcodes.borgholm_core_horizontal_showcase.qodefCustomHorizontalSlider = qodefCustomHorizontalSlider;

})(jQuery);