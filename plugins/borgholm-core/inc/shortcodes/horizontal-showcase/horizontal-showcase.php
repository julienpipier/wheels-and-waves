<?php

if ( ! function_exists( 'borgholm_core_add_horizontal_showcase_shortcode' ) ) {
	/**
	 * Function that add shortcode into shortcodes list for registration
	 *
	 * @param array $shortcodes
	 *
	 * @return array
	 */
	function borgholm_core_add_horizontal_showcase_shortcode( $shortcodes ) {
		$shortcodes[] = 'BorgholmCoreHorizontalShowcaseShortcode';
		
		return $shortcodes;
	}
	
	add_filter( 'borgholm_core_filter_register_shortcodes', 'borgholm_core_add_horizontal_showcase_shortcode' );
}

if ( class_exists( 'BorgholmCoreShortcode' ) ) {
	class BorgholmCoreHorizontalShowcaseShortcode extends BorgholmCoreShortcode {
		
		public function map_shortcode() {
			$this->set_shortcode_path( BORGHOLM_CORE_SHORTCODES_URL_PATH . '/horizontal-showcase' );
			$this->set_base( 'borgholm_core_horizontal_showcase' );
			$this->set_name( esc_html__( 'Horizontal Showcase', 'borgholm-core' ) );
			$this->set_description( esc_html__( 'Shortcode that adds horizontal leyout holder', 'borgholm-core' ) );
			$this->set_category( esc_html__( 'Borgholm Core', 'borgholm-core' ) );
			$this->set_scripts(
				array_merge(
					array(
						'pixi'                   => array(
							'registered' => false,
							'url'        => BORGHOLM_CORE_SHORTCODES_URL_PATH . '/horizontal-showcase/assets/js/plugins/pixi.min.js',
							'dependency' => array( 'jquery' )
						),
						'TweenMax'               => array(
							'registered' => false,
							'url'        => BORGHOLM_CORE_SHORTCODES_URL_PATH . '/horizontal-showcase/assets/js/plugins/TweenMax.min.js',
							'dependency' => array( 'jquery' )
						),
						'SmoothScrollbar'        => array(
							'registered' => false,
							'url'        => BORGHOLM_CORE_SHORTCODES_URL_PATH . '/horizontal-showcase/assets/js/plugins/smooth-scrollbar.js',
							'dependency' => array( 'jquery' )
						),
						'HorizontalScrollPlugin' => array(
							'registered' => false,
							'url'        => BORGHOLM_CORE_SHORTCODES_URL_PATH . '/horizontal-showcase/assets/js/plugins/HorizontalScrollPlugin.js',
							'dependency' => array( 'jquery' )
						),
						'overscroll'             => array(
							'registered' => false,
							'url'        => BORGHOLM_CORE_SHORTCODES_URL_PATH . '/horizontal-showcase/assets/js/plugins/overscroll.js',
							'dependency' => array( 'jquery' )
						)
					), apply_filters( 'borgholm_core_filter_horizontal_showcase_register_assets', array() )
				)
			);
			$this->set_option( array(
				'field_type' => 'text',
				'name'       => 'custom_class',
				'title'      => esc_html__( 'Custom Class', 'borgholm-core' ),
			) );
			$this->set_option( array(
				'field_type' => 'image',
				'name'       => 'featured_image',
				'title'      => esc_html__( 'Featured Image', 'borgholm-core' )
			) );
			$this->set_option( array(
				'field_type' => 'repeater',
				'name'       => 'children',
				'title'      => esc_html__( 'Slides', 'borgholm-core' ),
				'items'   => array(
					array(
						'field_type'    => 'select',
						'name'          => 'item_size_type',
						'title'         => esc_html__( 'Slide Size Type', 'borgholm-core' ),
						'options'       => array(
							''      => esc_html__( 'Default', 'borgholm-core' ),
							'small' => esc_html__( 'Small', 'borgholm-core' ),
						),
						'default_value' => ''
					),
					array(
						'field_type' => 'color',
						'name'       => 'background_color',
						'title'      => esc_html__( 'Background Color', 'borgholm-core' )
					),
					array(
						'field_type'    => 'text',
						'name'          => 'item_tagline',
						'title'         => esc_html__( 'Tagline', 'borgholm-core' )
					),
					array(
						'field_type'    => 'text',
						'name'          => 'item_title',
						'title'         => esc_html__( 'Title', 'borgholm-core' )
					),
					array(
						'field_type'  => 'text',
						'name'        => 'item_line_break_positions',
						'title'       => esc_html__( 'Positions of Line Break', 'borgholm-core' ),
						'description' => esc_html__( 'Enter the positions of the words after which you would like to create a line break. Separate the positions with commas (e.g. if you would like the first, third, and fourth word to have a line break, you would enter "1,3,4")', 'borgholm-core' )
					),
					array(
						'field_type'    => 'select',
						'name'          => 'item_disable_title_break_words',
						'title'         => esc_html__( 'Disable Title Line Break', 'borgholm-core' ),
						'description'   => esc_html__( 'Enabling this option will disable title line breaks for screen size 1024 and lower', 'borgholm-core' ),
						'options'       => borgholm_core_get_select_type_options_pool( 'no_yes', false ),
						'default_value' => 'no'
					),
					array(
						'field_type'  => 'text',
						'name'        => 'item_special_style_positions',
						'title'       => esc_html__( 'Title Special Words', 'borgholm-core' ),
						'description' => esc_html__( 'Enter the positions of the words which you would like to style in special way. Separate the positions with commas (e.g. if you would like the first, third, and fourth word to have a special style, you would enter "1,3,4")', 'borgholm-core' )
					),
					array(
						'field_type' => 'text',
						'name'       => 'item_link',
						'title'      => esc_html__( 'Title Custom Link', 'borgholm-core' )
					),
					array(
						'field_type'    => 'select',
						'name'          => 'item_target',
						'title'         => esc_html__( 'Custom Link Target', 'borgholm-core' ),
						'options'       => borgholm_core_get_select_type_options_pool( 'link_target' ),
						'default_value' => '_self'
					),
                    array(
                        'field_type'    => 'select',
                        'name'          => 'item_animate_special_style',
                        'title'         => esc_html__( 'Animate Special Style', 'borgholm-core' ),
                        'options'       => borgholm_core_get_select_type_options_pool( 'yes_no', false ),
                        'default_value' => 'no'
                    ),
					array(
						'field_type'    => 'textarea',
						'name'          => 'item_text',
						'title'         => esc_html__( 'Text', 'borgholm-core' )
					),
					array(
						'field_type'    => 'select',
						'name'          => 'item_enable_text_custom_styles',
						'title'         => esc_html__( 'Enable Text Custom Styles', 'borgholm-core' ),
						'description'   => esc_html__( 'Enabling this option will set text custom styles', 'borgholm-core' ),
						'options'       => borgholm_core_get_select_type_options_pool( 'no_yes', false ),
						'default_value' => 'no'
					),
					array(
						'field_type'    => 'textarea',
						'name'          => 'item_quote',
						'title'         => esc_html__( 'Quote', 'borgholm-core' )
					),
					array(
						'field_type'  => 'text',
						'name'        => 'item_quote_special_style_positions',
						'title'       => esc_html__( 'Quote Special Words', 'borgholm-core' ),
						'description' => esc_html__( 'Enter the positions of the words which you would like to style in special way. Separate the positions with commas (e.g. if you would like the first, third, and fourth word to have a special style, you would enter "1,3,4")', 'borgholm-core' )
					),
					array(
						'field_type' => 'text',
						'name'       => 'item_author',
						'title'      => esc_html__( 'Author', 'borgholm-core' )
					),
					array(
						'field_type' => 'text',
						'name'       => 'item_contact_address',
						'title'      => esc_html__( 'Contact Address', 'borgholm-core' )
					),
					array(
						'field_type' => 'text',
						'name'       => 'item_general_inquiries',
						'title'      => esc_html__( 'General Inquiries', 'borgholm-core' )
					),
					array(
						'field_type'    => 'textarea',
						'name'          => 'item_about_us',
						'title'         => esc_html__( 'About Us', 'borgholm-core' )
					),
					array(
						'field_type' => 'image',
						'name'       => 'item_image1',
						'title'      => esc_html__( 'Image 1', 'borgholm-core' )
					),
					array(
						'field_type' => 'text',
						'name'       => 'item_image1_link',
						'title'      => esc_html__( 'Image 1 Link', 'borgholm-core' )
					),
					array(
						'field_type'    => 'select',
						'name'          => 'item_image1_target',
						'title'         => esc_html__( 'Image 1 Link Target', 'borgholm-core' ),
						'options'       => borgholm_core_get_select_type_options_pool( 'link_target' ),
						'default_value' => '_self'
					),
					array(
						'field_type'    => 'text',
						'name'          => 'item_image1_title',
						'title'         => esc_html__( 'Image 1 Title', 'borgholm-core' )
					),
					array(
						'field_type'    => 'text',
						'name'          => 'item_image1_subtitle',
						'title'         => esc_html__( 'Image 1 Subtitle', 'borgholm-core' )
					),
					array(
						'field_type' => 'image',
						'name'       => 'item_image2',
						'title'      => esc_html__( 'Image 2', 'borgholm-core' )
					),
					array(
						'field_type' => 'text',
						'name'       => 'item_image2_link',
						'title'      => esc_html__( 'Image 2 Link', 'borgholm-core' )
					),
					array(
						'field_type'    => 'select',
						'name'          => 'item_image2_target',
						'title'         => esc_html__( 'Image 2 Link Target', 'borgholm-core' ),
						'options'       => borgholm_core_get_select_type_options_pool( 'link_target' ),
						'default_value' => '_self'
					),
					array(
						'field_type'    => 'text',
						'name'          => 'item_image2_title',
						'title'         => esc_html__( 'Image 2 Title', 'borgholm-core' )
					),
					array(
						'field_type'    => 'text',
						'name'          => 'item_image2_subtitle',
						'title'         => esc_html__( 'Image 2 Subtitle', 'borgholm-core' )
					),
                    array(
                        'field_type'    => 'select',
                        'name'          => 'item_image_hover',
                        'title'         => esc_html__( 'Image Hover Animation', 'borgholm-core' ),
                        'options'       => borgholm_core_get_select_type_options_pool( 'yes_no', false ),
                        'default_value' => 'yes'
                    )
				)
			) );
			$this->map_extra_options();
		}
		
		public function render( $options, $content = null ) {
			parent::render( $options );
			$atts = $this->get_atts();
			
			$atts['holder_classes'] = $this->get_holder_classes( $atts );
			$atts['items']          = $this->parse_repeater_items( $atts['children'] );
			$atts['this_shortcode'] = $this;
			
			return borgholm_core_get_template_part( 'shortcodes/horizontal-showcase', 'templates/horizontal-showcase', '', $atts );
		}
		
		private function get_holder_classes( $atts ) {
			$holder_classes = $this->init_holder_classes();
			
			$holder_classes[] = 'qodef-horizontal-showcase qodef-custom-horizontal-slider';

			return implode( ' ', $holder_classes );
		}

		public function get_slide_classes( $slide_atts ) {
			$classes = array( 'qodef-m-item', 'qodef-horizontal-slide' );

			if ( ! empty( $slide_atts['item_size_type'] ) ) {
				$classes[] = 'qodef-size-type--' . $slide_atts['item_size_type'];
			}

			if ( ! empty( $slide_atts['item_image_hover'] && $slide_atts['item_image_hover'] === 'yes' ) ) {
			    $classes[] = 'qodef-hover-animation';
            }

			return implode( ' ', $classes );
		}

		public function get_slide_styles( $slide_atts ) {
			$styles = array();

			if ( ! empty( $slide_atts['background_color'] ) ) {
				$styles[] = 'background-color: ' . $slide_atts['background_color'];
			}

			return $styles;
		}
	}
}