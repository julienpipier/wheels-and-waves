<?php if ( isset( $query_result ) && intval( $max_num_pages ) > 1 ) { ?>
	<div class="qodef-m-pagination qodef--infinite-scroll">
        <span class="qodef-infinite-scroll-spinner">
	        <?php echo borgholm_custom_decoration_svg(); ?>
	    </span>
	</div>
<?php } ?>