<?php

if ( ! function_exists( 'borgholm_core_add_author_list_variation_info_below' ) ) {
	/**
	 * Function that add variation layout for this module
	 *
	 * @param array $variations
	 *
	 * @return array
	 */
	function borgholm_core_add_author_list_variation_info_below( $variations ) {
		$variations['info-below'] = esc_html__( 'Info Below', 'borgholm-core' );
		
		return $variations;
	}
	
	add_filter( 'borgholm_core_filter_author_list_layouts', 'borgholm_core_add_author_list_variation_info_below' );
}
