(function ($) {
    "use strict";

    qodefCore.shortcodes.borgholm_core_section_title = {};

    $(document).ready(function () {
        qodefSectionTitle.init();
    });

    var qodefSectionTitle = {
        init: function () {
            this.holder = $('.qodef-section-title.qodef-animate-special-style');

            if ( this.holder.length ) {
                this.holder.each( function () {
                    var $thisHolder = $(this);

                    if ( $thisHolder.closest('.qodef-horizontal-showcase').length ) {
                        if ( qodef.windowWidth > 680 ) {
                            var $interval = setInterval(function () {
                                if ($thisHolder.closest('.qodef-horizontal-slide').hasClass('qodef--appear')) {
                                    var $specialStyle = $thisHolder.find('.qodef-special-style');

                                    qodefSectionTitle.animateText($specialStyle, 'qodef-animate', 250);

                                    clearInterval($interval);
                                }
                            }, 100);
                        } else {
                            $thisHolder.appear(function () {
                                var $title = $thisHolder.find('.qodef-m-title'),
                                    $specialStyle = $title.find('.qodef-special-style');

                                qodefSectionTitle.animateText( $specialStyle, 'qodef-animate', 0 );
                            }, {accX: 0, accY: -50});
                        }
                    } else if ( $thisHolder.closest('rs-module').length ) {
                        if ( $thisHolder.hasClass('qodef-animate-special-style') ) {
                            var $revSlider = $thisHolder.closest('rs-module'),
                                $specialStyle = $thisHolder.find('.qodef-special-style');

                            $specialStyle.removeClass('qodef-animate');

                            $revSlider.on('revolution.slide.onbeforeswap', function () {
                                $specialStyle.removeClass('qodef-animate');
                            });

                            $revSlider.on('revolution.slide.onafterswap', function () {
                                $specialStyle.addClass('qodef-animate');
                            });
                        }
                    } else {
                        $thisHolder.appear(function () {
                            var $title = $thisHolder.find('.qodef-m-title'),
                                $specialStyle = $title.find('.qodef-special-style');

                            qodefSectionTitle.animateText( $specialStyle, 'qodef-animate', 0 );
                        }, {accX: 0, accY: -50});
                    }
                });
            }
        },
        animateText: function ( $selector, $class, $delay ) {
            setTimeout( function () {
                $selector.addClass($class);
            }, $delay );
        }
    };

    qodefCore.shortcodes.borgholm_core_section_title.qodefSectionTitle = qodefSectionTitle;

})(jQuery);