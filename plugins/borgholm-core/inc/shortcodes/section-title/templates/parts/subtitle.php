<?php if ( ! empty( $subtitle ) ) { ?>
	<div class="qodef-m-subtitle" <?php qode_framework_inline_style( $subtitle_styles ); ?>><?php echo esc_html( $subtitle ); ?></div>
<?php } ?>