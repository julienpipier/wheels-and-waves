<div <?php qode_framework_class_attribute( $holder_classes ); ?>>
	<?php borgholm_core_template_part( 'shortcodes/section-title', 'templates/parts/tagline', '', $params ) ?>
	<?php borgholm_core_template_part( 'shortcodes/section-title', 'templates/parts/title', '', $params ) ?>
	<?php borgholm_core_template_part( 'shortcodes/section-title', 'templates/parts/subtitle', '', $params ) ?>
	<?php borgholm_core_template_part( 'shortcodes/section-title', 'templates/parts/text', '', $params ) ?>
</div>