<?php

if ( ! function_exists( 'borgholm_core_add_section_title_shortcode' ) ) {
	/**
	 * Function that add shortcode into shortcodes list for registration
	 *
	 * @param array $shortcodes
	 *
	 * @return array
	 */
	function borgholm_core_add_section_title_shortcode( $shortcodes ) {
		$shortcodes[] = 'BorgholmCoreSectionTitleShortcode';

		return $shortcodes;
	}

	add_filter( 'borgholm_core_filter_register_shortcodes', 'borgholm_core_add_section_title_shortcode' );
}

if ( class_exists( 'BorgholmCoreShortcode' ) ) {
	class BorgholmCoreSectionTitleShortcode extends BorgholmCoreShortcode {

		public function map_shortcode() {
			$this->set_shortcode_path( BORGHOLM_CORE_SHORTCODES_URL_PATH . '/section-title' );
			$this->set_base( 'borgholm_core_section_title' );
			$this->set_name( esc_html__( 'Section Title', 'borgholm-core' ) );
			$this->set_description( esc_html__( 'Shortcode that adds section title element', 'borgholm-core' ) );
			$this->set_category( esc_html__( 'Borgholm Core', 'borgholm-core' ) );
			$this->set_option( array(
				'field_type' => 'text',
				'name'       => 'custom_class',
				'title'      => esc_html__( 'Custom Class', 'borgholm-core' ),
			) );
			$this->set_option( array(
				'field_type' => 'text',
				'name'       => 'tagline',
				'title'      => esc_html__( 'Tagline', 'borgholm-core' )
			) );
			$this->set_option( array(
				'field_type' => 'color',
				'name'       => 'tagline_color',
				'title'      => esc_html__( 'Tagline Color', 'borgholm-core' ),
				'group'      => esc_html__( 'Tagline Style', 'borgholm-core' )
			) );
			$this->set_option( array(
				'field_type' => 'text',
				'name'       => 'tagline_margin_bottom',
				'title'      => esc_html__( 'Tagline Margin Bottom', 'borgholm-core' ),
				'group'      => esc_html__( 'Tagline Style', 'borgholm-core' )
			) );
			$this->set_option( array(
				'field_type' => 'text',
				'name'       => 'title',
				'title'      => esc_html__( 'Title', 'borgholm-core' )
			) );
			$this->set_option( array(
				'field_type'  => 'text',
				'name'        => 'line_break_positions',
				'title'       => esc_html__( 'Positions of Line Break', 'borgholm-core' ),
				'description' => esc_html__( 'Enter the positions of the words after which you would like to create a line break. Separate the positions with commas (e.g. if you would like the first, third, and fourth word to have a line break, you would enter "1,3,4")', 'borgholm-core' ),
				'group'       => esc_html__( 'Title Style', 'borgholm-core' )
			) );
			$this->set_option( array(
				'field_type'    => 'select',
				'name'          => 'disable_title_break_words',
				'title'         => esc_html__( 'Disable Title Line Break', 'borgholm-core' ),
				'description'   => esc_html__( 'Enabling this option will disable title line breaks for screen size 1024 and lower', 'borgholm-core' ),
				'options'       => borgholm_core_get_select_type_options_pool( 'no_yes', false ),
				'default_value' => 'no',
				'group'         => esc_html__( 'Title Style', 'borgholm-core' )
			) );
			$this->set_option( array(
				'field_type'  => 'text',
				'name'        => 'special_style_positions',
				'title'       => esc_html__( 'Positions of special styled words', 'borgholm-core' ),
				'description' => esc_html__( 'Enter the positions of the words which you would like to style in special way. Separate the positions with commas (e.g. if you would like the first, third, and fourth word to have a special style, you would enter "1,3,4")', 'borgholm-core' ),
				'group'       => esc_html__( 'Title Style', 'borgholm-core' )
			) );
			$this->set_option( array(
				'field_type'    => 'select',
				'name'          => 'title_tag',
				'title'         => esc_html__( 'Title Tag', 'borgholm-core' ),
				'options'       => borgholm_core_get_select_type_options_pool( 'title_tag', true, array(), array(
					'div'  => esc_attr__( 'Custom', 'borgholm-core' ),
					'span' => esc_attr__( 'Custom 2', 'borgholm-core' )
				) ),
				'default_value' => 'h1',
				'group'         => esc_html__( 'Title Style', 'borgholm-core' )
			) );
			$this->set_option( array(
				'field_type' => 'color',
				'name'       => 'title_color',
				'title'      => esc_html__( 'Title Color', 'borgholm-core' ),
				'group'      => esc_html__( 'Title Style', 'borgholm-core' )
			) );
			$this->set_option( array(
				'field_type' => 'text',
				'name'       => 'link',
				'title'      => esc_html__( 'Title Custom Link', 'borgholm-core' ),
				'group'      => esc_html__( 'Title Style', 'borgholm-core' )
			) );
			$this->set_option( array(
				'field_type'    => 'select',
				'name'          => 'target',
				'title'         => esc_html__( 'Custom Link Target', 'borgholm-core' ),
				'options'       => borgholm_core_get_select_type_options_pool( 'link_target' ),
				'default_value' => '_self',
				'dependency'    => array(
					'show' => array(
						'image_action' => array(
							'values'        => 'custom-link',
							'default_value' => ''
						)
					)
				),
				'group'         => esc_html__( 'Title Style', 'borgholm-core' )
			) );
            $this->set_option( array(
                'field_type'    => 'select',
                'name'          => 'animate_special_style',
                'title'         => esc_html__( 'Animate Special Style', 'borgholm-core' ),
                'options'       => borgholm_core_get_select_type_options_pool( 'yes_no', false ),
                'default_value' => 'no',
                'group'         => esc_html__( 'Title Style', 'borgholm-core' )
            ) );
			$this->set_option( array(
				'field_type' => 'text',
				'name'       => 'subtitle',
				'title'      => esc_html__( 'Subtitle', 'borgholm-core' )
			) );
			$this->set_option( array(
				'field_type' => 'color',
				'name'       => 'subtitle_color',
				'title'      => esc_html__( 'Subtitle Color', 'borgholm-core' ),
				'group'      => esc_html__( 'Subtitle Style', 'borgholm-core' )
			) );
			$this->set_option( array(
				'field_type' => 'text',
				'name'       => 'subtitle_margin_top',
				'title'      => esc_html__( 'Subtitle Margin Top', 'borgholm-core' ),
				'group'      => esc_html__( 'Subtitle Style', 'borgholm-core' )
			) );
			$this->set_option( array(
				'field_type' => 'textarea',
				'name'       => 'text',
				'title'      => esc_html__( 'Text', 'borgholm-core' )
			) );
			$this->set_option( array(
				'field_type'    => 'select',
				'name'          => 'enable_text_custom_styles',
				'title'         => esc_html__( 'Enable Text Custom Styles', 'borgholm-core' ),
				'description'   => esc_html__( 'Enabling this option will set text custom styles', 'borgholm-core' ),
				'options'       => borgholm_core_get_select_type_options_pool( 'no_yes', false ),
				'default_value' => 'no',
				'group'         => esc_html__( 'Text Style', 'borgholm-core' )
			) );
			$this->set_option( array(
				'field_type'  => 'text',
				'name'        => 'text_special_style_positions',
				'title'       => esc_html__( 'Positions of special styled words', 'borgholm-core' ),
				'description' => esc_html__( 'Enter the positions of the words which you would like to style in special way. Separate the positions with commas (e.g. if you would like the first, third, and fourth word to have a special style, you would enter "1,3,4")', 'borgholm-core' ),
				'group'       => esc_html__( 'Text Style', 'borgholm-core' )
			) );
			$this->set_option( array(
				'field_type' => 'color',
				'name'       => 'text_color',
				'title'      => esc_html__( 'Text Color', 'borgholm-core' ),
				'group'      => esc_html__( 'Text Style', 'borgholm-core' )
			) );
			$this->set_option( array(
				'field_type' => 'text',
				'name'       => 'text_margin_top',
				'title'      => esc_html__( 'Text Margin Top', 'borgholm-core' ),
				'group'      => esc_html__( 'Text Style', 'borgholm-core' )
			) );
			$this->set_option( array(
				'field_type' => 'select',
				'name'       => 'content_alignment',
				'title'      => esc_html__( 'Content Alignment', 'borgholm-core' ),
				'options'    => array(
					''       => esc_html__( 'Default', 'borgholm-core' ),
					'left'   => esc_html__( 'Left', 'borgholm-core' ),
					'center' => esc_html__( 'Center', 'borgholm-core' ),
					'right'  => esc_html__( 'Right', 'borgholm-core' )
				),
			) );
		}

		public static function call_shortcode( $params ) {
			$html = qode_framework_call_shortcode( 'borgholm_core_section_title', $params );
			$html = str_replace( "\n", '', $html );

			return $html;
		}

		public function render( $options, $content = null ) {
			parent::render( $options );
			$atts = $this->get_atts();

			$atts['holder_classes']  = $this->get_holder_classes( $atts );
			$atts['tagline_styles']  = $this->get_tagline_styles( $atts );
			$atts['title']           = $this->get_modified_title( $atts );
			$atts['title_styles']    = $this->get_title_styles( $atts );
			$atts['subtitle_styles'] = $this->get_subtitle_styles( $atts );
			$atts['text']            = $this->get_modified_text( $atts );
			$atts['text_styles']     = $this->get_text_styles( $atts );

			return borgholm_core_get_template_part( 'shortcodes/section-title', 'templates/section-title', '', $atts );
		}

		private function get_holder_classes( $atts ) {
			$holder_classes = $this->init_holder_classes();

			$holder_classes[] = 'qodef-section-title';
			$holder_classes[] = ! empty( $atts['content_alignment'] ) ? 'qodef-alignment--' . $atts['content_alignment'] : 'qodef-alignment--left';
			$holder_classes[] = $atts['disable_title_break_words'] === 'yes' ? 'qodef-title-break--disabled' : '';
			$holder_classes[] = $atts['enable_text_custom_styles'] === 'yes' ? 'qodef-text-custom-styles--enabled' : '';
			$holder_classes[] = $atts['animate_special_style'] === 'yes' ? 'qodef-animate-special-style' : '';

			return implode( ' ', $holder_classes );
		}

		private function get_tagline_styles( $atts ) {
			$styles = array();

			if ( $atts['tagline_margin_bottom'] !== '' ) {
				if ( qode_framework_string_ends_with_space_units( $atts['tagline_margin_bottom'] ) ) {
					$styles[] = 'margin-bottom: ' . $atts['tagline_margin_bottom'];
				} else {
					$styles[] = 'margin-bottom: ' . intval( $atts['tagline_margin_bottom'] ) . 'px';
				}
			}

			if ( ! empty( $atts['tagline_color'] ) ) {
				$styles[] = 'color: ' . $atts['tagline_color'];
			}

			return $styles;
		}

		private function get_modified_title( $atts ) {
			$title = $atts['title'];

			if ( ! empty( $title ) ) {
				$split_title = explode( ' ', $title );

				if ( ! empty( $atts['special_style_positions'] ) ) {
					$special_style_positions = explode( ',', str_replace( ' ', '', $atts['special_style_positions'] ) );

					foreach ( $special_style_positions as $position ) {
						$position = (int) $position;
						if ( isset( $split_title[ $position - 1 ] ) && ! empty( $split_title[ $position - 1 ] ) ) {
							$split_title[ $position - 1 ] = '<span class="qodef-special-style">' . $split_title[ $position - 1 ] . '</span>';
						}
					}
				}

				if ( ! empty( $atts['line_break_positions'] ) ) {
					$line_break_positions = explode( ',', str_replace( ' ', '', $atts['line_break_positions'] ) );

					foreach ( $line_break_positions as $position ) {
						$position = (int) $position;
						if ( isset( $split_title[ $position - 1 ] ) && ! empty( $split_title[ $position - 1 ] ) ) {
							$split_title[ $position - 1 ] = $split_title[ $position - 1 ] . '<br />';
						}
					}
				}

				$title = implode( ' ', $split_title );
			}

			return $title;
		}

		private function get_title_styles( $atts ) {
			$styles = array();

			if ( ! empty( $atts['title_color'] ) ) {
				$styles[] = 'color: ' . $atts['title_color'];
			}

			return $styles;
		}

		private function get_subtitle_styles( $atts ) {
			$styles = array();

			if ( $atts['subtitle_margin_top'] !== '' ) {
				if ( qode_framework_string_ends_with_space_units( $atts['subtitle_margin_top'] ) ) {
					$styles[] = 'margin-top: ' . $atts['subtitle_margin_top'];
				} else {
					$styles[] = 'margin-top: ' . intval( $atts['subtitle_margin_top'] ) . 'px';
				}
			}

			if ( ! empty( $atts['subtitle_color'] ) ) {
				$styles[] = 'color: ' . $atts['subtitle_color'];
			}

			return $styles;
		}

		private function get_modified_text( $atts ) {
			$text = $atts['text'];

			if ( ! empty( $text ) ) {
				$split_text = explode( ' ', $text );

				if ( ! empty( $atts['text_special_style_positions'] ) ) {
					$special_style_positions = explode( ',', str_replace( ' ', '', $atts['text_special_style_positions'] ) );

					foreach ( $special_style_positions as $position ) {
						$position = (int) $position;
						if ( isset( $split_text[ $position - 1 ] ) && ! empty( $split_text[ $position - 1 ] ) ) {
							$split_text[ $position - 1 ] = '<span class="qodef-special-style">' . $split_text[ $position - 1 ] . '</span>';
						}
					}
				}

				$text = implode( ' ', $split_text );
			}

			return $text;
		}

		private function get_text_styles( $atts ) {
			$styles = array();

			if ( $atts['text_margin_top'] !== '' ) {
				if ( qode_framework_string_ends_with_space_units( $atts['text_margin_top'] ) ) {
					$styles[] = 'margin-top: ' . $atts['text_margin_top'];
				} else {
					$styles[] = 'margin-top: ' . intval( $atts['text_margin_top'] ) . 'px';
				}
			}

			if ( ! empty( $atts['text_color'] ) ) {
				$styles[] = 'color: ' . $atts['text_color'];
			}

			return $styles;
		}
	}
}