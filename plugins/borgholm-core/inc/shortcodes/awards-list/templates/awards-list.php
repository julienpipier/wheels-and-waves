<div <?php qode_framework_class_attribute( $holder_classes ); ?>>
	<div class="qodef-m-items">
		<?php foreach ( $items as $item ) { ?>
			<div class="qodef-m-item qodef-e">
				<div class="qodef-e-year">
					<span class="qodef-e-item-year"><?php echo esc_html( $item['item_year'] ); ?></span>
				</div>
				<div class="qodef-e-awards">
					<span class="qodef-e-item-awards"><?php echo esc_html( $item['item_awards'] ); ?></span>
					<span class="qodef-e-item-description"><?php echo esc_html( $item['item_awards_des'] ); ?></span>
				</div>
			</div>
		<?php } ?>
	</div>
</div>