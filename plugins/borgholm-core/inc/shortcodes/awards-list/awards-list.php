<?php

if ( ! function_exists( 'borgholm_core_add_awards_list_shortcode' ) ) {
	/**
	 * Function that add shortcode into shortcodes list for registration
	 *
	 * @param $shortcodes array
	 *
	 * @return array
	 */
	function borgholm_core_add_awards_list_shortcode( $shortcodes ) {
		$shortcodes[] = 'BorgholmCoreAwardsListShortcode';
		
		return $shortcodes;
	}
	
	add_filter( 'borgholm_core_filter_register_shortcodes', 'borgholm_core_add_awards_list_shortcode' );
}

if ( class_exists( 'BorgholmCoreShortcode' ) ) {
	class BorgholmCoreAwardsListShortcode extends BorgholmCoreShortcode {

		public function map_shortcode() {
			$this->set_shortcode_path( BORGHOLM_CORE_SHORTCODES_URL_PATH . '/awards-list' );
			$this->set_base( 'borgholm_core_awards_list' );
			$this->set_name( esc_html__( 'Awards List', 'borgholm-core' ) );
			$this->set_description( esc_html__( 'Shortcode that adds awards list holder', 'borgholm-core' ) );
			$this->set_category( esc_html__( 'Borgholm Core', 'borgholm-core' ) );
			$this->set_option( array(
				'field_type' => 'text',
				'name'       => 'custom_class',
				'title'      => esc_html__( 'Custom Class', 'borgholm-core' ),
			) );

			$this->set_option( array(
				'field_type' => 'select',
				'name'       => 'skin',
				'title'      => esc_html__( 'Link Skin', 'borgholm-core' ),
				'options'    => array(
					''      => esc_html__( 'Default', 'borgholm-core' ),
					'light' => esc_html__( 'Light', 'borgholm-core' )
				)
			) );
			$this->set_option( array(
				'field_type' => 'repeater',
				'name'       => 'children',
				'title'      => esc_html__( 'Child elements', 'borgholm-core' ),
				'items'   => array(
					array(
						'field_type' => 'text',
						'name'       => 'item_year',
						'title'      => esc_html__( 'Year', 'borgholm-core' )
					),
					array(
						'field_type' => 'text',
						'name'       => 'item_awards',
						'title'      => esc_html__( 'Awards', 'borgholm-core' )
					),
					array(
						'field_type' => 'text',
						'name'       => 'item_awards_des',
						'title'      => esc_html__( 'Description', 'borgholm-core' )
					)
				)
			) );
		}
		
		public function render( $options, $content = null ) {
			parent::render( $options );
			$atts = $this->get_atts();
			
			$atts['holder_classes'] = $this->get_holder_classes( $atts );
			$atts['items']          = $this->parse_repeater_items( $atts['children'] );

			return borgholm_core_get_template_part( 'shortcodes/awards-list', 'templates/awards-list', '', $atts );
		}
		
		private function get_holder_classes( $atts ) {
			$holder_classes = $this->init_holder_classes();
			
			$holder_classes[] = 'qodef-awards-list';
			$holder_classes[] = ! empty( $atts['skin'] ) ? 'qodef-skin--' . $atts['skin'] : '';
			
			return implode( ' ', $holder_classes );
		}
	}
}