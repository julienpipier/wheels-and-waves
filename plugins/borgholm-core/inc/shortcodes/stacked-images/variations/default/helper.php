<?php

if ( ! function_exists( 'borgholm_core_add_stacked_images_variation_default' ) ) {
	/**
	 * Function that add variation layout for this module
	 *
	 * @param array $variations
	 *
	 * @return array
	 */
	function borgholm_core_add_stacked_images_variation_default( $variations ) {
		$variations['default'] = esc_html__( 'Default', 'borgholm-core' );
		
		return $variations;
	}
	
	add_filter( 'borgholm_core_filter_stacked_images_layouts', 'borgholm_core_add_stacked_images_variation_default' );
}
