<?php

include_once BORGHOLM_CORE_SHORTCODES_PATH . '/stacked-images/stacked-images.php';

foreach ( glob( BORGHOLM_CORE_SHORTCODES_PATH . '/stacked-images/variations/*/include.php' ) as $variation ) {
	include_once $variation;
}