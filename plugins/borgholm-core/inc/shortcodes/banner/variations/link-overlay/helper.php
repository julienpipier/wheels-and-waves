<?php

if ( ! function_exists( 'borgholm_core_add_banner_variation_link_overlay' ) ) {
	/**
	 * Function that add variation layout for this module
	 *
	 * @param array $variations
	 *
	 * @return array
	 */
	function borgholm_core_add_banner_variation_link_overlay( $variations ) {
		$variations['link-overlay'] = esc_html__( 'Link Overlay', 'borgholm-core' );
		
		return $variations;
	}
	
	add_filter( 'borgholm_core_filter_banner_layouts', 'borgholm_core_add_banner_variation_link_overlay' );
}
