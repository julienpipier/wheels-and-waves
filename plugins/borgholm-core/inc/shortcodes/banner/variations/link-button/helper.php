<?php

if ( ! function_exists( 'borgholm_core_add_banner_variation_link_button' ) ) {
	/**
	 * Function that add variation layout for this module
	 *
	 * @param array $variations
	 *
	 * @return array
	 */
	function borgholm_core_add_banner_variation_link_button( $variations ) {
		$variations['link-button'] = esc_html__( 'Link Button', 'borgholm-core' );
		
		return $variations;
	}
	
	add_filter( 'borgholm_core_filter_banner_layouts', 'borgholm_core_add_banner_variation_link_button' );
}
