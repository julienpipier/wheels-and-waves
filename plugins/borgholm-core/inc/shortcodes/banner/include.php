<?php

include_once BORGHOLM_CORE_SHORTCODES_PATH . '/banner/banner.php';

foreach ( glob( BORGHOLM_CORE_INC_PATH . '/shortcodes/banner/variations/*/include.php' ) as $variation ) {
	include_once $variation;
}