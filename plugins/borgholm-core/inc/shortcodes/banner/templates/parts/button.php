<?php if ( class_exists( 'BorgholmCoreButtonShortcode' ) ) { ?>
	<div class="qodef-m-button">
		<?php echo BorgholmCoreButtonShortcode::call_shortcode( $button_params ); ?>
	</div>
<?php } ?>