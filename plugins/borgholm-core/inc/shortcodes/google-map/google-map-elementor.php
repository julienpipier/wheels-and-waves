<?php

class BorgholmCoreElementorGoogleMap extends BorgholmCoreElementorWidgetBase {
	
	function __construct( array $data = [], $args = null ) {
		$this->set_shortcode_slug( 'borgholm_core_google_map' );
		
		parent::__construct( $data, $args );
	}
}

borgholm_core_get_elementor_widgets_manager()->register_widget_type( new BorgholmCoreElementorGoogleMap() );