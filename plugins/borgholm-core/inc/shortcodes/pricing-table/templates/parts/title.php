<?php if ( ! empty( $title ) ) { ?>
	<div class="qodef-m-title">
		<h3 <?php qode_framework_inline_style( $title_styles ); ?>>
			<?php echo esc_html( $title ); ?>
			<?php if ( $featured_table === 'yes' ) { ?>
				<span class="qodef-featured-label"><?php echo borgholm_star_svg(); ?></span>
			<?php } ?>
		</h3>
	</div>
<?php } ?>