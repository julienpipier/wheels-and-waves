<?php if ( ! empty( $price_period ) ) { ?>
	<div class="qodef-m-price-period" <?php qode_framework_inline_style( $price_period_styles ); ?>><?php echo esc_html( $price_period ); ?></div>
<?php } ?>