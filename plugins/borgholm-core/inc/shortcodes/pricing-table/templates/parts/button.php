<?php if ( ! empty( $button_params ) ) { ?>
	<div class="qodef-m-button clear">
		<?php echo BorgholmCoreButtonShortcode::call_shortcode( $button_params ); ?>
	</div>
<?php } ?>