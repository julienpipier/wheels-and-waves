<?php

include_once BORGHOLM_CORE_SHORTCODES_PATH . '/pricing-table/pricing-table.php';

foreach ( glob( BORGHOLM_CORE_SHORTCODES_PATH . '/pricing-table/variations/*/include.php' ) as $variation ) {
	include_once $variation;
}