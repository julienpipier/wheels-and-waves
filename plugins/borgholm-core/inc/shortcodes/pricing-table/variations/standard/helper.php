<?php

if ( ! function_exists( 'borgholm_core_add_pricing_table_variation_standard' ) ) {
	/**
	 * Function that add variation layout for this module
	 *
	 * @param array $variations
	 *
	 * @return array
	 */
	function borgholm_core_add_pricing_table_variation_standard( $variations ) {
		
		$variations['standard'] = esc_html__( 'Standard', 'borgholm-core' );
		
		return $variations;
	}
	
	add_filter( 'borgholm_core_filter_pricing_table_layouts', 'borgholm_core_add_pricing_table_variation_standard' );
}
