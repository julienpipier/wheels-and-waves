<div <?php qode_framework_class_attribute( $holder_classes ); ?> <?php qode_framework_inline_style( $holder_styles ); ?>>
	<div class="qodef-m-inner">
		<?php borgholm_core_template_part( 'shortcodes/pricing-table', 'templates/parts/price', '', $params ) ?>
		<div class="qodef-m-inner-2">
			<?php borgholm_core_template_part( 'shortcodes/pricing-table', 'templates/parts/price-period', '', $params ) ?>
			<?php borgholm_core_template_part( 'shortcodes/pricing-table', 'templates/parts/title', '', $params ) ?>
			<?php borgholm_core_template_part( 'shortcodes/pricing-table', 'templates/parts/content', '', $params ) ?>
		</div>
		<?php borgholm_core_template_part( 'shortcodes/pricing-table', 'templates/parts/button', '', $params ) ?>
	</div>
</div>