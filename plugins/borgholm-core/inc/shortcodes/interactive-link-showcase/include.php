<?php

include_once BORGHOLM_CORE_SHORTCODES_PATH . '/interactive-link-showcase/interactive-link-showcase.php';

foreach ( glob( BORGHOLM_CORE_SHORTCODES_PATH . '/interactive-link-showcase/variations/*/include.php' ) as $variation ) {
	include_once $variation;
}