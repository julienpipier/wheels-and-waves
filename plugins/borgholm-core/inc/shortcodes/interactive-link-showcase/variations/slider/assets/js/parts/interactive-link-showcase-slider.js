(function ($) {
    "use strict";

    $(document).ready(function () {
        qodefInteractiveLinkShowcaseSlider.init();
    });

    $(window).resize(function () {
		qodefInteractiveLinkShowcaseSlider.init();
	});

    var qodefInteractiveLinkShowcaseSlider = {
        init: function () {
            this.holder = $('.qodef-interactive-link-showcase.qodef-layout--slider');

            if (this.holder.length) {
                this.holder.each(function () {
                    var $thisHolder = $(this),
                        $images = $thisHolder.find('.qodef-m-image');

                    if($thisHolder.hasClass('qodef-custom-layout--enabled')) {
                        qodefInteractiveLinkShowcaseSlider.fullHeightEnable($thisHolder);
                    }

                    var $swiperSlider = new Swiper($thisHolder.find('.swiper-container'), {
                        loop: true,
                        slidesPerView: 'auto',
                        centeredSlides: true,
                        speed: 1000,
                        init: false
                    });
    
                    $thisHolder.waitForImages(function () {
                        $swiperSlider.init();
                    });
    
                    $swiperSlider.on('init', function () {
                        $images.eq(0).addClass('qodef--active');
                        $thisHolder.find('.swiper-slide-active').addClass('qodef--active');
        
                        $swiperSlider.on('slideChangeTransitionStart', function () {
                            var $swiperSlides = $thisHolder.find('.swiper-slide'),
                                $activeSlideItem = $thisHolder.find('.swiper-slide-active');
            
                            $images.removeClass('qodef--active').eq($activeSlideItem.data('swiper-slide-index')).addClass('qodef--active');
                            $swiperSlides.removeClass('qodef--active');
            
                            $activeSlideItem.addClass('qodef--active');
                        });
        
                        $thisHolder.find('.swiper-slide').on('click', function (e) {
                            var $thisSwiperLink = $(this),
                                $activeSlideItem = $thisHolder.find('.swiper-slide-active');
            
                            if (!$thisSwiperLink.hasClass('swiper-slide-active')) {
                                e.preventDefault();
                                e.stopImmediatePropagation();
                
                                if (e.pageX < $activeSlideItem.offset().left) {
                                    $swiperSlider.slidePrev();
                                    return false;
                                }
                
                                if (e.pageX > $activeSlideItem.offset().left + $activeSlideItem.outerWidth()) {
                                    $swiperSlider.slideNext();
                                    return false;
                                }
                            }
                        });
        
                        $thisHolder.addClass('qodef--init');

                        qodefInteractiveLinkShowcaseSlider.setMouseScroll( $thisHolder );
                    });
                });
            }
        },

        fullHeightEnable: function ($holder) {
            var header = $('#qodef-page-header'),
                mobileHeader = $('#qodef-page-mobile-header'),
                headerTopOffset = header.length ? header.offset().top : 0,
                mobileHeaderTopOffset = mobileHeader.length ? mobileHeader.offset().top : 0,
                headerHeight = header.length ? header.outerHeight() : 0,
                mobileHeaderHeight = mobileHeader.length ? mobileHeader.outerHeight() : 0,
                newHeight = 0;

            if (qodef.windowWidth < 1025) {
                newHeight = qodef.windowHeight - mobileHeaderTopOffset - mobileHeaderHeight;
            } else {
                newHeight = qodef.windowHeight - headerTopOffset - headerHeight;
            }

            $holder.css({
                'height': newHeight,
            });
        },

        setMouseScroll: function ( $thisHolder ) {
            var $slider = $thisHolder.find('.swiper-container'),
                $scrollStart = false;

            $slider.on(
                'mousewheel',
                function ( e ) {
                    e.preventDefault();

                    if ( ! $scrollStart ) {
                        $scrollStart = true;
                        if ( e.deltaY < 0 ) {
                            $slider[0].swiper.slideNext();
                        } else {
                            $slider[0].swiper.slidePrev();
                        }

                        setTimeout(
                            function () {
                                $scrollStart = false;
                            },
                            1000
                        );
                    }
                }
            );
        }
    };

	qodefCore.shortcodes.borgholm_core_interactive_link_showcase.qodefInteractiveLinkShowcaseSlider = qodefInteractiveLinkShowcaseSlider;

})(jQuery);