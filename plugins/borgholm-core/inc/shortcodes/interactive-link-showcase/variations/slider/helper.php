<?php

if ( ! function_exists( 'borgholm_core_add_interactive_link_showcase_variation_slider' ) ) {
	/**
	 * Function that add variation layout for this module
	 *
	 * @param array $variations
	 *
	 * @return array
	 */
	function borgholm_core_add_interactive_link_showcase_variation_slider( $variations ) {
		$variations['slider'] = esc_html__( 'Slider', 'borgholm-core' );
		
		return $variations;
	}
	
	add_filter( 'borgholm_core_filter_interactive_link_showcase_layouts', 'borgholm_core_add_interactive_link_showcase_variation_slider' );
}
