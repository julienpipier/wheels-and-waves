<?php

if ( ! function_exists( 'borgholm_core_add_custom_font_variation_simple' ) ) {
	/**
	 * Function that add variation layout for this module
	 *
	 * @param array $variations
	 *
	 * @return array
	 */
	function borgholm_core_add_custom_font_variation_simple( $variations ) {
		$variations['simple'] = esc_html__( 'Simple', 'borgholm-core' );
		
		return $variations;
	}
	
	add_filter( 'borgholm_core_filter_custom_font_layouts', 'borgholm_core_add_custom_font_variation_simple' );
}
