<?php

if ( ! function_exists( 'borgholm_core_add_custom_font_shortcode' ) ) {
	/**
	 * Function that add shortcode into shortcodes list for registration
	 *
	 * @param array $shortcodes
	 *
	 * @return array
	 */
	function borgholm_core_add_custom_font_shortcode( $shortcodes ) {
		$shortcodes[] = 'BorgholmCoreCustomFontShortcode';
		
		return $shortcodes;
	}
	
	add_filter( 'borgholm_core_filter_register_shortcodes', 'borgholm_core_add_custom_font_shortcode' );
}

if ( class_exists( 'BorgholmCoreShortcode' ) ) {
	class BorgholmCoreCustomFontShortcode extends BorgholmCoreShortcode {
		
		public function __construct() {
			$this->set_layouts( apply_filters( 'borgholm_core_filter_custom_font_layouts', array() ) );
			
			parent::__construct();
		}
		
		public function map_shortcode() {
			$this->set_shortcode_path( BORGHOLM_CORE_SHORTCODES_URL_PATH . '/custom-font' );
			$this->set_base( 'borgholm_core_custom_font' );
			$this->set_name( esc_html__( 'Custom Font', 'borgholm-core' ) );
			$this->set_description( esc_html__( 'Shortcode that displays custom font with provided parameters', 'borgholm-core' ) );
			$this->set_category( esc_html__( 'Borgholm Core', 'borgholm-core' ) );
            $this->set_scripts(
                array(
                    'typed' => array(
                        'registered'	=> false,
                        'url'			=> BORGHOLM_CORE_INC_URL_PATH . '/shortcodes/custom-font/assets/js/plugins/typed.js',
                        'dependency'	=> array( 'jquery' )
                    )
                )
            );

			$options_map = borgholm_core_get_variations_options_map( $this->get_layouts() );

			$this->set_option( array(
				'field_type'    => 'select',
				'name'          => 'layout',
				'title'         => esc_html__( 'Layout', 'borgholm-core' ),
				'options'       => $this->get_layouts(),
				'default_value' => $options_map['default_value'],
				'visibility'    => array(
					'map_for_page_builder' => $options_map['visibility'],
					'map_for_widget' => $options_map['visibility']
				)
			) );
			$this->set_option( array(
				'field_type' => 'text',
				'name'       => 'custom_class',
				'title'      => esc_html__( 'Custom Class', 'borgholm-core' ),
			) );
			$this->set_option( array(
				'field_type' => 'text',
				'name'       => 'title',
				'title'      => esc_html__( 'Title Text', 'borgholm-core' )
			) );
			$this->set_option( array(
				'field_type'    => 'select',
				'name'          => 'title_tag',
				'title'         => esc_html__( 'Title Tag', 'borgholm-core' ),
				'options'       => borgholm_core_get_select_type_options_pool( 'title_tag' ),
				'default_value' => 'p',
				'group'         => esc_html__( 'Typography', 'borgholm-core' )
			) );
			$this->set_option( array(
				'field_type' => 'color',
				'name'       => 'color',
				'title'      => esc_html__( 'Color', 'borgholm-core' ),
				'group'      => esc_html__( 'Typography', 'borgholm-core' )
			) );
			$this->set_option( array(
				'field_type' => 'text',
				'name'       => 'font_family',
				'title'      => esc_html__( 'Font Family', 'borgholm-core' ),
				'group'      => esc_html__( 'Typography', 'borgholm-core' )
			) );
			$this->set_option( array(
				'field_type' => 'text',
				'name'       => 'font_size',
				'title'      => esc_html__( 'Font Size', 'borgholm-core' ),
				'group'      => esc_html__( 'Typography', 'borgholm-core' )
			) );
			$this->set_option( array(
				'field_type' => 'text',
				'name'       => 'line_height',
				'title'      => esc_html__( 'Line Height', 'borgholm-core' ),
				'group'      => esc_html__( 'Typography', 'borgholm-core' )
			) );
			$this->set_option( array(
				'field_type' => 'text',
				'name'       => 'letter_spacing',
				'title'      => esc_html__( 'Letter Spacing', 'borgholm-core' ),
				'group'      => esc_html__( 'Typography', 'borgholm-core' )
			) );
			$this->set_option( array(
				'field_type' => 'select',
				'name'       => 'font_weight',
				'title'      => esc_html__( 'Font Weight', 'borgholm-core' ),
				'options'    => borgholm_core_get_select_type_options_pool( 'font_weight' ),
				'group'      => esc_html__( 'Typography', 'borgholm-core' )
			) );
			$this->set_option( array(
				'field_type' => 'select',
				'name'       => 'font_style',
				'title'      => esc_html__( 'Font Style', 'borgholm-core' ),
				'options'    => borgholm_core_get_select_type_options_pool( 'font_style' ),
				'group'      => esc_html__( 'Typography', 'borgholm-core' )
			) );
			$this->set_option( array(
				'field_type' => 'select',
				'name'       => 'text_transform',
				'title'      => esc_html__( 'Text Transform', 'borgholm-core' ),
				'options'    => borgholm_core_get_select_type_options_pool( 'text_transform' ),
				'group'      => esc_html__( 'Typography', 'borgholm-core' )
			) );
			$this->set_option( array(
				'field_type' => 'text',
				'name'       => 'margin',
				'title'      => esc_html__( 'Margin', 'borgholm-core' ),
				'group'      => esc_html__( 'Typography', 'borgholm-core' )
			) );
            $this->set_option( array(
                'field_type'    => 'select',
                'name'          => 'type_out_effect',
                'title'         => esc_html__( 'Enable Type Out Effect', 'borgholm-core' ),
                'options'       => borgholm_core_get_select_type_options_pool( 'yes_no', false ),
                'default_value' => 'no',
                'description'   => esc_html__( 'Adds a type out effect inside custom font content', 'borgholm-core' ),
                'group'         => esc_html__( 'Typography', 'borgholm-core' )
            ) );
            $this->set_option( array(
                'field_type'  => 'text',
                'name'        => 'type_out_position',
                'title'       => esc_html__( 'Position of Type Out Effect', 'borgholm-core' ),
                'description' => esc_html__( 'Enter the position of the word after which you would like to display type out effect (e.g. if you would like the type out effect after the 3rd word, you would enter "3")', 'borgholm-core' ),
                'dependency'  => array(
                    'show' => array(
                        'type_out_effect' => array(
                            'values'        => 'yes',
                            'default_value' => 'no'
                        )
                    )
                ),
                'group'       => esc_html__( 'Typography', 'borgholm-core' )
            ) );
            $this->set_option( array(
                'field_type' => 'color',
                'name'       => 'typed_color',
                'title'      => esc_html__( 'Typed Color', 'borgholm-core' ),
                'dependency' => array(
                    'show' => array(
                        'type_out_effect' => array(
                            'values'        => 'yes',
                            'default_value' => 'no'
                        )
                    )
                ),
                'group'      => esc_html__( 'Typography', 'borgholm-core' )
            ) );
            $this->set_option( array(
                'field_type' => 'text',
                'name'       => 'typed_ending_1',
                'title'      => esc_html__( 'Typed Ending Number 1', 'borgholm-core' ),
                'dependency' => array(
                    'show' => array(
                        'type_out_effect' => array(
                            'values'        => 'yes',
                            'default_value' => 'no'
                        )
                    )
                ),
                'group'      => esc_html__( 'Typography', 'borgholm-core' )
            ) );
            $this->set_option( array(
                'field_type' => 'text',
                'name'       => 'typed_ending_2',
                'title'      => esc_html__( 'Typed Ending Number 2', 'borgholm-core' ),
                'dependency' => array(
                    'hide' => array(
                        'typed_ending_1' => array(
                            'values' => ''
                        )
                    )
                ),
                'group'      => esc_html__( 'Typography', 'borgholm-core' )
            ) );
            $this->set_option( array(
                'field_type' => 'text',
                'name'       => 'typed_ending_3',
                'title'      => esc_html__( 'Typed Ending Number 3', 'borgholm-core' ),
                'dependency' => array(
                    'hide' => array(
                        'typed_ending_2' => array(
                            'values' => ''
                        )
                    )
                ),
                'group'      => esc_html__( 'Typography', 'borgholm-core' )
            ) );
            $this->set_option( array(
                'field_type' => 'text',
                'name'       => 'typed_ending_4',
                'title'      => esc_html__( 'Typed Ending Number 4', 'borgholm-core' ),
                'dependency' => array(
                    'hide' => array(
                        'typed_ending_3' => array(
                            'values' => ''
                        )
                    )
                ),
                'group'      => esc_html__( 'Typography', 'borgholm-core' )
            ) );
            $this->set_option( array(
                'field_type'    => 'select',
                'name'          => 'title_break_word',
                'title'         => esc_html__( 'Break Word Before Type Out Effect', 'borgholm-core' ),
                'options'       => borgholm_core_get_select_type_options_pool( 'yes_no', false ),
                'default_value' => 'no',
                'dependency'    => array(
                    'hide' => array(
                        'type_out_effect' => array(
                            'values' => 'no'
                        )
                    )
                ),
                'group'         => esc_html__( 'Typography', 'borgholm-core' )
            ) );
            $this->set_option( array(
                'field_type'    => 'select',
                'name'          => 'disable_break_word',
                'title'         => esc_html__( 'Disable Line Break for Smaller Screens', 'borgholm-core' ),
                'options'       => borgholm_core_get_select_type_options_pool( 'yes_no', false ),
                'default_value' => 'no',
                'dependency'    => array(
                    'show' => array(
                        'title_break_word' => array(
                            'values' => 'yes'
                        )
                    )
                ),
                'group'         => esc_html__( 'Typography', 'borgholm-core' )
            ) );
			$this->set_option( array(
				'field_type'  => 'text',
				'name'        => 'font_size_1366',
				'title'       => esc_html__( 'Font Size', 'borgholm-core' ),
				'description' => esc_html__( 'Set responsive style value for screen size 1366', 'borgholm-core' ),
				'group'       => esc_html__( 'Screen Size 1366', 'borgholm-core' )
			) );
			$this->set_option( array(
				'field_type'  => 'text',
				'name'        => 'line_height_1366',
				'title'       => esc_html__( 'Line Height', 'borgholm-core' ),
				'description' => esc_html__( 'Set responsive style value for screen size 1366', 'borgholm-core' ),
				'group'       => esc_html__( 'Screen Size 1366', 'borgholm-core' )
			) );
			$this->set_option( array(
				'field_type'  => 'text',
				'name'        => 'letter_spacing_1366',
				'title'       => esc_html__( 'Letter Spacing', 'borgholm-core' ),
				'description' => esc_html__( 'Set responsive style value for screen size 1366', 'borgholm-core' ),
				'group'       => esc_html__( 'Screen Size 1366', 'borgholm-core' )
			) );
			$this->set_option( array(
				'field_type'  => 'text',
				'name'        => 'font_size_1024',
				'title'       => esc_html__( 'Font Size', 'borgholm-core' ),
				'description' => esc_html__( 'Set responsive style value for screen size 1024', 'borgholm-core' ),
				'group'       => esc_html__( 'Screen Size 1024', 'borgholm-core' )
			) );
			$this->set_option( array(
				'field_type'  => 'text',
				'name'        => 'line_height_1024',
				'title'       => esc_html__( 'Line Height', 'borgholm-core' ),
				'description' => esc_html__( 'Set responsive style value for screen size 1024', 'borgholm-core' ),
				'group'       => esc_html__( 'Screen Size 1024', 'borgholm-core' )
			) );
			$this->set_option( array(
				'field_type'  => 'text',
				'name'        => 'letter_spacing_1024',
				'title'       => esc_html__( 'Letter Spacing', 'borgholm-core' ),
				'description' => esc_html__( 'Set responsive style value for screen size 1024', 'borgholm-core' ),
				'group'       => esc_html__( 'Screen Size 1024', 'borgholm-core' )
			) );
			$this->set_option( array(
				'field_type'  => 'text',
				'name'        => 'font_size_768',
				'title'       => esc_html__( 'Font Size', 'borgholm-core' ),
				'description' => esc_html__( 'Set responsive style value for screen size 768', 'borgholm-core' ),
				'group'       => esc_html__( 'Screen Size 768', 'borgholm-core' )
			) );
			$this->set_option( array(
				'field_type'  => 'text',
				'name'        => 'line_height_768',
				'title'       => esc_html__( 'Line Height', 'borgholm-core' ),
				'description' => esc_html__( 'Set responsive style value for screen size 768', 'borgholm-core' ),
				'group'       => esc_html__( 'Screen Size 768', 'borgholm-core' )
			) );
			$this->set_option( array(
				'field_type'  => 'text',
				'name'        => 'letter_spacing_768',
				'title'       => esc_html__( 'Letter Spacing', 'borgholm-core' ),
				'description' => esc_html__( 'Set responsive style value for screen size 768', 'borgholm-core' ),
				'group'       => esc_html__( 'Screen Size 768', 'borgholm-core' )
			) );
			$this->set_option( array(
				'field_type'  => 'text',
				'name'        => 'font_size_680',
				'title'       => esc_html__( 'Font Size', 'borgholm-core' ),
				'description' => esc_html__( 'Set responsive style value for screen size 680', 'borgholm-core' ),
				'group'       => esc_html__( 'Screen Size 680', 'borgholm-core' )
			) );
			$this->set_option( array(
				'field_type'  => 'text',
				'name'        => 'line_height_680',
				'title'       => esc_html__( 'Line Height', 'borgholm-core' ),
				'description' => esc_html__( 'Set responsive style value for screen size 680', 'borgholm-core' ),
				'group'       => esc_html__( 'Screen Size 680', 'borgholm-core' )
			) );
			$this->set_option( array(
				'field_type'  => 'text',
				'name'        => 'letter_spacing_680',
				'title'       => esc_html__( 'Letter Spacing', 'borgholm-core' ),
				'description' => esc_html__( 'Set responsive style value for screen size 680', 'borgholm-core' ),
				'group'       => esc_html__( 'Screen Size 680', 'borgholm-core' )
			) );
		}

        public function load_assets() {
            wp_enqueue_script( 'typed' );
        }
		
		public function render( $options, $content = null ) {
			parent::render( $options );
			$atts = $this->get_atts();
			
			$atts['unique_class']   = 'qodef-custom-font-' . rand( 0, 1000 );
			$atts['holder_classes'] = $this->get_holder_classes( $atts );
			$atts['holder_styles']  = $this->get_holder_styles( $atts );
            $atts['title']          = $this->get_modified_title( $atts );
			$this->set_responsive_styles( $atts );

			return borgholm_core_get_template_part( 'shortcodes/custom-font', 'variations/'.$atts['layout'].'/templates/custom-font', '', $atts );
		}
		
		private function get_holder_classes( $atts ) {
			$holder_classes = $this->init_holder_classes();
			
			$holder_classes[] = 'qodef-custom-font';
			$holder_classes[] = $atts['unique_class'];
			$holder_classes[] = ! empty( $atts['layout'] ) ? 'qodef-layout--' . $atts['layout'] : '';
            $holder_classes[] = $atts['type_out_effect'] === 'yes' ? 'qodef-cf-has-type-out' : '';
            $holder_classes[] = $atts['disable_break_word'] === 'yes' ? 'qodef-disable-title-break' : '';
			
			return implode( ' ', $holder_classes );
		}
		
		private function get_holder_styles( $atts ) {
			$styles = array();
			
			if ( ! empty( $atts['color'] ) ) {
				$styles[] = 'color: ' . $atts['color'];
			}
			
			if ( ! empty( $atts['font_family'] ) ) {
				$styles[] = 'font-family: ' . $atts['font_family'];
			}
			
			$font_size = $atts['font_size'];
			if ( ! empty( $font_size ) ) {
				if ( qode_framework_string_ends_with_typography_units( $font_size ) ) {
					$styles[] = 'font-size: ' . $font_size;
				} else {
					$styles[] = 'font-size: ' . intval( $font_size ) . 'px';
				}
			}
			
			$line_height = $atts['line_height'];
			if ( ! empty( $line_height ) ) {
				if ( qode_framework_string_ends_with_typography_units( $line_height ) ) {
					$styles[] = 'line-height: ' . $line_height;
				} else {
					$styles[] = 'line-height: ' . intval( $line_height ) . 'px';
				}
			}
			
			$letter_spacing = $atts['letter_spacing'];
			if ( $letter_spacing !== '' ) {
				if ( qode_framework_string_ends_with_typography_units( $letter_spacing ) ) {
					$styles[] = 'letter-spacing: ' . $letter_spacing;
				} else {
					$styles[] = 'letter-spacing: ' . intval( $letter_spacing ) . 'px';
				}
			}
			
			if ( ! empty( $atts['font_weight'] ) ) {
				$styles[] = 'font-weight: ' . $atts['font_weight'];
			}
			
			if ( ! empty( $atts['font_style'] ) ) {
				$styles[] = 'font-style: ' . $atts['font_style'];
			}
			
			if ( ! empty( $atts['text_transform'] ) ) {
				$styles[] = 'text-transform: ' . $atts['text_transform'];
			}
			
			if ( $atts['margin'] !== '' ) {
				$styles[] = 'margin: ' . $atts['margin'];
			}
			
			return $styles;
		}
		
		private function set_responsive_styles( $atts ) {
			$unique_class = '.' . $atts['unique_class'];
			$screen_sizes = array( '1366', '1024', '768', '680' );
			$option_keys  = array( 'font_size', 'line_height', 'letter_spacing' );
			
			foreach ( $screen_sizes as $screen_size ) {
				$styles = array();
				
				foreach ( $option_keys as $option_key ) {
					$option_value = $atts[ $option_key . '_' . $screen_size ];
					$style_key    = str_replace( '_', '-', $option_key );
					
					if ( $option_value !== '' ) {
						if ( qode_framework_string_ends_with_typography_units( $option_value ) ) {
							$styles[ $style_key ] = $option_value . '!important';
						} else {
							$styles[ $style_key ] = intval( $option_value ) . 'px !important';
						}
					}
				}
				
				if ( ! empty( $styles ) ) {
					add_filter( 'borgholm_core_filter_add_responsive_' . $screen_size . '_inline_style_in_footer', function ( $style ) use ( $unique_class, $styles ) {
						$style .= qode_framework_dynamic_style( $unique_class, $styles );
						
						return $style;
					} );
				}
			}
		}

        private function get_modified_title( $atts ) {
            $title             = $atts['title'];
            $type_out_effect   = $atts['type_out_effect'];
            $title_break_word  = $atts['title_break_word'];
            $type_out_position = str_replace( ' ', '', $atts['type_out_position'] );

            if ( ! empty( $title ) && ( $type_out_effect === 'yes' ) ) {
                $split_title = explode( ' ', $title );

                if ( $type_out_effect === 'yes' && ! empty( $type_out_position ) ) {
                    $typed_styles   = $this->get_typed_styles( $atts );
                    $typed_ending_1 = $atts['typed_ending_1'];
                    $typed_ending_2 = $atts['typed_ending_2'];
                    $typed_ending_3 = $atts['typed_ending_3'];
                    $typed_ending_4 = $atts['typed_ending_4'];

                    $typed_html = '<span class="qodef-cf-typed-wrap" ' . qode_framework_get_inline_style( $typed_styles ) . '><span class="qodef-cf-typed">';
                    if ( ! empty( $typed_ending_1 ) ) {
                        $typed_html .= '<span class="qodef-cf-typed-1">' . esc_html( $typed_ending_1 ) . '</span>';
                    }
                    if ( ! empty( $typed_ending_2 ) ) {
                        $typed_html .= '<span class="qodef-cf-typed-2">' . esc_html( $typed_ending_2 ) . '</span>';
                    }
                    if ( ! empty( $typed_ending_3 ) ) {
                        $typed_html .= '<span class="qodef-cf-typed-3">' . esc_html( $typed_ending_3 ) . '</span>';
                    }
                    if ( ! empty( $typed_ending_4 ) ) {
                        $typed_html .= '<span class="qodef-cf-typed-4">' . esc_html( $typed_ending_4 ) . '</span>';
                    }
                    $typed_html .= '</span></span>';

                    if ( ! empty( $split_title[ $type_out_position - 1 ] ) && $title_break_word === 'yes' ) {
                        $split_title[ $type_out_position - 1 ] = $split_title[ $type_out_position - 1 ] . '<br/>' . $typed_html;
                    } else {
                        $split_title[ $type_out_position - 1 ] = $split_title[ $type_out_position - 1 ] . ' ' . $typed_html;
                    }
                }

                $title = implode( ' ', $split_title );
            }

            return $title;
        }

        private function get_typed_styles( $atts ) {
            $styles = array();

            if ( ! empty( $atts['typed_color'] ) ) {
                $styles[] = 'color: ' . $atts['typed_color'];
            }

            return implode( ';', $styles );
        }
	}
}
