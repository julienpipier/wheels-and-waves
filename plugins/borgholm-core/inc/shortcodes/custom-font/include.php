<?php

include_once BORGHOLM_CORE_SHORTCODES_PATH . '/custom-font/custom-font.php';

foreach ( glob( BORGHOLM_CORE_SHORTCODES_PATH . '/custom-font/variations/*/include.php' ) as $variation ) {
	include_once $variation;
}