(function ($) {
    'use strict';

    qodefCore.shortcodes.borgholm_core_custom_font = {};

    $(window).on('load', function () {
        qodefCustomFont.init();
    });

    var qodefCustomFont = {
        init: function () {
            this.customfonts = $('.qodef-custom-font');

            if ( this.customfonts.length ) {
                this.customfonts.each( function () {
                    var $thisCustomFont = $(this);

                    qodefCustomFont.typeOutEffect( $thisCustomFont );
                });
            }
        },
        typeOutEffect: function ( $thisCustomFont ) {
            var $typed = $thisCustomFont.find('.qodef-cf-typed');

            if ( $typed.length ) {
                $typed.each( function () {
                    var $thisTyped = $(this),
                        $typedWrap = $thisTyped.parent('.qodef-cf-typed-wrap'),
                        $customFontHolder = $typedWrap.parent('.qodef-custom-font'),
                        $str = [],
                        $string_1 = $thisTyped.find('.qodef-cf-typed-1').text(),
                        $string_2 = $thisTyped.find('.qodef-cf-typed-2').text(),
                        $string_3 = $thisTyped.find('.qodef-cf-typed-3').text(),
                        $string_4 = $thisTyped.find('.qodef-cf-typed-4').text();

                    if ( $string_1.length ) {
                        $str.push($string_1);
                    }

                    if ( $string_2.length ) {
                        $str.push($string_2);
                    }

                    if ( $string_3.length ) {
                       $str.push($string_3);
                    }

                    if ( $string_4.length ) {
                        $str.push($string_4);
                    }

                    $customFontHolder.appear(function () {
                        $thisTyped.typed({
                            strings: $str,
                            typeSpeed: 90,
                            backDelay: 700,
                            loop: true,
                            contentType: 'text',
                            loopCount: false,
                            cursorChar: '|'
                        });
                    }, {accX: 0, accY: -50});
                });
            }
        }
    };

    qodefCore.shortcodes.borgholm_core_custom_font.qodefCustomFont  = qodefCustomFont;

})(jQuery);