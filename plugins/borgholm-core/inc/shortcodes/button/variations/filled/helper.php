<?php

if ( ! function_exists( 'borgholm_core_add_button_variation_filled' ) ) {
	/**
	 * Function that add variation layout for this module
	 *
	 * @param array $variations
	 *
	 * @return array
	 */
	function borgholm_core_add_button_variation_filled( $variations ) {
		$variations['filled'] = esc_html__( 'Filled', 'borgholm-core' );
		
		return $variations;
	}
	
	add_filter( 'borgholm_core_filter_button_layouts', 'borgholm_core_add_button_variation_filled' );
}
