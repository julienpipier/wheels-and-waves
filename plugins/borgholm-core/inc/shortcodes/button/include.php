<?php

include_once BORGHOLM_CORE_SHORTCODES_PATH . '/button/button.php';

foreach ( glob( BORGHOLM_CORE_SHORTCODES_PATH . '/button/variations/*/include.php' ) as $variation ) {
	include_once $variation;
}