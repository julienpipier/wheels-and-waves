<?php

if ( ! function_exists( 'borgholm_core_add_page_title_options' ) ) {
	/**
	 * Function that add general options for this module
	 */
	function borgholm_core_add_page_title_options() {
		$qode_framework = qode_framework_get_framework_root();

		$page = $qode_framework->add_options_page(
			array(
				'scope'       => BORGHOLM_CORE_OPTIONS_NAME,
				'type'        => 'admin',
				'slug'        => 'title',
				'icon'        => 'fa fa-cog',
				'title'       => esc_html__( 'Title', 'borgholm-core' ),
				'description' => esc_html__( 'Global Title Options', 'borgholm-core' )
			)
		);

		if ( $page ) {
			$page->add_field_element(
				array(
					'field_type'    => 'yesno',
					'name'          => 'qodef_enable_page_title',
					'title'         => esc_html__( 'Enable Page Title', 'borgholm-core' ),
					'description'   => esc_html__( 'Use this option to enable/disable page title', 'borgholm-core' ),
					'default_value' => 'yes'
				)
			);

			$page_title_section = $page->add_section_element(
				array(
					'name'       => 'qodef_page_title_section',
					'title'      => esc_html__( 'Title Area', 'borgholm-core' ),
					'dependency' => array(
						'hide' => array(
							'qodef_enable_page_title' => array(
								'values'        => 'no',
								'default_value' => ''
							)
						)
					)
				)
			);

			$page_title_section->add_field_element(
				array(
					'field_type'    => 'select',
					'name'          => 'qodef_title_layout',
					'title'         => esc_html__( 'Title Layout', 'borgholm-core' ),
					'description'   => esc_html__( 'Choose a title layout', 'borgholm-core' ),
					'options'       => apply_filters( 'borgholm_core_filter_title_layout_options', array() ),
					'default_value' => 'standard'
				)
			);

			$page_title_section->add_field_element(
				array(
					'field_type'    => 'yesno',
					'name'          => 'qodef_set_page_title_area_in_grid',
					'title'         => esc_html__( 'Page Title In Grid', 'borgholm-core' ),
					'description'   => esc_html__( 'Enabling this option will set page title area to be in grid', 'borgholm-core' ),
					'default_value' => 'yes'
				)
			);
			
			$page_title_section->add_field_element(
				array(
					'field_type'  => 'text',
					'name'        => 'qodef_page_title_width',
					'title'       => esc_html__( 'Width', 'borgholm-core' ),
					'description' => esc_html__( 'Enter title width, drop text in new row', 'borgholm-core' ),
					'args'        => array(
						'suffix' => esc_html__( '%', 'borgholm-core' )
					)
				)
			);
			
			$page_title_section->add_field_element(
				array(
					'field_type'  => 'text',
					'name'        => 'qodef_page_title_width_on_smaller_screens',
					'title'       => esc_html__( 'Width on Smaller Screens', 'borgholm-core' ),
					'description' => esc_html__( 'Enter title width, drop text in new row, to be displayed on smaller screens with active mobile header', 'borgholm-core' ),
					'args'        => array(
						'suffix' => esc_html__( '%', 'borgholm-core' )
					)
				)
			);

			$page_title_section->add_field_element(
				array(
					'field_type'  => 'text',
					'name'        => 'qodef_page_title_height',
					'title'       => esc_html__( 'Height', 'borgholm-core' ),
					'description' => esc_html__( 'Enter title height', 'borgholm-core' ),
					'args'        => array(
						'suffix' => esc_html__( 'px', 'borgholm-core' )
					)
				)
			);
			
			$page_title_section->add_field_element(
				array(
					'field_type'  => 'text',
					'name'        => 'qodef_page_title_height_on_smaller_screens',
					'title'       => esc_html__( 'Height on Smaller Screens', 'borgholm-core' ),
					'description' => esc_html__( 'Enter title height to be displayed on smaller screens with active mobile header', 'borgholm-core' ),
					'args'        => array(
						'suffix' => esc_html__( 'px', 'borgholm-core' )
					)
				)
			);

			$page_title_section->add_field_element(
				array(
					'field_type'  => 'color',
					'name'        => 'qodef_page_title_background_color',
					'title'       => esc_html__( 'Background Color', 'borgholm-core' ),
					'description' => esc_html__( 'Enter page title area background color', 'borgholm-core' )
				)
			);

			$page_title_section->add_field_element(
				array(
					'field_type'  => 'image',
					'name'        => 'qodef_page_title_background_image',
					'title'       => esc_html__( 'Background Image', 'borgholm-core' ),
					'description' => esc_html__( 'Enter page title area background image', 'borgholm-core' )
				)
			);

			$page_title_section->add_field_element(
				array(
					'field_type' => 'select',
					'name'       => 'qodef_page_title_background_image_behavior',
					'title'      => esc_html__( 'Background Image Behavior', 'borgholm-core' ),
					'options'    => array(
						''           => esc_html__( 'Default', 'borgholm-core' ),
						'responsive' => esc_html__( 'Set Responsive Image', 'borgholm-core' ),
						'parallax'   => esc_html__( 'Set Parallax Image', 'borgholm-core' )
					)
				)
			);

			$page_title_section->add_field_element(
				array(
					'field_type' => 'color',
					'name'       => 'qodef_page_title_color',
					'title'      => esc_html__( 'Title Color', 'borgholm-core' )
				)
			);

			$page_title_section->add_field_element(
				array(
					'field_type'    => 'select',
					'name'          => 'qodef_page_title_tag',
					'title'         => esc_html__( 'Title Tag', 'borgholm-core' ),
					'description'   => esc_html__( 'Enabling this option will set title tag', 'borgholm-core' ),
					'options'       => borgholm_core_get_select_type_options_pool( 'title_tag', false ),
					'default_value' => 'h1'
				)
			);

			$page_title_section->add_field_element(
				array(
					'field_type'    => 'select',
					'name'          => 'qodef_page_title_text_alignment',
					'title'         => esc_html__( 'Text Alignment', 'borgholm-core' ),
					'options'       => array(
						'left'   => esc_html__( 'Left', 'borgholm-core' ),
						'center' => esc_html__( 'Center', 'borgholm-core' ),
						'right'  => esc_html__( 'Right', 'borgholm-core' )
					),
					'default_value' => 'left'
				)
			);

			$page_title_section->add_field_element(
				array(
					'field_type'    => 'select',
					'name'          => 'qodef_page_title_vertical_text_alignment',
					'title'         => esc_html__( 'Vertical Text Alignment', 'borgholm-core' ),
					'options'       => array(
						'header-bottom' => esc_html__( 'From Bottom of Header', 'borgholm-core' ),
						'window-top'    => esc_html__( 'From Window Top', 'borgholm-core' )
					),
					'default_value' => 'header-bottom'
				)
			);

			// Hook to include additional options after module options
			do_action( 'borgholm_core_action_after_page_title_options_map', $page_title_section );
		}
	}

	add_action( 'borgholm_core_action_default_options_init', 'borgholm_core_add_page_title_options', borgholm_core_get_admin_options_map_position( 'title' ) );
}