<?php
// Load title image template
borgholm_core_get_page_title_image(); ?>
<div class="qodef-m-content <?php echo esc_attr( borgholm_core_get_page_title_content_classes() ); ?>">
	<?php
	// Load breadcrumbs template
	borgholm_core_breadcrumbs(); ?>
</div>

